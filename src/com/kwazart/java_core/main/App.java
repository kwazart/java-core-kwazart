package com.kwazart.java_core.main;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 30.09.2020 1:21
 */

public class App {
	static public void method() {

	}

	public static void main(String[] args) throws InterruptedException {
		JFrame frame = new JFrame("Авторизация в системе");

		frame.setSize(600, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // говорит что произойдет при закрытии окна
		// по дефолту закрывает окно, но программа работает
		frame.setLocationRelativeTo(null); // устанавливаем точку возникновения фрейма. если null то по центру экрана

//		frame.setResizable(true); // позволяем изменять размер
		//frame.setUndecorated(true); // скрывает title

//		frame.setState(JFrame.ICONIFIED); // программно сворачиваем форму
//		frame.setExtendedState(JFrame.MAXIMIZED_BOTH); // программно разворачиваем форму на максимальную ширину и высоту
//		frame.setExtendedState(JFrame.NORMAL); // разворачиваем до стандартного размера из setSize
//
//		frame.setLayout(new GridBagLayout());
//
//		JButton button = new JButton(); // создаем кнопку, в конструкторе можем прописать текст кнопки
//		JButton button1 = new JButton("вторая кнопка");
//		button.setText("Пример кнопки"); // текст на кнопке
//		button.setToolTipText("При наведении"); // текст при наведении
//
//		button.setBackground(Color.YELLOW); // закрашиваем кнопку
//		button.setForeground(Color.RED); // задаем цвет текста кнопки
//		button1.setBackground(new Color(100, 20, 200));
//
//		Cursor cursor = new Cursor(Cursor.MOVE_CURSOR);
//		button.setCursor(cursor);
//		button1.setCursor(new Cursor(Cursor.NE_RESIZE_CURSOR));
//
//		JTextField textField = new JTextField(10);
//		textField.setText("дефолт");
//		textField.setEditable(false); // разрешаем вводить в поле или запрещаем
//		textField.setBackground(Color.YELLOW);
//
//		JPasswordField passwordField = new JPasswordField(10); // поле для пароля
//
//		JLabel label = new JLabel();
//		label.setText("пример метки");
//		label.setFont(new Font("Verdana", Font.ITALIC, 25)); // изменяем шрифты
//
//		JProgressBar progressBar = new JProgressBar();
//		progressBar.setStringPainted(true); // если хотим видеть в процентах
//		progressBar.setMinimum(0); // задаем границы значений
//		progressBar.setMaximum(80);
//		progressBar.setValue(25); // задаем текущий прогресс
//
//		JPanel panel = new JPanel(); // компонент который содержит другие компоненты
//		panel.setBackground(Color.gray);
//		JButton jButton = new JButton("new button");
//		panel.add(jButton);

//		frame.setLayout(new BorderLayout());
//
//		JButton jButton = new JButton("button");

		//frame.add(jButton, BorderLayout.PAGE_START); // будет добавлена к самому верху

//		JPanel panel1 = new JPanel();
//		JPanel panel2 = new JPanel();
//		JPanel panel3 = new JPanel();
//		JPanel panel4 = new JPanel();
//		JPanel panel5 = new JPanel();
//		panel1.setBackground(Color.YELLOW);
//		panel2.setBackground(Color.BLUE);
//		panel3.setBackground(Color.GREEN);
//		panel4.setBackground(Color.RED);
//		panel5.setBackground(Color.GRAY);

//		frame.add(panel1, BorderLayout.PAGE_START); // верх
//		frame.add(panel2, BorderLayout.PAGE_END); // низ
//		frame.add(panel3, BorderLayout.LINE_END); // слева
//		frame.add(panel4, BorderLayout.LINE_START); // справа
//		frame.add(panel5, BorderLayout.CENTER); // центре

//		frame.add(panel1, BorderLayout.NORTH);
//		frame.add(panel2, BorderLayout.SOUTH);
//		frame.add(panel3, BorderLayout.WEST);
//		frame.add(panel4, BorderLayout.EAST);
//		frame.add(panel5, BorderLayout.CENTER);
//
//		panel5.add(jButton, BorderLayout.PAGE_START);



		//frame.setLayout(new FlowLayout()); // все элементы центрируются по середине сверху и в одном ряду
//		frame.setLayout(new BorderLayout());
//		JPanel panel = new JPanel(new FlowLayout());
//		panel.setPreferredSize(new Dimension(300, 200));
//		panel.setBackground(Color.gray);
//		JButton button1 = new JButton("button 1");
//		JButton button2 = new JButton("button 2");
//		JButton button3 = new JButton("button 3");
//		JButton button4 = new JButton("button 4");
//		JTextField textField = new JTextField(20);
//
//		panel.add(button1);
//		panel.add(button2);
//		panel.add(button3);
//		panel.add(button4);
//		panel.add(textField);
//		frame.add(panel, BorderLayout.SOUTH);


		//frame.setLayout(new GridLayout(3,6));// делит всю область на условные одинаковые блоки (сетка)
//		frame.setLayout(new BorderLayout());
//		JPanel panelCenter = new JPanel(new GridLayout(3,3));
//		JPanel panelNorth = new JPanel(new BorderLayout());
//
//		JButton button1 = new JButton("b1");
//		JButton button2 = new JButton("b2");
//		JButton button3 = new JButton("b3");
//		JButton button4 = new JButton("b4");
//		JButton button5 = new JButton("b5");
//		JButton button6 = new JButton("b6");
//		JButton button7 = new JButton("b7");
//		JButton button8 = new JButton("b8");
//		JButton button9 = new JButton("b9");
//		JButton button0 = new JButton("b0");
//
//		JTextField  textField = new JTextField(10);
//
//		panelCenter.add(button1);
//		panelCenter.add(button2);
//		panelCenter.add(button3);
//		panelCenter.add(button4);
//		panelCenter.add(button5);
//		panelCenter.add(button6);
//		panelCenter.add(button7);
//		panelCenter.add(button8);
//		panelCenter.add(button9);
//		panelCenter.add(button0);
//
//		panelNorth.add(textField, BorderLayout.CENTER);
//
//		frame.add(panelNorth, BorderLayout.NORTH);
//		frame.add(panelCenter, BorderLayout.CENTER);


		frame.setLayout(new GridBagLayout());

		JLabel loginLabel = new JLabel("Login:");
		JLabel passwordLabel = new JLabel("Password:");

		JButton loginButton = new JButton("Login");

		// можно вешать сколько угодно listeners
		loginButton.addActionListener(new LoginButtonActionListener());
		loginButton.addActionListener(new SecondLoginButtonActionListener());
		loginButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("third action listener");
			}
		});

		JTextField loginTextField = new JTextField(15);
		JPasswordField passwordField = new JPasswordField(15);

		JButton registrationButton = new JButton("Registration");
		registrationButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String login = loginTextField.getText();
				String password = String.valueOf(passwordField.getPassword());
				System.out.println("Ваш логин: " + login + "\tВаш пароль: " + password);
			}
		});



		GridBagConstraints c = new GridBagConstraints();

		c.gridx = 0; // задаем начальное положение
		c.gridy = 0;
		c.gridwidth = 1; // определяет количество строк
		c.gridheight = 1; // определяет колиество столбцов
		c.weightx = 0.0; // указывает как будет происходит изменение компонентов
		c.weighty = 0.9;
		c.anchor = GridBagConstraints.NORTH; // указываем где именно будет располагаться компонент
		c.fill = GridBagConstraints.HORIZONTAL; // КАК именно будет располагаться компонент
		Insets insets = new Insets(20,2,2,2); // задает отступы: сверху, слева, снизу, справа
		c.insets = insets;
		c.ipadx = 0; // показывают на сколько будет увеличин размер компонента
		c.ipady = 0;

		// можно все указать через конструктор

		frame.add(loginLabel, new GridBagConstraints(0,0,1,1, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(5,5,5,5), 0,0));
		frame.add(loginTextField, new GridBagConstraints(1,0,1,1, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(5,5,5,5), 0,0));
		frame.add(passwordLabel, new GridBagConstraints(0,1,1,1, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(5,5,5,5), 0,0));
		frame.add(passwordField, new GridBagConstraints(1,1,1,1, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(5,5,5,5), 0,0));

		frame.add(loginButton, new GridBagConstraints(0,2,1,1, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(5,5,2,5), 0,0));
		frame.add(registrationButton, new GridBagConstraints(1,2,1,1, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(2,5,5,5), 0,0));


		frame.pack();
		frame.setVisible(true); // устанавливаем видимость фрейма

		// добавляем компоненты на фрейм
//		frame.add(panel);
//		frame.add(textField);
//		frame.add(passwordField);
//		frame.add(button);
//		frame.add(label);
//		frame.add(progressBar);
//
//		for (int i = progressBar.getMinimum(); i < progressBar.getMaximum(); i++) {
//			progressBar.setValue(i);
//			Thread.sleep(50);
//		}

	}

	// необходимо всегда реализовать интерфейс ActionListiner любым способом
	public static class LoginButtonActionListener implements ActionListener {

		// реагирует на клик
		@Override
		public void actionPerformed(ActionEvent e) {
			System.out.println("Hello Java");
		}
	}

	public static class SecondLoginButtonActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			System.out.println("second listener");
		}
	}
}
