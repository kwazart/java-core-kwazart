package com.kwazart.java_core.main;

import com.kwazart.java_core.main.util.MenuUtil;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 21.09.2020 16:07
 */

public class Menu {
    public static void main(String[] args) {
        MenuUtil menuUtil = new MenuUtil();
        menuUtil.viewMenu();
    }
}
