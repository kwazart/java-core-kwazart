package com.kwazart.java_core.main.model;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 29.09.2020 20:31
 */

public interface InfoPrintable {
	void printInfo();
	String getTHEME();
}
