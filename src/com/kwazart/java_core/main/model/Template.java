package com.kwazart.java_core.main.model;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 02.10.2020 0:14
 */

public class Template implements InfoPrintable {
	private static final String THEME = "THEME";

	public void printInfo() {
		System.out.println("\n\n");
	}

	@Override
	public String getTHEME() {
		return THEME;
	}
}
