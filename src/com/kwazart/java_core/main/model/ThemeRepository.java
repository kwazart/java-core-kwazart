package com.kwazart.java_core.main.model;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 22.09.2020 23:59
 */

public class ThemeRepository<T> {
	private String name;
	private Map<Integer, T> themes = new HashMap<>();

	public ThemeRepository(String name) {
		this.name = name;
	}

	public void add(T themeName) {
		int length = themes.size();
		themes.put(length + 1, themeName);
	}

	public T getTheme(int key) {
		return themes.get(key);
	}

	public List<T> getAllThemes() {
		List<T> themesList = new ArrayList<>();
		for(Map.Entry<Integer, T> entry : themes.entrySet()) {
			themesList.add(entry.getValue());
		}
		return themesList;
	}

	public int getMaxIndex() {
		return themes.size();
	}

	public String getName() {
		return name;
	}
}
