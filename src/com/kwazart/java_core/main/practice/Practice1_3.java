package com.kwazart.java_core.main.practice;

import java.io.IOException;
import java.util.Scanner;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 17.09.2020 21:35
 */

/*
* Console calculator
* Themes:
* - variable
* - condition
* - loops
* */
public class Practice1_3 {
    public static void main(String[] args) throws IOException {
        // use class Scanner for data reading
        // use class String for data parsing
        // don't use exceptions and deep validating

        System.out.println(
                "---------------------------------------------------" +
                "\n|                  CALCULATOR                     |\n" +
                "---------------------------------------------------");
        System.out.println("\nMain function:");
        String s = "\t%-5s - %s%n";
        System.out.printf(s, "+", "addition");
        System.out.printf(s, "-", "subtraction");
        System.out.printf(s, "*", "multiplication");
        System.out.printf(s, "/", "division");
        System.out.printf(s, "^", "exponent");

        System.out.println("This calculator can work with two numbers.");

        Scanner scanner = new Scanner(System.in); // scanner for data reading
        String choice;
        while (true) {

            double a, b;
            System.out.print("Choose what you will work with: " +
                    "\n\t1 - integer values" +
                    "\n\t2 - fractional numbers" +
                    "\n\t0 - Exit" +
                    "\n\nEnter: ");
            while (true) {
                choice = scanner.next();
                if (choice.equalsIgnoreCase("1") || choice.equalsIgnoreCase("2")) {
                    System.out.print("\nPlease enter the first number:");
                    String firstNumber = scanner.next();
                    System.out.print("\nPlease enter the second number:");
                    String secondNumber = scanner.next();
                    if (choice.equals("1")) {
                        a = Integer.parseInt(firstNumber) * 1.;
                        b = Integer.parseInt(secondNumber) * 1.;
                        System.out.println("Choose operation:\t+\t-\t*\t/\t^");
                    } else {
                        System.out.println("Choose operation:\t+\t-\t*\t/");
                        a = Double.parseDouble(firstNumber);
                        b = Double.parseDouble(secondNumber);
                    }
                    break;
                } else if (choice.equalsIgnoreCase("0")) {
                    return;
                } else {
                    System.out.print("Wrong input! Try again!\nEnter:");
                }
            }


            char operation = (char) System.in.read();

            switch (operation) {
                case '+':
                    System.out.println("A + B = " + (a + b));
                    break;
                case '-':
                    System.out.println("A - B = " + (a - b));
                    break;
                case '*':
                    System.out.println("A * B = " + (a * b));
                    break;
                case '/':
                    System.out.println("A / B = " + (a / b));
                    break;
                case '^':
                    if (b == 0)
                        System.out.println("A ^ B = 1");
                    else if (choice.equals("1")) {
                        int res = 1;
                        for (int i = 1; i < b; i++) {
                            if ((b > 0)) {
                                res *= a;
                            } else {
                                res /= a;
                            }
                        }
                        System.out.println("A ^ B = " + res);
                    } else
                        System.out.println("Calculator can only raise whole numbers to a power");
                    break;
                default:
                    System.out.println("Wrong input!");
            }

            System.out.print("\n\nThank you!\nTry again? Y/N(Any key): ");
            String ch = scanner.next();
            System.out.println("\n\n");
            if (!ch.equalsIgnoreCase("y")) {
                System.out.println("See you!");
                break;
            }
        }
    }
}
