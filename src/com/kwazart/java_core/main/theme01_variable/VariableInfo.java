package com.kwazart.java_core.main.theme01_variable;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 17.09.2020 19:03
 */

public class VariableInfo implements InfoPrintable {
    public static final String THEME = "Переменные";

    public void printInfo() {
        // ТИПЫ ПРИМИТИВНЫХ ПЕРЕМЕННЫХ
        // Целочисленные
        byte b = 95;
        short sh = 2000;
        int i = 5_000_000;
        long l = 2_000_000_000_000l;
        // Дробные (с плавающей точкой)
        float f = 10.4f;
        double d = 10.5;
        // Символьные
        char ch = 'a';
        // Логические
        boolean boo = true;

        System.out.println("\n\nПРИМИТИВНЫЕ ТИПЫ В JAVA\nЦелочисленные:\n\tbyte\n\tshort\n\tint\n\tlong\nВещественные:\n\tfloat\n\tdouble\nЛогические:\n\tboolean\n");
        System.out.println("Диапазон значений:");
        String s1 = "%-10s - от %20d до %-20d\tразмер в битах %d\tв байтах %d\n";
        String s1f = "%-10s - от %20s до %-20s \tразмер в битах %d\tв байтах %d\n";
        System.out.printf(s1, "byte", Byte.MIN_VALUE, Byte.MAX_VALUE, Byte.SIZE, Byte.SIZE/8);
        System.out.printf(s1, "short", Short.MIN_VALUE, Short.MAX_VALUE, Short.SIZE, Short.SIZE/8);
        System.out.printf(s1, "int", Integer.MIN_VALUE, Integer.MAX_VALUE, Integer.SIZE, Integer.SIZE/8);
        System.out.printf(s1, "long", Long.MIN_VALUE, Long.MAX_VALUE, Long.SIZE, Long.SIZE/8);
        System.out.printf(s1f, "float", "3.4e-38", "3.4e38", Float.SIZE, Float.SIZE/8);
        System.out.printf(s1f, "double", "1.7e-308", "1.7e308", Double.SIZE, Double.SIZE/8);
        System.out.printf(s1, "char", (int)Character.MIN_VALUE, (int)Character.MAX_VALUE, Character.SIZE, Character.SIZE/8);
        System.out.println("boolean - от " + Boolean.FALSE + " до " + Boolean.TRUE + "\tразмер в битах - 32, в некоторыех JVM в массивах есть реализации где переменные равны 1 биту" );

        System.out.println("\n");

        System.out.println("\nМожно объявлять переменные следующим образом" +
                "\n\tint x = 0;\n\tlong i,j,k;\n\tbyte a1 = 0xF1, a2 = 0x07;");
        System.out.println("Вещественные переменные объявляются так:" +
                "\n\tfloat f1 = 3.5f, f2 = 3.7E6f, f3 = -1.8E-7f;\n\tdouble d = 3.0");
        System.out.println("Символьные переменные можно объявлять следующим образом:" +
                "\n\tchar ch1 = 'f';\n\tchar ch2 = '\\u0042';\n\tchar ch3 = 57; ");
        System.out.println("Логические переменные можно объявлять так: " +
                "boolean b1 = false;\n\tboolean b2 = 5 > 3;");
        System.out.println("\n\nЛитерал - явно заданное значение. В разных системах:" +
                "\n\tДесятичная - 102" +
                "\n\tШестнадцатеричная - 0х2C4, начинается с 0х" +
                "\n\tВосьмеричная - 016, начинается с нуля" +
                "\n\tДвоичная - 0b11001, начинается с 0b");

        // Классы-оболочки (Wrapper) для примитивов
        System.out.println("\nКлассы обертки");
        String s2 = "Для примитивного типа %8s\tклассом оболочкой является %25s\n";
        System.out.printf(s2, "byte", Byte.class);
        System.out.printf(s2, "short", Short.class);
        System.out.printf(s2, "int", Integer.class);
        System.out.printf(s2, "long", Long.class);
        System.out.printf(s2, "float", Float.class);
        System.out.printf(s2, "double", Double.class);
        System.out.printf(s2, "char", Character.class);
        System.out.printf(s2, "boolean", Boolean.class);

        System.out.println("\nПроцесс преобразования примитивныз типов в ссылочные (int -> Integer) называется " +
                "autoboxing (автоупаковой), а обратный ему - unboxing (автораспаковкой)");
        System.out.println("\n\tInteger x = 18; // autoboxing\n\tint i = new Integer(10);");
        System.out.println("\nЭти классы дают возможность сохранять внутри объекта примитив, а сам объект будет вести " +
                "\nсебя как Object (ну как любой другой объект). При всём этом мы получаем большое количество " +
                "\nразношерстных, полезных статических методов, как например — сравнение чисел, перевод символа " +
                "\nв регистр, определение того, является ли символ буквой или числом, поиск минимального числа и т.п. " +
                "\nПредоставляемый набор функционала зависит лишь от самой обертки.");

        // ESCAPE-последовательности
        System.out.println("\nESCAPE последовательности");
        String s3 = "\\%-5s %-30s \\u00%s%n";
        System.out.printf(s3, "b", "Забой (backspace)", "08");
        System.out.printf(s3, "t", "Табуляция", "09");
        System.out.printf(s3, "n", "Перевод строки", "0A");
        System.out.printf(s3, "f", "Перевод страницы", "0C");
        System.out.printf(s3, "r", "Возврат каретки", "0D");
        System.out.printf(s3, "\"", "Двойная кавычка", "22");
        System.out.printf(s3, "'", "Апостроф", "27");
        System.out.printf(s3, "\\", "Обратная косая черта", "5C");

        //Приведение типов
        System.out.println("\nПриведение типов");
        System.out.println("Существует преобразование расширающее и сужающее. Если тип данных становится больше" +
                "\nдопусти был int, а стал long, то тип становится шире (их 32 бит становится 64)." +
                "\nВ таком случае мы не рискуем потерять данные, т.к. если влезло в int, то в long влезет тем более," +
                "\n поэтому данное приведение мы не замечаем, так как оно осуществляется автоматически." +
                "\nА вот в обратную сторону преобразование требует явного указания от нас, данное приведение " +
                "\n типа называется - сужение");
        System.out.println("\n\t int intValue = 128;\n\tbyte value = (byte) intValue; // явное приведение" +
                "\n\tSystem.out.println(value); // -128 из-за зацикливания значений");
        System.out.println("\nРасширяющие автоматические преобразования:" +
                "\n\tbyte -> short -> int -> long" +
                "\n\tint -> double" +
                "\n\tshort -> float -> double" +
                "\n\tchar -> int");
        System.out.println("Если в литерале мы ничего не указываем, то тип default будет int или (в случае если стоит точка) - double");
    }

    public String getTHEME() {
        return THEME;
    }
}
