package com.kwazart.java_core.main.theme022_io;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 04.10.2020 23:46
 */

public class EmptyIOClass implements InfoPrintable {
	private static final String THEME = "Ввод вывод данных";
	@Override
	public void printInfo() {}

	@Override
	public String getTHEME() {
		return THEME;
	}
}
