package com.kwazart.java_core.main.theme022_io;

import java.io.File;
import java.io.IOException;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 01.10.2020 11:33
 */

// класс File
public class FileClass {
	public static void main(String[] args) {
		String fileName = "test.txt";
		String fullName = "";
		String dirName = System.getProperty("user.dir");
		fullName = dirName + File.separator + fileName;
		System.out.println("File path : " + fileName);
		File file = new File(fullName); // задаем файл с указанным путем но не создаем его
		if ( ! file.exists()){ // проверяем на существование
			try {
				if( file.createNewFile()) // создаем файл с путем прописанным в конструкторе
					System.out.println("File created!"); // вывод в консоль т.к. в File не умеет записывать
				else
					System.out.println("Something Wrong!");
			} catch (IOException ex) {
//				Logger.getLogger(FileClass.class.getName()).
//						log(Level.SEVERE, null, ex);
			}
		}else{
			System.out.println("File already exists!");
		}

		String dirname = dirName + "/dir1/dir2/dir3/dir4/dir5";
		File d = new File(dirname);
		// Create directories now.
		d.mkdirs();
	}
}
