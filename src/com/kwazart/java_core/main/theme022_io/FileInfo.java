package com.kwazart.java_core.main.theme022_io;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 02.10.2020 0:12
 */

public class FileInfo implements InfoPrintable {
	private static final String THEME = "Класс File";

	public void printInfo() {
		System.out.println("\n\nКласс File\n" +
				"Этот класс работает непосредственно с файловой системой и является незаменимым при работе с файлами\n" +
				"и папками на диске. Класс File не имеет отношения к процессам чтения или записи. Он содержит информацию\n" +
				"о самих файлах и папках. Экземпляр класса File, можно ассоциировать с файлом или директорией, после чего,\n" +
				"использовать методы класса для получения информации о директории или файле, для изменения характеристик\n" +
				"папок или файлов и конечно же, для создания новых бъектов файловой системы.\n" +
				"Полное описание этого класса можете посмотреть на официальном сайте по адресу:\n" +
				"https://docs.oracle.com/javase/8/docs/api/java/io/File.html.\n\n" +
				"" +
				"File имеет такие конструкторы:\n\n" +
				"" +
				"File (File parent, String child) создает новый объект с именем child по тому же пути, где размещен объект parent;\n" +
				"File (String path) создает новый объект по пути path;\n" +
				"File (String parent, String child) создает новый объект с именем child по пути parent;\n" +
				"File (URI uri) создает новый объект по абстрактному пути, полученному путем конвертации uri.\n\n" +
				"" +
				"Прежде всего, вы должны понять следующее: если вы создали объект класса File, это вовсе не означает\n" +
				"автоматического создания файла или папки на диске. Чтобы создать файл или папку, соответствующие объекту File,\n" +
				"надо вызывать специальные методы.\n" +
				"Создайте новый проект и добавьте в метод main() такой код:\n\n" +
				"" +
				"\tString fileName = \"test.txt\";\n" +
				"\tString fullName = \"\";\n" +
				"\tString dirName = System.getProperty(\"user.dir\");\n" +
				"\tfullName = dirName + File.separator + fileName;\n" +
				"\tSystem.out.println(\"File path : \" + fileName);\n" +
				"\tFile file = new File(fullName); // задаем файл с указанным путем но не создаем его\n\n" +
				"" +
				"Мы создали объект класса файл, указав в качестве пути корневую папку текущего проекта. Обратите внимание,\n" +
				"что для получения пути к корневой папке проекта мы использовали свойство \"user.dir\". А вот для разделителя\n" +
				"между именем папки и именем файла мы использовали поле File.separator. Запомните, что в классе File хранятся\n" +
				"необходимые свойства, позволяющие не обращаться к вызову метода System.getProperty(). Запустите проект,\n" +
				"а затем перейдите в его корневую папку и убедитесь, что файл test.txt не создан. Это то, о чем мы говорили:\n" +
				"создание объекта класса File не приводит автоматически к созданию файла на диске.\n" +
				"Добавьте к коду такое продолжение:\n\n" +
				"" +
				"\tif ( ! file.exists()){ // проверяем на существование\n" +
				"\t\ttry {\n" +
				"\t\t\tif( file.createNewFile()) // создаем файл с путем прописанным в конструкторе\n" +
				"\t\t\t\tSystem.out.println(\"File created!\"); // вывод в консоль т.к. в File не умеет записывать\n" +
				"\t\t\telse\n" +
				"\t\t\t\tSystem.out.println(\"Something Wrong!\");\n" +
				"\t\t} catch (IOException ex) {\n" +
				"\t\t\t//Logger.getLogger(FileClass.class.getName()).log(Level.SEVERE, null, ex);\n" +
				"\t\t}\n" +
				"\t}else{\n" +
				"\t\tSystem.out.println(\"File already exists!\");\n" +
				"\t}\n\n" +
				"" +
				"Обратите внимание на то, что мы ничего в созданный файл не записывали. Класс File не умеет читать или писать.\n" +
				"Для этих целей применяются потоковые классы.\n" +
				"Для создания папок этот класс использует методы mkdir() и mkdirs(). Первый метод позволяет создать только\n" +
				"одну папку, а второй – цепочку вложенных папок.\n" +
				"Добавьте к проекту такой код и выполните проект снова.\n\n" +
				"" +
				"\tString dirname = dirName + \"/dir1/dir2/dir3/dir4/dir5\";\n" +
				"\tFile d = new File(dirname);\n" +
				"\t// Create directories now.\n" +
				"\td.mkdirs();" +
				"\n\n" +
				"А вот если бы вы вызвали метод mkdir(), то никакие папки созданы бы не были, потому, что этот метод требует указания\n" +
				"только одной папки, без вложенных папок.\n" +
				"В этом классе собрано еще много полезных методов. Например, таких как:\n\n" +
				"" +
				"deleteOnExit() если вызвать этот метод для объекта класса File, то файл на диске, ассоциированный с этим\n" +
				"               объектом, будетудален автоматически, при завершении работы JVM;\n" +
				"getFreeSpace() возвращает кол-во свободных байт на том диске, где расположен текущий объект;\n" +
				"getTotalSpace() возвращает общий размер диска, на котором расположен текущий объект;\n" +
				"setReadonly() делает файл доступным только для чтения.\n\n");

		System.out.println("\n\nПолная версия кода:\n\n" +
				"// класс File\n" +
				"public class FileClass {\n" +
				"\tpublic static void main(String[] args) {\n" +
				"\t\tString fileName = \"test.txt\";\n" +
				"\t\tString fullName = \"\";\n" +
				"\t\tString dirName = System.getProperty(\"user.dir\");\n" +
				"\t\tfullName = dirName + File.separator + fileName;\n" +
				"\t\tSystem.out.println(\"File path : \" + fileName);\n" +
				"\t\tFile file = new File(fullName); // задаем файл с указанным путем но не создаем его\n" +
				"\t\tif ( ! file.exists()){ // проверяем на существование\n" +
				"\t\t\ttry {\n" +
				"\t\t\t\tif( file.createNewFile()) // создаем файл с путем прописанным в конструкторе\n" +
				"\t\t\t\t\tSystem.out.println(\"File created!\"); // вывод в консоль т.к. в File не умеет записывать\n" +
				"\t\t\t\telse\n" +
				"\t\t\t\t\tSystem.out.println(\"Something Wrong!\");\n" +
				"\t\t\t} catch (IOException ex) {\n" +
				"//\t\t\t\tLogger.getLogger(FileClass.class.getName()).\n" +
				"//\t\t\t\t\t\tlog(Level.SEVERE, null, ex);\n" +
				"\t\t\t}\n" +
				"\t\t}else{\n" +
				"\t\t\tSystem.out.println(\"File already exists!\");\n" +
				"\t\t}\n" +
				"\n" +
				"\t\tString dirname = dirName + \"/dir1/dir2/dir3/dir4/dir5\";\n" +
				"\t\tFile d = new File(dirname);\n" +
				"\t\t// Create directories now.\n" +
				"\t\td.mkdirs();\n" +
				"\t}\n" +
				"}");
	}

	@Override
	public String getTHEME() {
		return THEME;
	}
}
