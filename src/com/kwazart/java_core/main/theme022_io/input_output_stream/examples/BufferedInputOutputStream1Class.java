package com.kwazart.java_core.main.theme022_io.input_output_stream.examples;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 01.10.2020 18:12
 */

// запись в файл с помощью BufferedStream
public class BufferedInputOutputStream1Class {
	public static void main(String[] args) {
		String text = "This lines of text should be written in file\n"
				+ "using buffered stream.\n"
				+ "Just one more line.\n";
		try(FileOutputStream out=new FileOutputStream("notes.txt");
			// буффер потока это скрытый буффер (не одно и то же что и массив используемый ранее
			// размер равен 8192 байт, значение можно изменить
			BufferedOutputStream bos = new BufferedOutputStream(out))
		{
			byte[] buffer = text.getBytes();
			bos.write(buffer, 0, buffer.length);
		}
		catch(IOException ex){
			System.out.println(ex.getMessage());
		}
	}
}
