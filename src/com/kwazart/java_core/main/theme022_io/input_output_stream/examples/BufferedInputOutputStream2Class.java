package com.kwazart.java_core.main.theme022_io.input_output_stream.examples;

import java.io.*;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 01.10.2020 18:12
 */

// чтение из файла с помощью BufferedStream
public class BufferedInputOutputStream2Class {
	public static void main(String[] args) {
		try(FileInputStream fin = new FileInputStream(new File("notes.txt"));
			BufferedInputStream bis = new BufferedInputStream(fin)){
			int c;
			while((c=bis.read())!=-1){
				System.out.print((char)c);
			}
		}
		catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
}
