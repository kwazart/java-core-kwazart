package com.kwazart.java_core.main.theme022_io.input_output_stream.examples;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 01.10.2020 16:24
 */

// вывод изображения во фрейм
public class ByteArrayInputOutput1Class {
	BufferedImage image = null;
	JFrame form = null;

	public static void main(String[] args) {
		File fnew=new File("png.png");
		try{
			BufferedImage bImage=ImageIO.read(fnew);
			ByteArrayOutputStream baos=new ByteArrayOutputStream();
			ImageIO.write(bImage, "jpg", baos );
			byte[] imageInByte=baos.toByteArray();
			ByteArrayInputOutput1Class imf =new ByteArrayInputOutput1Class(imageInByte);
		}
		catch (IOException ex) {
//			Logger.getLogger(Filetest.class.getName()).
//					log(Level.SEVERE, null, ex);
			System.out.println("Input output error");
		}
	}
	public ByteArrayInputOutput1Class(byte[] imageInByte) throws IOException {
		image = ImageIO.read(new ByteArrayInputStream(imageInByte));
		form = new JFrame();
		form.setSize(image.getWidth(), image.getHeight());
		form.setAlwaysOnTop(true);
		JPanel pn = new JPanel () {
			@Override
			public void paint(Graphics g) {
				super.paint(g);
				g.drawImage(image, 0, 0, image.
						getWidth(), image.getHeight(), null);
			}};

		pn.setSize(image.getWidth(), image.getHeight());
		form.add(pn);
		form.setVisible(true);
	}
}
