package com.kwazart.java_core.main.theme022_io.input_output_stream.examples;

import java.io.ByteArrayInputStream;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 01.10.2020 16:24
 */

// чтение стринга через ByteArrayStream
public class ByteArrayInputOutput2Class {
	public static void main(String[] args) {
		String line="This is a sample string to be capitalized";
		ByteArrayInputStream bais = new ByteArrayInputStream(line.getBytes());
		int ch;
		StringBuilder sb = new StringBuilder();
		while ( (ch = bais.read()) != -1) {
			sb.append(Character.toUpperCase((char) ch));
		}
		System.out.println("Capitalized string: " + sb.toString());
	}
}
