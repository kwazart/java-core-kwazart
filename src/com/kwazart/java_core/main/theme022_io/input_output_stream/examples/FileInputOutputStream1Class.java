package com.kwazart.java_core.main.theme022_io.input_output_stream.examples;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 01.10.2020 11:59
 */
// чтение данных массивом байтов  равным всему файлу
public class FileInputOutputStream1Class {
	public static void main(String[] args) {
		InputStream in = null;
		OutputStream out = null;
		byte[] buffer = null;
		try {
			in = new FileInputStream(new File("test.txt"));
			buffer = new byte[in.available()];
			in.read(buffer);
			File file = new File("outputFile.tmp");
			out = new FileOutputStream(file);
			//new PrintStream(new File("test1.txt")).write(buffer); // быстрое копирование
			// вывод массива на консоль
			for (byte b : buffer) {
				System.out.print((char) b);
			}
			out.write(buffer);
		} catch (FileNotFoundException ex) {
//			Logger.getLogger(FileInputOutputStreamClass.class.getName()).
//					log(Level.SEVERE, null, ex);
			System.out.println("File not found");
		} catch (IOException ex) {
//			Logger.getLogger(FileInputOutputStreamClass.class.getName()).
//					log(Level.SEVERE, null, ex);\
			System.out.println("Error by input output procedure");
		}
		finally{
			try {
				in.close();
				out.close();
			} catch (IOException ex) {
//				Logger.getLogger(FileInputOutputStreamClass.class.getName()).
//						log(Level.SEVERE, null, ex);
				System.out.println("Cannot close streams");
			}
		}
	}
}
