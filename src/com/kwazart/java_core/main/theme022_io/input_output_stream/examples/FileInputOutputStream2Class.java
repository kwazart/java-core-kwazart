package com.kwazart.java_core.main.theme022_io.input_output_stream.examples;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 01.10.2020 13:22
 */
// чтение данных массивом байтов равным всему файлу через try with resources
public class FileInputOutputStream2Class {
	public static void main(String[] args) {
		File file = new File("outputFile2.tmp");
		byte[] buffer=null;
		try (InputStream in = new FileInputStream(new File("1.jpg"));
			 OutputStream out = new FileOutputStream(file);) {
			buffer = new byte[in.available()];
			in.read(buffer);
			out.write(buffer);
		} catch (FileNotFoundException ex) {
//			Logger.getLogger(FileInputOutputStreamClass.class.getName()).
//					log(Level.SEVERE, null, ex);
			System.out.println("File not found");
		} catch (IOException ex) {
//			Logger.getLogger(FileInputOutputStreamClass.class.getName()).
//					log(Level.SEVERE, null, ex);\
			System.out.println("Error by input output procedure");
		}
	}
}
