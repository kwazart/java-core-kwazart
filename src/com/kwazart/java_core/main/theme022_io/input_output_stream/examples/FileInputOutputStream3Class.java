package com.kwazart.java_core.main.theme022_io.input_output_stream.examples;

import java.io.*;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 01.10.2020 13:22
 */

// чтение данных массивом байтов определенного размера
public class FileInputOutputStream3Class {
	public static void main(String[] args) {
		InputStream in=null;
		OutputStream out=null;
		byte[] buffer=new byte[8*1024];
		try {
			in = new FileInputStream(new File("2.jpg"));
			File file = new File("outputFile2.tmp");
			out = new FileOutputStream(file);
			int bytesRead=0;
			while ((bytesRead = in.read(buffer)) != -1) {
				out.write(buffer, 0, bytesRead);
			}
		} catch (FileNotFoundException ex) {
			//Logger.getLogger(Filetest.class.getName()).log(Level.SEVERE, null, ex);
			System.out.println("File not found");
		}
		catch (IOException ex) {
			//Logger.getLogger(Filetest.class.getName()).log(Level.SEVERE, null, ex);
			System.out.println("Error by input output procedure");
		}
		finally {
			try {
				in.close();
				out.close();
			} catch (IOException ex) {
//				Logger.getLogger(FileInputOutputStreamThirdClass.class.getName()).
//						log(Level.SEVERE, null, ex);
			}
		}
	}
}
