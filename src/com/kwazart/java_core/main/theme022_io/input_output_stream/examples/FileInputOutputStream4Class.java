package com.kwazart.java_core.main.theme022_io.input_output_stream.examples;

import java.io.*;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 01.10.2020 16:10
 */

// побайтовое чтение данных
public class FileInputOutputStream4Class {
	public static void main(String[] args) {
		FileInputStream in = null;
		FileOutputStream out = null;
		try {
			in = new FileInputStream(new File("test.txt"));
			File file = new File("outputFile3.txt");
			out = new FileOutputStream(file);
			int c;
			while ((c = in.read()) != -1) {
				//System.out.print((char) c); можем направить в консоль поток
				if(c < 65)out.write(c);
			}
		} catch (FileNotFoundException ex) {
//			Logger.getLogger(FileInputOutputStreamFourthClass.class.getName()).
//					log(Level.SEVERE, null, ex);
			System.out.println("File not found");
		}
		catch (IOException ex) {
//			Logger.getLogger(FileInputOutputStreamFourthClass.class.getName()).
//					log(Level.SEVERE, null, ex);
			System.out.println("Error by input output procedure");
		}
		finally{
			try {
				in.close();
				out.close();
			} catch (IOException ex) {
//				Logger.getLogger(FileInputOutputStreamFourthClass.class.getName()).
//						log(Level.SEVERE, null, ex);
				System.out.println("Closing streams error");
			}
		}
	}
}
