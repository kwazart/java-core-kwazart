package com.kwazart.java_core.main.theme022_io.input_output_stream.examples;

import java.io.*;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 01.10.2020 17:17
 */

class Developer implements Serializable {
	String language;
	String name;
	int salary;

	public Developer( String name, String language, int salary) {
		this.language = language;
		this.name = name;
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Developer{" +
				"language='" + language + '\'' +
				", name='" + name + '\'' +
				", salary=" + salary +
				'}';
	}
}

// запись объекта в файл
public class ObjectInputOutputStream1Class {
	public static void main(String[] args) {
		FileOutputStream fout=null;
		ObjectOutputStream oout=null;
		try {
			fout = new FileOutputStream(new File("test.txt"));

			Developer d = new Developer("Artem", "Java", 100000);
			oout = new ObjectOutputStream(fout);
			oout.writeObject(d); // для этого классу необхожимо реализовать интерфейс Serializable
			System.out.println("Wrote - " + d);
		} catch (FileNotFoundException ex) {
			//Logger.getLogger(ObjectInputOutputStream1Class.class.getName()).log(Level.SEVERE, null, ex);
			System.out.println("cannot find the file");
		} catch (IOException ex) {
			//Logger.getLogger(ObjectInputOutputStream1Class.class.getName()).log(Level.SEVERE, null, ex);
			System.out.println("input output error");
		}
		finally
		{
			try {
				oout.close();
			} catch (IOException ex) {
				//Logger.getLogger(ObjectInputOutputStream1Class.class.getName()).log(Level.SEVERE, null, ex);
				System.out.println("closing file error");
			}
		}
	}
}
