package com.kwazart.java_core.main.theme022_io.input_output_stream.examples;

import java.io.*;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 01.10.2020 17:17
 */


// чтение объекта из файла
public class ObjectInputOutputStream2Class {
	public static void main(String[] args) {
		FileInputStream fin = null;
		try {
			fin = new FileInputStream(new File("test.txt"));
			ObjectInputStream oin = new ObjectInputStream(fin);
			Developer d = (Developer) oin.readObject();
			System.out.println(d);
		} catch (FileNotFoundException ex) {
//			Logger.getLogger(ObjectInputOutputStream2Class.class.getName()).log(Level.SEVERE, null, ex);
			ex.printStackTrace();
		} catch (IOException ex) {
//			Logger.getLogger(ObjectInputOutputStream2Class.class.getName()).log(Level.SEVERE, null, ex);
			ex.printStackTrace();
		} catch (ClassNotFoundException ex) {
//			Logger.getLogger(ObjectInputOutputStream2Class.class.getName()).log(Level.SEVERE, null, ex);
			ex.printStackTrace();
		} finally {
			try {
				fin.close();
			} catch (IOException ex) {
//				Logger.getLogger(ObjectInputOutputStream2Class.class.getName()).
//						log(Level.SEVERE, null, ex);
			}
		}
	}
}
