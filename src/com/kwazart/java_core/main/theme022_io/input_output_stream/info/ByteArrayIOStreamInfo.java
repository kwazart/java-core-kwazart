package com.kwazart.java_core.main.theme022_io.input_output_stream.info;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 02.10.2020 0:50
 */

public class ByteArrayIOStreamInfo implements InfoPrintable {
	private static final String THEME = "ByteArrayInputStream и ByteArrayOutputStream";

	public void printInfo() {
		System.out.println("\n\nByteArrayInputStream и ByteArrayOutputStream\n\n" +
				"Во всех предыдущих примерах мы рассматривали классы FileInputStream и FileOutputStream, которые являются\n" +
				"реализациями базовых абстрактных классов InputStream и OutputStream. Рассмотрим еще другие байтовые потоки,\n" +
				"производные от этих двух абстрактных классов. Очень часто в Java возникает необходимость преобразовать\n" +
				"какой-нибудь объект в массив байт.\n" +
				"Пример 1:\n\n" +
				"" +
				"// вывод изображения во фрейм\n" +
				"public class ByteArrayInputOutput1Class {\n" +
				"\tBufferedImage image = null;\n" +
				"\tJFrame form = null;\n" +
				"\n" +
				"\tpublic static void main(String[] args) {\n" +
				"\t\tFile fnew=new File(\"png.png\");\n" +
				"\t\ttry{\n" +
				"\t\t\tBufferedImage bImage=ImageIO.read(fnew);\n" +
				"\t\t\tByteArrayOutputStream baos=new ByteArrayOutputStream();\n" +
				"\t\t\tImageIO.write(bImage, \"jpg\", baos );\n" +
				"\t\t\tbyte[] imageInByte=baos.toByteArray();\n" +
				"\t\t\tByteArrayInputOutput1Class imf =new ByteArrayInputOutput1Class(imageInByte);\n" +
				"\t\t}\n" +
				"\t\tcatch (IOException ex) {\n" +
				"//\t\t\tLogger.getLogger(Filetest.class.getName()).\n" +
				"//\t\t\t\t\tlog(Level.SEVERE, null, ex);\n" +
				"\t\t\tSystem.out.println(\"Input output error\");\n" +
				"\t\t}\n" +
				"\t}\n" +
				"\tpublic ByteArrayInputOutput1Class(byte[] imageInByte) throws IOException {\n" +
				"\t\timage = ImageIO.read(new ByteArrayInputStream(imageInByte));\n" +
				"\t\tform = new JFrame();\n" +
				"\t\tform.setSize(image.getWidth(), image.getHeight());\n" +
				"\t\tform.setAlwaysOnTop(true);\n" +
				"\t\tJPanel pn = new JPanel () {\n" +
				"\t\t\t@Override\n" +
				"\t\t\tpublic void paint(Graphics g) {\n" +
				"\t\t\t\tsuper.paint(g);\n" +
				"\t\t\t\tg.drawImage(image, 0, 0, image.\n" +
				"\t\t\t\t\t\tgetWidth(), image.getHeight(), null);\n" +
				"\t\t\t}};\n" +
				"\n" +
				"\t\tpn.setSize(image.getWidth(), image.getHeight());\n" +
				"\t\tform.add(pn);\n" +
				"\t\tform.setVisible(true);\n" +
				"\t}\n" +
				"}\n\n" +
				"" +
				"В приведенном примере мы с помощью класса ImageIO читаем графический файл в объект типа\n" +
				"BufferedImage. Затем создаем объект байтового потока ByteArrayOutputStream, который хранит свои данные\n" +
				"в оперативной памяти. Записываем объект типа BufferedImage в этот поток. Дальше остается преобразовать поток\n" +
				"в байтовый массив с помощью метода toByteArray(). Вы еще не оценили этот пример? Тогда продолжим. Мы пока\n" +
				"ничего не делаем с полученным байтовым массивом. Давайте исправим эту ситуацию. Очень часто возникает\n" +
				"необходимость выполнить действие, которое условно можно назвать так \"преобразование выходного потока\n" +
				"во входной\". Другими словами, в одной части кода вы записали объект в выходной поток, а в другом месте\n" +
				"вам надо этот объект прочитать. В Java самый удобный способ выполнить такое действие – использовать потоки\n" +
				"ByteArrayOutputStream и ByteArrayInputStream. Сначала объект надо записать в выходной поток. Затем\n" +
				"преобразовать этот поток в байтовый массив и передать его, куда надо. Затем байтовый массив преобразовать во\n" +
				"входной поток и прочитать его.\n\n" +
				"Добавьте в состав приложения класс, который будет принимать массив байт с изображением и выводить это\n" +
				"изображение во фрейме (Пример 1).\n\n" +
				"" +
				"Обратите внимание, что в конструкторе класса мы создаем входной поток ByteArrayInputStream вокруг массива\n" +
				"байт, который перед этим был получен из выходного потока ByteArrayOutputStream\n\n" +
				"" +
				"Рассмотрим еще пример преобразования строки с помощью байтового потока в оперативной памяти.\n" +
				"Пример 2:\n\n" +
				"" +
				"// чтение стринга через ByteArrayStream\n" +
				"public class ByteArrayInputOutput2Class {\n" +
				"\tpublic static void main(String[] args) {\n" +
				"\t\tString line=\"This is a sample string to be capitalized\";\n" +
				"\t\tByteArrayInputStream bais = new ByteArrayInputStream(line.getBytes());\n" +
				"\t\tint ch;\n" +
				"\t\tStringBuilder sb = new StringBuilder();\n" +
				"\t\twhile ( (ch = bais.read()) != -1) {\n" +
				"\t\t\tsb.append(Character.toUpperCase((char) ch));\n" +
				"\t\t}\n" +
				"\t\tSystem.out.println(\"Capitalized string: \" + sb.toString());\n" +
				"\t}\n" +
				"}\n\n" +
				"" +
				"В этом примере мы преобразовываем строку в байтовый поток, а затем выполняем побайтовое чтение потока\n" +
				"и одновременно требуемое преобразование прочитанных данных. Поскольку при преобразовании мы создаем новую\n" +
				"строку, здесь удобнее использовать класс StringBuilder.");
	}

	@Override
	public String getTHEME() {
		return THEME;
	}
}
