package com.kwazart.java_core.main.theme022_io.input_output_stream.info;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 04.10.2020 23:50
 */

public class FileIOStreamInfo implements InfoPrintable {
	private static String THEME = "FileInputStream и FileOutputStream";

	public void printInfo() {
		System.out.println("\n\nFileInputStream и FileOutputStream\n" +
				"После этой предварительной информации рассмотрим побайтовую работу с файлами.\n" +
				"Сначала рассмотрим несколько примеров превращения входного потока в файл. В первом примере предполагается,\n" +
				"что данные во входной поток попадают из файла на диске, и что размер этого файла позволяет прочитать его\n" +
				"сразу. Если же мы не можем полагать, что размер данных во входном потоке приемлем, чтобы прочитать его за один\n" +
				"раз, то надо использовать подход, продемонстрированный во втором примере.\n" +
				"Пример 1:\n\n" +
				"" +
				"// чтение данных массивом байтов  равным всему файлу\n" +
				"public class FileInputOutputStream1Class {\n" +
				"\tpublic static void main(String[] args) {\n" +
				"\t\tInputStream in = null;\n" +
				"\t\tOutputStream out = null;\n" +
				"\t\tbyte[] buffer = null;\n" +
				"\t\ttry {\n" +
				"\t\t\tin = new FileInputStream(new File(\"test.txt\"));\n" +
				"\t\t\tbuffer = new byte[in.available()];\n" +
				"\t\t\tin.read(buffer);\n" +
				"\t\t\tFile file = new File(\"outputFile.tmp\");\n" +
				"\t\t\tout = new FileOutputStream(file);\n" +
				"\t\t\t//new PrintStream(new File(\"test1.txt\")).write(buffer); // быстрое копирование\n" +
				"\t\t\t// вывод массива на консоль\n" +
				"\t\t\tfor (byte b : buffer) {\n" +
				"\t\t\t\tSystem.out.print((char) b);\n" +
				"\t\t\t}\n" +
				"\t\t\tout.write(buffer);\n" +
				"\t\t} catch (FileNotFoundException ex) {\n" +
				"//\t\t\tLogger.getLogger(FileInputOutputStreamClass.class.getName()).\n" +
				"//\t\t\t\t\tlog(Level.SEVERE, null, ex);\n" +
				"\t\t\tSystem.out.println(\"File not found\");\n" +
				"\t\t} catch (IOException ex) {\n" +
				"//\t\t\tLogger.getLogger(FileInputOutputStreamClass.class.getName()).\n" +
				"//\t\t\t\t\tlog(Level.SEVERE, null, ex);\\\n" +
				"\t\t\tSystem.out.println(\"Error by input output procedure\");\n" +
				"\t\t}\n" +
				"\t\tfinally{\n" +
				"\t\t\ttry {\n" +
				"\t\t\t\tin.close();\n" +
				"\t\t\t\tout.close();\n" +
				"\t\t\t} catch (IOException ex) {\n" +
				"//\t\t\t\tLogger.getLogger(FileInputOutputStreamClass.class.getName()).\n" +
				"//\t\t\t\t\t\tlog(Level.SEVERE, null, ex);\n" +
				"\t\t\t\tSystem.out.println(\"Cannot close streams\");\n" +
				"\t\t\t}\n" +
				"\t\t}\n" +
				"\t}\n" +
				"}\n\n" +
				"" +
				"Рассмотрим приведенный выше код. Мы создаем объект in типа InputStream и связываем его с существующим\n" +
				"файлом на диске. Это и есть наш входной поток, через который данные будут поступать в наше приложение.\n" +
				"Понятно, что источником данных для этого потока является файл, с которым мы связали наш поток. Этот файл уже\n" +
				"должен находиться в текущей папке вашего приложения. Обратите внимание, что конструктору потока мы\n" +
				"передаем объект типа File. Затем мы создаем массив, в который хотим прочитать содержимое потока. Мы выбрали\n" +
				"поток, который читает данные побайтово, поэтому тип массива – byte[]. Поскольку мы договорились, что размер\n" +
				"потока допускает чтение за один раз, мы должны сделать размер массива таким, чтобы все данные разместились\n" +
				"в нем. Смотрите, как мы указываем размерность массива при создании – используя потоковый метод available().\n" +
				"Затем методом read() за один вызов читаем все данные из потока в созданный байтовый массив.\n\n" +
				"" +
				"Мы хотим создать копию прочитанного файла, поэтому создаем выходной поток out типа OutputStream. Снова,\n" +
				"используя класс File, ассоциируем выходной поток с файлом на жестком диске. Вы понимаете, что для выходного\n" +
				"потока можно указать не существующий файл, и поток создаст этот файл самостоятельно. И, наконец, за один вызов\n" +
				"метода write() от объекта выходного потока, записываем содержимое нашего массива в файл. Проверьте текущую\n" +
				"папку своего приложения и убедитесь, что выходной файл создался и что он является копией исходного файла.\n\n" +
				"" +
				"Отметьте важный момент: мы выполняем чтение и запись файл побайтово, поэтому таким способом мо-\n" +
				"жем копировать не только текстовые файлы, а и бинарные. Положите в текущую папку проекта какой-нибудь\n" +
				"графический файл, свяжите его со входным потоком и запустите приложение. Будет создана копия вашего\n" +
				"графического файла.\n\n" +
				"" +
				"Обратите внимание на try-catch-finally блоки, без которых этот код работать не будет. У вас есть альтернатива.\n" +
				"Посмотрите, как выглядит этот же код, но с улучшенным try блоком с двумя ресурсами:\n\n" +
				"" +
				"// чтение данных массивом байтов равным всему файлу через try with resources\n" +
				"public class FileInputOutputStream2Class {\n" +
				"\tpublic static void main(String[] args) {\n" +
				"\t\tFile file = new File(\"outputFile2.tmp\");\n" +
				"\t\tbyte[] buffer=null;\n" +
				"\t\ttry (InputStream in = new FileInputStream(new File(\"1.jpg\"));\n" +
				"\t\t\t OutputStream out = new FileOutputStream(file);) {\n" +
				"\t\t\tbuffer = new byte[in.available()];\n" +
				"\t\t\tin.read(buffer);\n" +
				"\t\t\tout.write(buffer);\n" +
				"\t\t} catch (FileNotFoundException ex) {\n" +
				"//\t\t\tLogger.getLogger(FileInputOutputStreamClass.class.getName()).\n" +
				"//\t\t\t\t\tlog(Level.SEVERE, null, ex);\n" +
				"\t\t\tSystem.out.println(\"File not found\");\n" +
				"\t\t} catch (IOException ex) {\n" +
				"//\t\t\tLogger.getLogger(FileInputOutputStreamClass.class.getName()).\n" +
				"//\t\t\t\t\tlog(Level.SEVERE, null, ex);\\\n" +
				"\t\t\tSystem.out.println(\"Error by input output procedure\");\n" +
				"\t\t}\n" +
				"\t}\n" +
				"}\n\n" +
				"" +
				"Этот код немного короче, так как отсутствуют явные вызовы метода close().\n" +
				"Теперь рассмотрим случай, когда мы не уверены, что размер данных во входном потоке допускает чтение всех\n" +
				"данных целиком. В этом случае будем читать данные в цикле, порциями.\n" +
				"Пример 2:\n\n" +
				"" +
				"// чтение данных массивом байтов определенного размера\n" +
				"public class FileInputOutputStream3Class {\n" +
				"\tpublic static void main(String[] args) {\n" +
				"\t\tInputStream in=null;\n" +
				"\t\tOutputStream out=null;\n" +
				"\t\tbyte[] buffer=new byte[8*1024];\n" +
				"\t\ttry {\n" +
				"\t\t\tin = new FileInputStream(new File(\"2.jpg\"));\n" +
				"\t\t\tFile file = new File(\"outputFile2.tmp\");\n" +
				"\t\t\tout = new FileOutputStream(file);\n" +
				"\t\t\tint bytesRead=0;\n" +
				"\t\t\twhile ((bytesRead = in.read(buffer)) != -1) {\n" +
				"\t\t\t\tout.write(buffer, 0, bytesRead);\n" +
				"\t\t\t}\n" +
				"\t\t} catch (FileNotFoundException ex) {\n" +
				"\t\t\t//Logger.getLogger(Filetest.class.getName()).log(Level.SEVERE, null, ex);\n" +
				"\t\t\tSystem.out.println(\"File not found\");\n" +
				"\t\t}\n" +
				"\t\tcatch (IOException ex) {\n" +
				"\t\t\t//Logger.getLogger(Filetest.class.getName()).log(Level.SEVERE, null, ex);\n" +
				"\t\t\tSystem.out.println(\"Error by input output procedure\");\n" +
				"\t\t}\n" +
				"\t\tfinally {\n" +
				"\t\t\ttry {\n" +
				"\t\t\t\tin.close();\n" +
				"\t\t\t\tout.close();\n" +
				"\t\t\t} catch (IOException ex) {\n" +
				"//\t\t\t\tLogger.getLogger(FileInputOutputStreamThirdClass.class.getName()).\n" +
				"//\t\t\t\t\t\tlog(Level.SEVERE, null, ex);\n" +
				"\t\t\t}\n" +
				"\t\t}\n" +
				"\t}\n" +
				"}\n\n" +
				"" +
				"Отличия этого примера состоят в том, что мы выполняем чтение входного потока в цикле и на каждой\n" +
				"итерации прочитанную порцию данных сразу записываем в выходной поток. Это избавляет нас от необходимости\n" +
				"создавать большой массив для чтения, чтобы в него поместились все данные. В приведенном случае размер\n" +
				"массива для чтения выбирается произвольно и влияет только на число итераций, которые придется выполнить,\n" +
				"чтобы скопировать все данные. Отметьте использование переменной bytesRead, в которую метод read() возвращает\n" +
				"количество прочитанных байт. Во всех итерациях (если их будет больше одной) метод read() будет заполнять\n" +
				"весь массив buffer и значение bytesRead будет совпадать с размером массива buffer. А на последней итерации бу-\n" +
				"дет прочитана оставшаяся порция данных, которая, как правило, будет меньше размера массива. Поэтому очень\n" +
				"важно, в методе write() в третьем параметре указывать именно количество фактически прочитанных байт, а не\n" +
				"размер массива.\n\n" +
				"" +
				"Вы уже увидели, что байтовые потоки позволяют работать как с текстовыми, так и с бинарными файлами.\n" +
				"Какие еще возможности дает нам использование этих потоков? В предыдущих примерах мы выполняли чтение\n" +
				"в массив, однако есть возможность выполнять чтение буквально побайтово. Это, конечно медленнее, но иногда\n" +
				"бывает необходимо обрабатывать каждый прочитанный байт. Рассмотрим такой пример.\n" +
				"Пример 3:\n\n" +
				"" +
				"// побайтовое чтение данных\n" +
				"public class FileInputOutputStream4Class {\n" +
				"\tpublic static void main(String[] args) {\n" +
				"\t\tFileInputStream in = null;\n" +
				"\t\tFileOutputStream out = null;\n" +
				"\t\ttry {\n" +
				"\t\t\tin = new FileInputStream(new File(\"test.txt\"));\n" +
				"\t\t\tFile file = new File(\"outputFile3.txt\");\n" +
				"\t\t\tout = new FileOutputStream(file);\n" +
				"\t\t\tint c;\n" +
				"\t\t\twhile ((c = in.read()) != -1) {\n" +
				"\t\t\t\t//System.out.print((char) c); можем направить в консоль поток\n" +
				"\t\t\t\tif(c < 65)out.write(c);\n" +
				"\t\t\t}\n" +
				"\t\t} catch (FileNotFoundException ex) {\n" +
				"//\t\t\tLogger.getLogger(FileInputOutputStreamFourthClass.class.getName()).\n" +
				"//\t\t\t\t\tlog(Level.SEVERE, null, ex);\n" +
				"\t\t\tSystem.out.println(\"File not found\");\n" +
				"\t\t}\n" +
				"\t\tcatch (IOException ex) {\n" +
				"//\t\t\tLogger.getLogger(FileInputOutputStreamFourthClass.class.getName()).\n" +
				"//\t\t\t\t\tlog(Level.SEVERE, null, ex);\n" +
				"\t\t\tSystem.out.println(\"Error by input output procedure\");\n" +
				"\t\t}\n" +
				"\t\tfinally{\n" +
				"\t\t\ttry {\n" +
				"\t\t\t\tin.close();\n" +
				"\t\t\t\tout.close();\n" +
				"\t\t\t} catch (IOException ex) {\n" +
				"//\t\t\t\tLogger.getLogger(FileInputOutputStreamFourthClass.class.getName()).\n" +
				"//\t\t\t\t\t\tlog(Level.SEVERE, null, ex);\n" +
				"\t\t\t\tSystem.out.println(\"Closing streams error\");\n" +
				"\t\t\t}\n" +
				"\t\t}\n" +
				"\t}\n" +
				"}\n\n" +
				"" +
				"В этом примере мы снова выполняем чтение входного потока в цикле. Но теперь используем метод read(), который\n" +
				"читает один байт и возвращает код прочитанного байта. В этом же цикле мы выполняем простую проверку – если\n" +
				"прочитанный символ не буква, а цифра или разделитель, мы записываем его в файл, в противном случае – не за-\n" +
				"писываем. Возможно, этот пример надуманный, но он наглядно демонстрирует логику побайтовой обработки.\n\n");
	}

	@Override
	public String getTHEME() {
		return THEME;
	}
}
