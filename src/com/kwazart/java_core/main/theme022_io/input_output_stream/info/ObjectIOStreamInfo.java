package com.kwazart.java_core.main.theme022_io.input_output_stream.info;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 04.10.2020 23:02
 */

public class ObjectIOStreamInfo implements InfoPrintable {
	private static final String THEME = "ObjectInputStream и ObjectOutputStream";

	public void printInfo() {
		System.out.println("\n\nObjectInputStream и ObjectOutputStream.\n\n" +
				"Эти классы являются незаменимыми, когда вам надо записывать или читать объекты каких-либо классов. А такая\n" +
				"необходимость возникает очень часто. Рассмотрим примеры использования этих потоков.\n\n" +
				"" +
				"class Developer implements Serializable {\n" +
				"\tString language;\n" +
				"\tString name;\n" +
				"\tint salary;\n" +
				"\n" +
				"\tpublic Developer( String name, String language, int salary) {\n" +
				"\t\tthis.language = language;\n" +
				"\t\tthis.name = name;\n" +
				"\t\tthis.salary = salary;\n" +
				"\t}\n" +
				"\n" +
				"\t@Override\n" +
				"\tpublic String toString() {\n" +
				"\t\treturn \"Developer{\" +\n" +
				"\t\t\t\t\"language='\" + language + '\\'' +\n" +
				"\t\t\t\t\", name='\" + name + '\\'' +\n" +
				"\t\t\t\t\", salary=\" + salary +\n" +
				"\t\t\t\t'}';\n" +
				"\t}\n" +
				"}\n" +
				"\n" +
				"// запись объекта в файл\n" +
				"public class ObjectInputOutputStream1Class {\n" +
				"\tpublic static void main(String[] args) {\n" +
				"\t\tFileOutputStream fout=null;\n" +
				"\t\tObjectOutputStream oout=null;\n" +
				"\t\ttry {\n" +
				"\t\t\tfout = new FileOutputStream(new File(\"test.txt\"));\n" +
				"\n" +
				"\t\t\tDeveloper d = new Developer(\"Artem\", \"Java\", 100000);\n" +
				"\t\t\toout = new ObjectOutputStream(fout);\n" +
				"\t\t\toout.writeObject(d); // для этого классу необхожимо реализовать интерфейс Serializable\n" +
				"\t\t\tSystem.out.println(\"Wrote - \" + d);\n" +
				"\t\t} catch (FileNotFoundException ex) {\n" +
				"\t\t\t//Logger.getLogger(ObjectInputOutputStream1Class.class.getName()).log(Level.SEVERE, null, ex);\n" +
				"\t\t\tSystem.out.println(\"cannot find the file\");\n" +
				"\t\t} catch (IOException ex) {\n" +
				"\t\t\t//Logger.getLogger(ObjectInputOutputStream1Class.class.getName()).log(Level.SEVERE, null, ex);\n" +
				"\t\t\tSystem.out.println(\"input output error\");\n" +
				"\t\t}\n" +
				"\t\tfinally\n" +
				"\t\t{\n" +
				"\t\t\ttry {\n" +
				"\t\t\t\toout.close();\n" +
				"\t\t\t} catch (IOException ex) {\n" +
				"\t\t\t\t//Logger.getLogger(ObjectInputOutputStream1Class.class.getName()).log(Level.SEVERE, null, ex);\n" +
				"\t\t\t\tSystem.out.println(\"closing file error\");\n" +
				"\t\t\t}\n" +
				"\t\t}\n" +
				"\t}\n" +
				"}\n\n" +
				"" +
				"Разберем этот код. Сначала мы создаем уже знакомый нам объект потока FileOutputStream и связываем его\n" +
				"с файлом \"test.txt\", в который хотим записать объект класса Developer.\n" +
				"Потом создаем сам объект, который будем записывать. А вот теперь начинается нечто новое. Мы создаем объект потока\n" +
				"ObjectOutputStream вокруг ранее созданного объекта потока FileOutputStream. В таких случаях говорят, что поток\n" +
				"ObjectOutputStream оборачивает поток FileOutputStream. Теперь наш поток ObjectOutputStream тоже связан с файлом\n" +
				"\"test.txt\", но он обладает качествами, которые отсутствуют у потока FileOutputStream. Когда вы выполняете обертку\n" +
				"одного потока вокруг другого, вы добавляете потоку новые возможности. В нашем случае, мы теперь можем записывать\n" +
				"в поток объекты любых классов, потому, что у потока появился метод writeObject(), которого не было у потока\n" +
				"FileOutputStream. Запустите это приложение и обратите внимание на ошибку, которая возникла при выполнении.\n" +
				"Дело в том, что если мы хотим записывать объекты какого-либо класса в потоки, то этот класс должен\n" +
				"удовлетворять некоторым условиям. А именно – этот класс должен наследовать интерфейсу Serializable. Вы уже привыкли,\n" +
				"что если класс наследует интерфейсу, то он должен реализовать все методы, объявленные в интерфейсе. Однако\n" +
				"в Java существуют интерфейсы, к которых не объявлены никакие методы. Такие интерфейсы называются маркера-\n" +
				"ми. Интерфейс Serializable является маркером, поэтому в классе Developer достаточно добавить implements Serializable\n" +
				"и никаких методов реализовывать не надо. Добавьте в класс Developer наследование, запустите приложение и убедитесь, что\n" +
				"наш выходной файл создан и в него что-то записано. Не пытайтесь читать этот файл. Он бинарный. Мы с вами\n" +
				"подробно разберем то, что сейчас сделали в последнем разделе нашего урока, когда будем говорить о сериализа-\n" +
				"ции. Поэтому сейчас просто запомните: чтобы записывать и читать объекты класса, надо наследовать этот класс от\n" +
				"Serializable.\n" +
				"Давайте прочитаем то, что записали в файл \"test.txt\".\n\n" +
				"" +
				"// чтение объекта из файла\n" +
				"public class ObjectInputOutputStream2Class {\n" +
				"\tpublic static void main(String[] args) {\n" +
				"\t\tFileInputStream fin = null;\n" +
				"\t\ttry {\n" +
				"\t\t\tfin = new FileInputStream(new File(\"test.txt\"));\n" +
				"\t\t\tObjectInputStream oin = new ObjectInputStream(fin);\n" +
				"\t\t\tDeveloper d = (Developer) oin.readObject();\n" +
				"\t\t\tSystem.out.println(d);\n" +
				"\t\t} catch (FileNotFoundException ex) {\n" +
				"//\t\t\tLogger.getLogger(ObjectInputOutputStream2Class.class.getName()).log(Level.SEVERE, null, ex);\n" +
				"\t\t\tex.printStackTrace();\n" +
				"\t\t} catch (IOException ex) {\n" +
				"//\t\t\tLogger.getLogger(ObjectInputOutputStream2Class.class.getName()).log(Level.SEVERE, null, ex);\n" +
				"\t\t\tex.printStackTrace();\n" +
				"\t\t} catch (ClassNotFoundException ex) {\n" +
				"//\t\t\tLogger.getLogger(ObjectInputOutputStream2Class.class.getName()).log(Level.SEVERE, null, ex);\n" +
				"\t\t\tex.printStackTrace();\n" +
				"\t\t} finally {\n" +
				"\t\t\ttry {\n" +
				"\t\t\t\tfin.close();\n" +
				"\t\t\t} catch (IOException ex) {\n" +
				"//\t\t\t\tLogger.getLogger(ObjectInputOutputStream2Class.class.getName()).\n" +
				"//\t\t\t\t\t\tlog(Level.SEVERE, null, ex);\n" +
				"\t\t\t}\n" +
				"\t\t}\n" +
				"\t}\n" +
				"}\n\n" +
				"" +
				"В этом примере вам все должно быть понятно. Мы снова создаем поток ObjectInputStream как обертку вокруг\n" +
				"потока FileInputStream и используем метод readObject() для чтения объекта из файла. Поскольку этот метод\n" +
				"возвращает тип Object, необходимо выполнять явное преобразование типа к требуемому типу данных. Затем мы\n" +
				"выводим в консоль описание прочитанного объекта, чтобы убедиться, что чтение прошло успешно. В нашем\n" +
				"классе перегружен метод toString(), поэтому мы просто вызываем println().\n\n");
	}

	@Override
	public String getTHEME() {
		return THEME;
	}
}
