package com.kwazart.java_core.main.theme022_io.reader_writer.examples;

import java.io.*;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 01.10.2020 18:33
 */

// копируем текст из файла в другой файла с помощью FileReader and FileWriter
public class FileReaderWriter1Class {
	public static void main(String[] args) {
		FileReader fr = null;
		FileWriter fw = null;
		try {
			fr = new FileReader("text.txt");
			//fw = new FileWriter("text1.txt"); // перезаписывает файл
			fw = new FileWriter("text1.txt", true); // добавляет данные в конец файла
			BufferedReader br = new BufferedReader(fr);
			String line="";
			int lineCounter=0;
			while((line = br.readLine()) != null) {
				if( (lineCounter++) % 2 == 0) {
					System.out.println(line);
					fw.write(line+System.getProperty("line.separator"));
				}
			}
		} catch (FileNotFoundException ex) {
			//Logger.getLogger(Filetest.class.getName()).log(Level.SEVERE, null, ex);
			ex.printStackTrace();
		} catch (IOException ex) {
			//Logger.getLogger(FileReaderWriter1Class.class.getName()).log(Level.SEVERE, null, ex);
			ex.printStackTrace();
		} finally {
			try {
				fr.close();
				fw.close();
			} catch (IOException ex) {
				//Logger.getLogger(FileReaderWriter1Class.class.getName()).log(Level.SEVERE, null, ex);
				ex.printStackTrace();
			}
		}
	}
}
