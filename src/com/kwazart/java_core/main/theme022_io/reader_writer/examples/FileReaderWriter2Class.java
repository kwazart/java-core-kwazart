package com.kwazart.java_core.main.theme022_io.reader_writer.examples;

import java.io.*;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 01.10.2020 18:33
 */

// копируем текст из файла в другой файла с помощью FileReader and FileWriter + работа с отдельными символами
public class FileReaderWriter2Class {
	public static void main(String[] args) {
		FileReader fr = null;
		FileWriter fw = null;
		try {
			fr = new FileReader("text.txt");
			//fw = new FileWriter("text1.txt"); // перезаписывает файл
			fw = new FileWriter("text1.txt", true); // добавляет данные в конец файла
			BufferedReader br = new BufferedReader(fr);
			String line="";
			int lineCounter=0;
			while((line = br.readLine()) != null) {
				char buffer[] = new char[line.length()];
				line.getChars(0, line.length(), buffer, 0);
				for (int i=0; i < buffer.length; i ++) {
					if(buffer[i]=='е')
						fw.write('Е');
					else
						fw.write(buffer[i]);
				}
				fw.write("\n");
			}
		} catch (FileNotFoundException ex) {
			//Logger.getLogger(Filetest.class.getName()).log(Level.SEVERE, null, ex);
			ex.printStackTrace();
		} catch (IOException ex) {
			//Logger.getLogger(FileReaderWriter1Class.class.getName()).log(Level.SEVERE, null, ex);
			ex.printStackTrace();
		} finally {
			try {
				fr.close();
				fw.close();
			} catch (IOException ex) {
				//Logger.getLogger(FileReaderWriter1Class.class.getName()).log(Level.SEVERE, null, ex);
				ex.printStackTrace();
			}
		}
	}
}
