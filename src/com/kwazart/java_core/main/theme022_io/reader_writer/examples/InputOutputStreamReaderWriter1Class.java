package com.kwazart.java_core.main.theme022_io.reader_writer.examples;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 01.10.2020 21:36
 */

// чтение файла с указание кодировки
public class InputOutputStreamReaderWriter1Class {
	public static void main(String[] args) {
		try (FileInputStream fis = new FileInputStream(new File("lines.txt"));
			 // необходимо задавать кодировку входящего файла
			 InputStreamReader reader = new InputStreamReader(fis, "windows-1251");
			 FileOutputStream fs = new FileOutputStream(new File("lines2.txt"));
			 // необходимо задавать кодировку исходящего файла
			 OutputStreamWriter writer = new OutputStreamWriter(fs, "UTF-8")) {
			int c;
			while ((c = reader.read()) != -1) {
				System.out.print((char) c);
				writer.write(c);
			}
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
//			Logger.getLogger(InputOutputStreamReaderWriter1Class.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			ex.printStackTrace();
//			Logger.getLogger(InputOutputStreamReaderWriter1Class.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
