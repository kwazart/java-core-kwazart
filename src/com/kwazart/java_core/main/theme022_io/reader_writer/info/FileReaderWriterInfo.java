package com.kwazart.java_core.main.theme022_io.reader_writer.info;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 04.10.2020 23:21
 */

public class FileReaderWriterInfo implements InfoPrintable {
	private static final String THEME = "FileReader и FileWriter";

	public void printInfo() {
		System.out.println("\n\nFileReader и FileWriter\n\n" +
				"Пожалуй, самым удобным способом читать текстовые файлы является использование класса FileReader.\n" +
				"Правда, при этом необходимо учитывать, что этот класс будет читать файл только в вашей текущей кодировке\n" +
				"и изменить ее не сможет. Как изменить кодировку, мы посмотрим в следующем примере. Давайте сейчас\n" +
				"прочитаем какой-нибудь текстовый файл и создадим его копию, записав в нее только строки с нечетными\n" +
				"номерами.\n\n" +
				"" +
				"// копируем текст из файла в другой файла с помощью FileReader and FileWriter\n" +
				"public class FileReaderWriter1Class {\n" +
				"\tpublic static void main(String[] args) {\n" +
				"\t\tFileReader fr = null;\n" +
				"\t\tFileWriter fw = null;\n" +
				"\t\ttry {\n" +
				"\t\t\tfr = new FileReader(\"text.txt\");\n" +
				"\t\t\t//fw = new FileWriter(\"text1.txt\"); // перезаписывает файл\n" +
				"\t\t\tfw = new FileWriter(\"text1.txt\", true); // добавляет данные в конец файла\n" +
				"\t\t\tBufferedReader br = new BufferedReader(fr);\n" +
				"\t\t\tString line=\"\";\n" +
				"\t\t\tint lineCounter=0;\n" +
				"\t\t\twhile((line = br.readLine()) != null) {\n" +
				"\t\t\t\tif( (lineCounter++) % 2 == 0) {\n" +
				"\t\t\t\t\tSystem.out.println(line);\n" +
				"\t\t\t\t\tfw.write(line+System.getProperty(\"line.separator\"));\n" +
				"\t\t\t\t}\n" +
				"\t\t\t}\n" +
				"\t\t} catch (FileNotFoundException ex) {\n" +
				"\t\t\t//Logger.getLogger(Filetest.class.getName()).log(Level.SEVERE, null, ex);\n" +
				"\t\t\tex.printStackTrace();\n" +
				"\t\t} catch (IOException ex) {\n" +
				"\t\t\t//Logger.getLogger(FileReaderWriter1Class.class.getName()).log(Level.SEVERE, null, ex);\n" +
				"\t\t\tex.printStackTrace();\n" +
				"\t\t} finally {\n" +
				"\t\t\ttry {\n" +
				"\t\t\t\tfr.close();\n" +
				"\t\t\t\tfw.close();\n" +
				"\t\t\t} catch (IOException ex) {\n" +
				"\t\t\t\t//Logger.getLogger(FileReaderWriter1Class.class.getName()).log(Level.SEVERE, null, ex);\n" +
				"\t\t\t\tex.printStackTrace();\n" +
				"\t\t\t}\n" +
				"\t\t}\n" +
				"\t}\n" +
				"}\n\n" +
				"" +
				"В этом примере вам все уже знакомо. Создаем два потока для чтения и записи. Связываем входной поток\n" +
				"с существующим файлом \"lines.txt\", а выходному потоку указываем имя файла \"lines1.txt\", который\n" +
				"должен быть создан. Оборачиваем входной поток потоком BufferedReader для создания буферизации. Самое\n" +
				"интересное происходит в цикле, где выполняется чтение. Мы вызываем метод readLine() который за один вызов\n" +
				"считывает из файла текущую строку, возвращает ее в переменную line и перемещает указатель в файле на\n" +
				"следующую строку. Этот метод мы получили из потока BufferedReader. Когда все строки файла будут прочитаны,\n" +
				"readLine() занесет в line значение null и цикл остановится.\n" +
				"Запустите наше приложение несколько раз подряд, а затем откройте выходной файл \"lines1.txt\". Его содержимое\n" +
				"будет таким же, как и после первого запуска. Это значит, что при каждом запуске кода этот файл перезаписывается.\n" +
				"А можно ли сделать так, чтобы при повторных запусках приложения выполнялось дозаписывание в файл? Да, это\n" +
				"сделать очень просто. Добавьте в строке создания потока\n\n" +
				"fw второй параметр со значением true:\n\n" +
				"\tfw = new FileWriter(\"lines1.txt\", true);\n\n" +
				"Выполните приложение и убедитесь, что теперь новые строки добавляются в файл \"lines1.txt\".\n\n" +
				"" +
				"В этом примере мы работали целиком со строками. Однако FileReader и FileWriter позволяют работать с\n" +
				"символами на \"низком уровне\". Давайте немного изменим код записи нашего файла. Будем проверять все символы\n" +
				"в строке и заменять \"e\" на \"E\".\n\n" +
				"" +
				"// копируем текст из файла в другой файла с помощью FileReader and FileWriter + работа с отдельными символами\n" +
				"public class FileReaderWriter2Class {\n" +
				"\tpublic static void main(String[] args) {\n" +
				"\t\tFileReader fr = null;\n" +
				"\t\tFileWriter fw = null;\n" +
				"\t\ttry {\n" +
				"\t\t\tfr = new FileReader(\"text.txt\");\n" +
				"\t\t\t//fw = new FileWriter(\"text1.txt\"); // перезаписывает файл\n" +
				"\t\t\tfw = new FileWriter(\"text1.txt\", true); // добавляет данные в конец файла\n" +
				"\t\t\tBufferedReader br = new BufferedReader(fr);\n" +
				"\t\t\tString line=\"\";\n" +
				"\t\t\tint lineCounter=0;\n" +
				"\t\t\twhile((line = br.readLine()) != null) {\n" +
				"\t\t\t\tchar buffer[] = new char[line.length()];\n" +
				"\t\t\t\tline.getChars(0, line.length(), buffer, 0);\n" +
				"\t\t\t\tfor (int i=0; i < buffer.length; i ++) {\n" +
				"\t\t\t\t\tif(buffer[i]=='е')\n" +
				"\t\t\t\t\t\tfw.write('Е');\n" +
				"\t\t\t\t\telse\n" +
				"\t\t\t\t\t\tfw.write(buffer[i]);\n" +
				"\t\t\t\t}\n" +
				"\t\t\t\tfw.write(\"\\n\");\n" +
				"\t\t\t}\n" +
				"\t\t} catch (FileNotFoundException ex) {\n" +
				"\t\t\t//Logger.getLogger(Filetest.class.getName()).log(Level.SEVERE, null, ex);\n" +
				"\t\t\tex.printStackTrace();\n" +
				"\t\t} catch (IOException ex) {\n" +
				"\t\t\t//Logger.getLogger(FileReaderWriter1Class.class.getName()).log(Level.SEVERE, null, ex);\n" +
				"\t\t\tex.printStackTrace();\n" +
				"\t\t} finally {\n" +
				"\t\t\ttry {\n" +
				"\t\t\t\tfr.close();\n" +
				"\t\t\t\tfw.close();\n" +
				"\t\t\t} catch (IOException ex) {\n" +
				"\t\t\t\t//Logger.getLogger(FileReaderWriter1Class.class.getName()).log(Level.SEVERE, null, ex);\n" +
				"\t\t\t\tex.printStackTrace();\n" +
				"\t\t\t}\n" +
				"\t\t}\n" +
				"\t}\n" +
				"}\n\n" +
				"" +
				"В этом примере мы преобразовываем прочитанную из файла строку в символьный массив, затем в цикле\n" +
				"просматриваем все символы в созданном массиве и выполняем необходимую обработку. Мы снова вызываем для\n" +
				"записи метод write(), но это уже не тот write(), который мы использовали перед этим. Тогда мы передавали этому\n" +
				"методу строку, а сейчас – char.\n\n");
	}

	@Override
	public String getTHEME() {
		return THEME;
	}
}
