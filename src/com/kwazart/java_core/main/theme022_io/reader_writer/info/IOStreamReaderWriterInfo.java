package com.kwazart.java_core.main.theme022_io.reader_writer.info;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 04.10.2020 23:31
 */

public class IOStreamReaderWriterInfo implements InfoPrintable {
	private static final String THEME = "InputStreamReader и OutputStreamWriter";

	public void printInfo() {
		System.out.println("\n\nInputStreamReader и OutputStreamWriter\n\n" +
				"" +
				"Перейдем к рассмотрению потоков, которые позволяют работать с текстовыми файлами в символьном режиме\n" +
				"и при этом управлять кодировкой. Вы уже понимаете, что тип String в Java предназначен для работы с текстом,\n" +
				"а тип byte[] – для работы с бинарным контентом. Тот факт, что мы с вами читали текстовые файлы с помощью\n" +
				"байтовых потоков, не должен вводить вас в заблуждение. Байты – это не текст. Но байты могут представлять собой\n" +
				"символы текста в какой-нибудь кодировке. И для того, чтобы правильно получить из байт то, что они представ-\n" +
				"ляют, надо уметь работать с кодировками. Все случаи, рассмотренные нами до сих пор, использовали кодировку\n" +
				"по умолчанию. У меня это – UTF–8. У большинства из вас, я полагаю – тоже. Проделайте такой эксперимент.\n" +
				"Сохраните входной файл \"lines.txt\" в кодировке, отличной от UTF–8. Например – в ANSI. И еще занесите в этот\n" +
				"файл пару строк не на английском языке. Выполните нашеприложение еще раз и посмотрите, что будет выведено\n" +
				"в консольное окно и что будет записано в выходной файл \"lines1.txt\". Вы увидите и там и там нечитаемые символы.\n" +
				"Почему это произошло? Потому, что потоки FileReader и FileWriter считают, что все символы закодированы\n" +
				"в UTF–8, а на самом деле у нас сейчас символы в другой кодировке. Прочитать символы, записанные в кодировке,\n" +
				"отличной от кодировки по умолчанию поможет поток InputStreamReader. А записать символы в другой кодировке\n" +
				"поможет поток OutputStreamWriter. Эти потоки превращают байтовый ввод-вывод в символьный, позволяя при\n" +
				"этом указывать требуемую кодировку.\n\n" +
				"" +
				"// чтение файла с указание кодировки\n" +
				"public class InputOutputStreamReaderWriter1Class {\n" +
				"\tpublic static void main(String[] args) {\n" +
				"\t\ttry (FileInputStream fis = new FileInputStream(new File(\"lines.txt\"));\n" +
				"\t\t\t // необходимо задавать кодировку входящего файла\n" +
				"\t\t\t InputStreamReader reader = new InputStreamReader(fis, \"windows-1251\");\n" +
				"\t\t\t FileOutputStream fs = new FileOutputStream(new File(\"lines2.txt\"));\n" +
				"\t\t\t // необходимо задавать кодировку исходящего файла\n" +
				"\t\t\t OutputStreamWriter writer = new OutputStreamWriter(fs, \"UTF-8\")) {\n" +
				"\t\t\tint c;\n" +
				"\t\t\twhile ((c = reader.read()) != -1) {\n" +
				"\t\t\t\tSystem.out.print((char) c);\n" +
				"\t\t\t\twriter.write(c);\n" +
				"\t\t\t}\n" +
				"\t\t} catch (FileNotFoundException ex) {\n" +
				"\t\t\tex.printStackTrace();\n" +
				"//\t\t\tLogger.getLogger(InputOutputStreamReaderWriter1Class.class.getName()).log(Level.SEVERE, null, ex);\n" +
				"\t\t} catch (IOException ex) {\n" +
				"\t\t\tex.printStackTrace();\n" +
				"//\t\t\tLogger.getLogger(InputOutputStreamReaderWriter1Class.class.getName()).log(Level.SEVERE, null, ex);\n" +
				"\t\t}\n" +
				"\t}\n" +
				"}\n\n" +
				"" +
				"Обратите внимание, как создается поток InputStreamReader. Он создается как обертка вокруг байтового потока\n" +
				"FileInputStream, но при создании мы указываем конструктору InputStreamReader требуемую нам кодировку. Я\n" +
				"указал \"windows-1251\" потому, что это кодовая страница для кириллицы, которую я использовал в файле \"lines.txt\".\n" +
				"Если вы использовали другую кодировку – укажите имя кодовой страницы для нее. Например, для английского\n" +
				"языка в ANSI кодировке имя кодовой страницы будет \"windows-1252\". В приведенном примере мы читаем\n" +
				"символы в ANSI, а записываем их уже в UTF–8. Запустите этот пример и убедитесь, что и в консольном окне и в выходном\n" +
				"файле \"lines2.txt\" будут читабельные символы, а сам этот файл будет в кодировке UTF–8.\n\n" +
				"" +
				"В предыдущем примере мы читали входной файл и записывали выходной по одному символу. Однако, мы\n" +
				"могли бы воспользоваться буферизацией и получить за это возможность читать и писать построчно. Другими\n" +
				"словами, мы можем создать поток BufferedReader вокруг потока InputStreamReader и получить в свое распоряжение\n" +
				"метод readLine(). Подобным образом мы можем создать поток BufferedWriter вокруг потока OutputStreamWriter\n" +
				"и получить возможность пользоваться методом write(), выполняющим запись строками. Посмотрите на немного\n" +
				"измененный код предыдущего примера. Обратите внимание, что правильная обработка кодировок остается.\n\n" +
				"" +
				"// чтение файла с указание кодировки\n" +
				"public class InputOutputStreamReaderWriter2Class {\n" +
				"\tpublic static void main(String[] args) {\n" +
				"\n" +
				"\t\ttry (FileInputStream fis = new FileInputStream(new File(\"lines.txt\"));\n" +
				"\t\t\t // необходимо задавать кодировку входящего файла\n" +
				"\t\t\t InputStreamReader reader = new InputStreamReader(fis, \"windows-1251\");\n" +
				"\t\t\t BufferedReader br = new BufferedReader(reader);\n" +
				"\t\t\t FileOutputStream fs = new FileOutputStream(new File(\"lines2.txt\"));\n" +
				"\t\t\t // необходимо задавать кодировку исходящего файла\n" +
				"\t\t\t OutputStreamWriter writer = new OutputStreamWriter(fs, \"UTF-8\");\n" +
				"\t\t\t BufferedWriter bw = new BufferedWriter(writer))\n" +
				"\t\t{\n" +
				"\t\t\tString line;\n" +
				"\t\t\t\twhile ((line = br.readLine()) != null) {\n" +
				"\t\t\t\t\tSystem.out.println(line);\n" +
				"\t\t\t\t\tbw.write(line + System.getProperty(\"line.separator\"));\n" +
				"\t\t\t\t}\n" +
				"\n" +
				"\t\t} catch (FileNotFoundException ex) {\n" +
				"\t\t\tex.printStackTrace();\n" +
				"//\t\t\tLogger.getLogger(InputOutputStreamReaderWriter1Class.class.getName()).log(Level.SEVERE, null, ex);\n" +
				"\t\t} catch (IOException ex) {\n" +
				"\t\t\tex.printStackTrace();\n" +
				"//\t\t\tLogger.getLogger(InputOutputStreamReaderWriter1Class.class.getName()).log(Level.SEVERE, null, ex);\n" +
				"\t\t}\n" +
				"\t}\n" +
				"}\n");
	}

	@Override
	public String getTHEME() {
		return THEME;
	}
}
