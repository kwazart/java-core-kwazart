package com.kwazart.java_core.main.theme022_io.system;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 01.10.2020 21:56
 */

// ввод данных с клавиатуры
public class System1Class {
	public static void main(String[] args) {
		System.out.print("Enter some symbols: ");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String line = null;
		try {
			line = br.readLine();
		} catch (IOException e) {
			System.err.print("IO error");
			e.printStackTrace();
		}
		System.out.println(line);
	}
}
