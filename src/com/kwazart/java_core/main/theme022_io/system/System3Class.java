package com.kwazart.java_core.main.theme022_io.system;

import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintStream;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 01.10.2020 22:02
 */

// перенаправление стандартного потока вывода из System
public class System3Class {
	public static void main(String[] args) {
		System.out.println("Before redirecting");
		try {
			System.setOut(new PrintStream(new FileOutputStream("out.txt")));
			System.out.println("The output is redirected into file now!");
		} catch(Exception e) {
			System.err.println("File opening error:");
			e.printStackTrace();
		}

		// теперь весь вывод пойдет в указанный файл
		System.out.println(1);
		System.out.println(true);

		try {
			// возвращаем обратно консоль для стандартного вывода
			System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
		} catch (Exception e) {
			System.out.println("cannot add new out");
		}

		System.out.println("after re-redirecting");
	}
}
