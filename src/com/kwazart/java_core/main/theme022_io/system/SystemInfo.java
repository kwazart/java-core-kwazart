package com.kwazart.java_core.main.theme022_io.system;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 04.10.2020 23:37
 */

public class SystemInfo implements InfoPrintable {
	private static final String THEME = "System.in, System.out и System.error";

	public void printInfo() {
		System.out.println("\n\nSystem.in, System.out и System.error\n\n" +
				"" +
				"В java существует три потока, которые создаются автоматически при запуске JVM. Другими словами, вам не\n" +
				"надо в своих программах создавать объекты этих потоков вно, они уже созданы в классе System. Класс System\n" +
				"является final классом и в нем определены интересующие нас три статических поля: in, out и err.\n\n" +
				"" +
				"public final class System {\n" +
				"\tstatic PrintStream out;\n" +
				"\ttatic PrintStream err;\n" +
				"\tstatic InputStream in;\n" +
				"\t...\n" +
				"}\n\n" +
				"" +
				"Каждый из этих потоков выполняет предопределенную роль:\n\n" +
				"- System.in предназначен для ввода в приложение данных, заносимых с клавиатуры;\n" +
				"- System.out предназначен для вывода данных из приложения в консольное окно;\n" +
				"- System.err предназначен для вывода данных об ошибках в консольное окно. Некоторые среды разработки, как\n" +
				"  например Eclipse, выводят сообщения этого потока красным цветом.\n\n" +
				"" +
				"Несмотря на специфику этих потоков, поскольку они также являются объектами потоковых классов, то могут\n" +
				"взаимодействовать с другими потоками, оборачиваться ими и т.п. Рассмотрим несколько примеров.\n" +
				"Ввод данных с клавиатуры в String:\n\n" +
				"" +
				"// ввод данных с клавиатуры\n" +
				"public class System1Class {\n" +
				"\tpublic static void main(String[] args) {\n" +
				"\t\tSystem.out.print(\"Enter some symbols: \");\n" +
				"\t\tBufferedReader br = new BufferedReader(new InputStreamReader(System.in));\n" +
				"\t\tString line = null;\n" +
				"\t\ttry {\n" +
				"\t\t\tline = br.readLine();\n" +
				"\t\t} catch (IOException e) {\n" +
				"\t\t\tSystem.err.print(\"IO error\");\n" +
				"\t\t\te.printStackTrace();\n" +
				"\t\t}\n" +
				"\t\tSystem.out.println(line);\n" +
				"\t}\n" +
				"}\n\n" +
				"" +
				"Создаем обертку InputStreamReader вокруг System.in, а затем вокруг этого, добавляем еще обертку BufferedReader,\n" +
				"чтобы иметь возможность работать со строками. Теперь, символы, вводимые с клавиатуры, будут заноситься в\n" +
				"строку line. В этом примере мы используем для чтения метод readLine(), который позволяет читать сразу вводимую\n" +
				"строку. Еще можно использовать метод read(), который считывает только один введенный с клавиатуры символ\n" +
				"и возвращает int код этого символа.\n\n" +
				"" +
				"Вывод в консольное окно потоками System.out и System. err вы уже видели много раз. В указанном примере мы\n" +
				"используем вывод в out при нормальном выполнении кода, и вывод в err – при возникновении исключения:\n\n" +
				"" +
				"try {\n" +
				"\tInputStream input = new FileInputStream(\"users.txt\");\n" +
				"\tSystem.out.println(\"File opened successfully!\");\n" +
				"} catch (IOException e){\n" +
				"\tSystem.err.println(\"File opening error:\");\n" +
				"\te.printStackTrace();\n" +
				"}\n\n" +
				"" +
				"Как вы видите, поток out связан с консольным окном, он инициализируется так по умолчанию. Однако,\n" +
				"при необходимости out можно перенаправить методом setOut(), например в файл:\n\n" +
				"" +
				"// перенаправление стандартного потока вывода из System\n" +
				"public class System2Class {\n" +
				"\tpublic static void main(String[] args) {\n" +
				"\t\ttry {\n" +
				"\t\t\tSystem.setOut(new PrintStream(new FileOutputStream(\"out.txt\")));\n" +
				"\t\t\tSystem.out.println(\"The output is redirected into file now!\");\n" +
				"\t\t} catch(Exception e) {\n" +
				"\t\t\tSystem.err.println(\"File opening error:\");\n" +
				"\t\t\te.printStackTrace();\n" +
				"\t\t}\n" +
				"\n" +
				"\t\t// теперь весь вывод пойдет в указанный файл\n" +
				"\t\tSystem.out.println(1);\n" +
				"\t\tSystem.out.println(true);\n" +
				"\n" +
				"\t\ttry {\n" +
				"\t\t\tSystem.setOut(new PrintStream(System.out));\n" +
				"\t\t} catch (Exception e) {\n" +
				"\t\t\tSystem.out.println(\"cannot add new out\");\n" +
				"\t\t}\n" +
				"\n" +
				"\t\tSystem.out.println(\"I am here\");\n" +
				"\t}\n" +
				"}\n\n" +
				"" +
				"Теперь метод println() будет направлять свой вывод не в консольное окно, а в указанный файл.\n\n" +
				"Возвращаем поток вывода (один из способов):\n\n" +
				"" +
				"// перенаправление стандартного потока вывода из System\n" +
				"public class System3Class {\n" +
				"\tpublic static void main(String[] args) {\n" +
				"\t\tSystem.out.println(\"Before redirecting\");\n" +
				"\t\ttry {\n" +
				"\t\t\tSystem.setOut(new PrintStream(new FileOutputStream(\"out.txt\")));\n" +
				"\t\t\tSystem.out.println(\"The output is redirected into file now!\");\n" +
				"\t\t} catch(Exception e) {\n" +
				"\t\t\tSystem.err.println(\"File opening error:\");\n" +
				"\t\t\te.printStackTrace();\n" +
				"\t\t}\n" +
				"\n" +
				"\t\t// теперь весь вывод пойдет в указанный файл\n" +
				"\t\tSystem.out.println(1);\n" +
				"\t\tSystem.out.println(true);\n" +
				"\n" +
				"\t\ttry {\n" +
				"\t\t\t// возвращаем обратно консоль для стандартного вывода\n" +
				"\t\t\tSystem.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));\n" +
				"\t\t} catch (Exception e) {\n" +
				"\t\t\tSystem.out.println(\"cannot add new out\");\n" +
				"\t\t}\n" +
				"\n" +
				"\t\tSystem.out.println(\"after re-redirecting\");\n" +
				"\t}\n" +
				"}\n\n");
	}

	@Override
	public String getTHEME() {
		return THEME;
	}
}
