package com.kwazart.java_core.main.theme023_serializable;

import com.kwazart.java_core.main.theme023_serializable.model.DeveloperExt;

import java.io.*;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 05.10.2020 1:14
 */

public class ExternalizeClass {
	public static void main(String[] args) {
		String fileName = "test1.txt";
		DeveloperExt developerExt = new DeveloperExt("Bill", 35, "Java", 50000);
		DeveloperExt developerExt1 = new DeveloperExt("Bill1", 35, "Java1", 50000);
		serializeEx(developerExt, developerExt1, fileName);
		deserializeEx(fileName);
	}

	public static void serializeEx(DeveloperExt obj, DeveloperExt obj1, String fileName) { // указываем явно класс объекта
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(fileName);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(obj);
			oos.writeObject(obj1);
			oos.close();
		} catch (FileNotFoundException e) {
			System.err.println("File not found");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("IO error");
			e.printStackTrace();
		} finally {
			try {
				fos.close();
			} catch (IOException e) {
				System.err.println("IO error by closing");
				e.printStackTrace();
			}
		}
	}

	public static void deserializeEx(String fileName) {
		Object obj = null;
		Object obj1 = null;
		try (FileInputStream fis = new FileInputStream(fileName)) {
			ObjectInputStream ois = new ObjectInputStream(fis);
			obj = ois.readObject();
			obj1 = ois.readObject();
			ois.close();
		} catch (FileNotFoundException e) {
			System.err.println("File not found");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("IO error");
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.err.println("Class not found");
			e.printStackTrace();
		}
		System.out.println(obj);
		System.out.println(obj1);
	}
}
