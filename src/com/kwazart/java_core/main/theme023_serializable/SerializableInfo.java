package com.kwazart.java_core.main.theme023_serializable;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 05.10.2020 0:21
 */

public class SerializableInfo implements InfoPrintable {
	private static final String THEME = "Сериализация и десериализация";

	public void printInfo() {
		System.out.println("\n\nСериализация – это процесс превращения объекта какого-либо класса в байтовый поток. Этот поток,\n" +
				"помимо данных объекта, будет содержать информацию о типе объекта и о типе данных, хранящихся в объекте.\n" +
				"Объект, превращенный в поток, можно записать в файл, передать по сети, занести в таблицу БД. Затем,\n" +
				"сериализованный объект можно будет извлечь из потока. Процесс превращения потока в объект в оперативной памяти,\n" +
				"называется десериализацией. Необходимо отметить, что и сериализация и десериализация не независят от\n" +
				"JVM. Это значит, что можно сериализовать объект на одной платформе, а затем десериализовать его на другой\n" +
				"платформе.\n\n" +
				"В Java существует два способа выполнения сериализации: использование интерфейса Seriallizable и\n" +
				"использование интерфейса Externalizable.\n\n");

		System.out.println("\n\nИнтерфейс Serializable\n" +
				"Рассмотрим первый способ сериализации. Если вы хотите иметь возможность сериализовать объекты какого-либо\n" +
				"класса, вы должны наследовать этот класс от интерфейса–маркера Serializable. Этот интерфейс не\n" +
				"описывает никаких методов, поэтому дополнительно реализовывать в классе ничего не надо.\n\n" +
				"" +
				"Полное описание этого интерфейса можете посмотреть на официальном сайте по адресу:\n" +
				"http://docs.oracle.com/javase/8/docs/api/java/io/Serializable.html.\n\n" +
				"" +
				"Для выполнения сериализации и десериализации в Java используются потоки ObjectInputStream и ObjectOutputStream.\n" +
				"Посмотрим, как это работает. Создадим два метода. Один будет выполнять сериализацию. Этот метод будет получать\n" +
				"в качестве параметра объект, который надо сериализировать и имя файла, в который надо записать сериализованный\n" +
				"объект. Второй метод будет получать имя файла, содержащего сериализованный объект, и возвращать объект,\n" +
				"десериализованный из этого файла.\n\n" +
				"" +
				"public static void serialize(Object obj, String fileName) {\n" +
				"\t\tFileOutputStream fos = null;\n" +
				"\t\ttry {\n" +
				"\t\t\tfos = new FileOutputStream(fileName);\n" +
				"\t\t\tObjectOutputStream oos = new ObjectOutputStream(fos);\n" +
				"\t\t\toos.writeObject(obj);\n" +
				"\t\t} catch (FileNotFoundException e) {\n" +
				"\t\t\tSystem.err.println(\"File not found\");\n" +
				"\t\t\te.printStackTrace();\n" +
				"\t\t} catch (IOException e) {\n" +
				"\t\t\tSystem.err.println(\"IO error\");\n" +
				"\t\t\te.printStackTrace();\n" +
				"\t\t} finally {\n" +
				"\t\t\ttry {\n" +
				"\t\t\t\tfos.close();\n" +
				"\t\t\t} catch (IOException e) {\n" +
				"\t\t\t\tSystem.err.println(\"IO error by closing\");\n" +
				"\t\t\t\te.printStackTrace();\n" +
				"\t\t\t}\n" +
				"\t\t}\n" +
				"\t}\n\n" +
				"" +
				"Логика работы этого метода для сериализации проста. Создается байтовый поток для вывода в файл. Вокруг\n" +
				"этого потока создается поток ObjectOutputStream. Затем выполняется запись в поток переданного объекта. Метод\n" +
				"универсальный и будет работать с объектами любого типа. Запись выполняется методом writeObject(), который мы\n" +
				"рассмотрели выше.\n\n" +
				"" +
				"public static Object deserialize(String fileName) {\n" +
				"\t\tObject obj = null;\n" +
				"\t\ttry (FileInputStream fis = new FileInputStream(fileName)) {\n" +
				"\t\t\tObjectInputStream ois = new ObjectInputStream(fis);\n" +
				"\t\t\tobj = ois.readObject();\n" +
				"\t\t\tois.close();\n" +
				"\t\t} catch (FileNotFoundException e) {\n" +
				"\t\t\tSystem.err.println(\"File not found\");\n" +
				"\t\t\te.printStackTrace();\n" +
				"\t\t} catch (IOException e) {\n" +
				"\t\t\tSystem.err.println(\"IO error\");\n" +
				"\t\t\te.printStackTrace();\n" +
				"\t\t} catch (ClassNotFoundException e) {\n" +
				"\t\t\tSystem.err.println(\"Class not found\");\n" +
				"\t\t\te.printStackTrace();\n" +
				"\t\t}\n" +
				"\t\treturn obj;\n" +
				"\t}\n\n" +
				"" +
				"Метод для десериализации выполняет обратную работу. Создает байтовый поток для чтения файла, оборачивает\n" +
				"этот поток потоком ObjectInputStream, и выполняет чтение методом readObject(). Запускаем:\n\n" +
				"" +
				"\tpublic static void main(String[] args) {\n" +
				"\t\tString fileName = \"test.txt\";\n" +
				"\t\tDeveloper developer = new Developer(\"Tommy\", \"Black\", 30, \"Java\", 100000);\n" +
				"\t\tserialize(developer, fileName);\n" +
				"\t\tDeveloper newDeveloper = (Developer) deserialize(fileName);\n" +
				"\t\tSystem.out.println(newDeveloper);\n" +
				"\t}\n\n" +
				"" +
				"Иногда в литературе в связи с сериализаций можно встретить термин персистентность. Что это такое? Обычно,\n" +
				"объект существует в оперативной памяти максимум до тех пор, пока выполняется приложение. При завершении\n" +
				"приложения мусорщик удаляет в оперативной памяти все объекты. Так вот, сериализация позволяет сохранить\n" +
				"объект в файл, а затем позже десериализовать его и продолжить работу с объектом. Таким образом, жизненный\n" +
				"цикл объекта уже не ограничивается временем выполнения приложения. Это и есть персистентность – продление\n" +
				"времени жизни объекта за пределы времени выполнения приложения.\n\n" +
				"" +
				"Как видите, в самой сериализации нет ничего сложного. Однако, необходимо учитывать некоторые детали.\n" +
				"Сериализовать можно только объекты классов, которые наследуют интерфейсу Serializable. При этом, если\n" +
				"в вашем классе есть поля типа других классов, они тоже должны наследовать интерфейс Serializable, чтобы\n" +
				"сериализация выполнилась успешно. А что произойдет, если в классе будут поля несериазлизуемого типа? Например,\n" +
				"если бы в нашем классе Developer, было поле типа Thread, то при попытке сериализовать объект такого класса, мы\n" +
				"бы получили исключение NotSerializableException. Это случилось бы потому, что класс Thread не наследует\n" +
				"интерфейсу Serializable и, следовательно, не может быть сериализован.\n\n" +
				"" +
				"В Java большинство классов поддерживают сериализацию, хотя есть и несериализуемые классы. Как быть\n" +
				"в ситуации, когда вам надо сериализовать объект класса, а у этого объекта есть несериализуемое поле? В этом случае,\n" +
				"такое несериализуемое поле можно обозначить спецификатором transient. Это приведет к тому, что объект будет\n" +
				"сериализован, но без этого поля. Этот же спецификатор можно использовать возле любых полей, которые вы не\n" +
				"хотите включать в сериализацию. Например, если бы мы не хотели сериализовать возраст разработчика, надо было бы в классе\n" +
				"пометить поле age спецификатором transient:\n\n" +
				"" +
				"public class Developer implements Serializable {\n" +
				"\tprivate String firstName;\n" +
				"\tprivate String surname;\n" +
				"\ttransient private int age;\n" +
				"\tprivate String language;\n" +
				"\tprivate int salary;\n" +
				"\t...\n" +
				"}\n\n" +
				"" +
				"Сериализация выполнилась бы успешно, но при десериализации поле age было бы равно нулю, т.е. инициализировано\n" +
				"значением по умолчанию для своего типа.\n" +
				"Если вам надо сериализовать несколько объектов за один раз, вы можете занести эти объекты в сериализуемую\n" +
				"коллекцию и сериализовать коллекцию целиком. Затем десериализовать ее и вытащить оттуда все элементы по\n" +
				"одному.\n\n" +
				"" +
				"Мы с вами рассмотрели основы сериализации с использованием интерфейса Serializable. Теперь заглянем\n" +
				"вовнутрь и поговорим о том, как все это работает, насколько это эффективно и безопасно. Как известно, The God\n" +
				"is in the details. Вот об этих мелочах сейчас и поговорим.\n\n" +
				"" +
				"Как выполняется процесс записи объекта в поток?Из объекта с помощью Reflection извлекается каждое\n" +
				"поле, проверяется не является ли оно transient и записывается в поток. При десериализации для воссоздаваемого\n" +
				"объекта выделяется необходимая память и в нее записываются значения сериализованных полей. Если\n" +
				"в объекте существуют transient поля, они записываются в десериализируемый объект со значениями по умолчанию\n" +
				"для своего типа. Обратите внимание, что при десериализации объекта конструктор не используется.\n" +
				"Надо еще учесть то, что static поля так же, как и transient, не сериализуются. А вот поля с модификатором final\n" +
				"сериализуются, как обычные.\n" +
				"А теперь предположим, что наш класс Developer наследуется от класса BaseDeveloper и интерфейса Serializable.\n" +
				"А вот родительский класс BaseDeveloper не наследует интерфейсу Serializable.\n" +
				"Мы хотим сериализвать объект дочернего класса Developer, который наследует несериализуемому базовому классу\n" +
				"BaseDeveloper. Выполнится ли такая сериализация? Сериализация выполнится в любом случае, но вот при\n" +
				"десериализации могут возникнуть ошибки. Дело в том, что при десериализации вызывается конструктор без параметров\n" +
				"родительского класса, а конструктор производного класса, как мы отметили ранее – не вызывается. Конструктор без\n" +
				"параметров родительского класса должен инициализировать поля родительского класса, переданные в дочерний класс.\n" +
				"А поля дочернего класса инициализируются не конструктором, а данными из потока. Поэтому, если у вас в базовом\n" +
				"классе BaseDeveloper не будет конструктора без параметров – вы получите ошибку. Если такой конструктор будет –\n" +
				"все выполнится успешно. Вспомните об этом, когда будете сериализовать объекты производных классов.\n\n" +
				"" +
				"Помните, что такое Java bean? Программисты, изучающие Java после знакомства с другими языками, часто\n" +
				"удивляются требованию оформлять свои классы, как Java bean. Я думаю, теперь вы понимаете, что Java bean – это\n" +
				"не просто требование стиля. В архитектуре языка Java для классов, оформленных, как Java bean становятся\n" +
				"доступными очень многие опции.\n\n" +
				"" +
				"Что можно сказать о безопасности данных при сериализации и десериализации? Вы уже должны привыкнуть\n" +
				"к тому, что когда создаете объект класса в конструкторе, то выполняете проверку присваиваемых полям данных.\n" +
				"Такую же проверку вы выполняете в сеттерах. Одно из ключевых требований ООП – контролируемый доступ\n" +
				"к данным класса. Другими словами, вы должны позаботиться о том, чтобы в каком-нибудь объекте класса\n" +
				"не оказалось 32 число какого-либо месяца, чтобы не оказалась отрицательная цена у товара или что-то в\n" +
				"таком роде. Но сейчас вы понимаете, что есть еще один источник появления объектов – десериализация. И это\n" +
				"накладывает на программиста некоторые новые обязанности. Дело в том, что вы не можете быть уверены\n" +
				"в правильности данных, из которых создаете десериализованный объект. Сериализованные данные можно\n" +
				"намеренно или ненамеренно изменить или повредить. Поэтому возьмите себе за правило: после выполнения\n" +
				"десериализации проверяйте правильность полученного объекта и если проверка не выполнилась – выбрасывайте\n" +
				"исключение.\n" +
				"Идем дальше. У нас сейчас в файле \"test.txt\" хранится сериализованный объект класса Developer.\n" +
				"Теперь представим, что после выполнения этой сериализации мы немного изменили класс Developer, например,\n" +
				"добавили такой метод для проверки зарплаты:\n\n" +
				"" +
				"public boolean checkSalary()\n" +
				"{\n" +
				"\treturn this.salary >= 100000;\n" +
				"}\n" +
				"Если вы сейчас попытаетесь выполнить десериализацию из файла \"test.txt\":\n" +
				"Developer newDeveloper = (Developer )deserialize(\"test.txt\");\n" +
				"System.out.println(newDeveloper);\n\n" +
				"" +
				"вы получите такую исключительную ситуацию:\n\n" +
				"" +
				"\tIO error\n" +
				"\tjava.io.InvalidClassException: com.kwazart.java_core.main.theme023_serializable.model.Developer; local class incompatible: stream classdesc serialVersionUID = -4597776508689486192, local class serialVersionUID = 1490225192805776987\n" +
				"\tat java.io.ObjectStreamClass.initNonProxy(ObjectStreamClass.java:699)\n" +
				"\tat java.io.ObjectInputStream.readNonProxyDesc(ObjectInputStream.java:1963)\n" +
				"\tat java.io.ObjectInputStream.readClassDesc(ObjectInputStream.java:1829)\n" +
				"\tat java.io.ObjectInputStream.readOrdinaryObject(ObjectInputStream.java:2120)\n" +
				"\tat java.io.ObjectInputStream.readObject0(ObjectInputStream.java:1646)\n" +
				"\tat java.io.ObjectInputStream.readObject(ObjectInputStream.java:482)\n" +
				"\tat java.io.ObjectInputStream.readObject(ObjectInputStream.java:440)\n" +
				"\tat com.kwazart.java_core.main.theme023_serializable.SerializeClass.deserialize(SerializeClass.java:47)\n" +
				"\tat com.kwazart.java_core.main.theme023_serializable.SerializeClass.main(SerializeClass.java:18)\n" +
				"\tnull\n\n" +
				"" +
				"Обратите внимание, мы не изменяли поля класса, просто добавили один метод! Почему же десериализация\n" +
				"не выполнилась? Дело в том, что в каждый класс, который наследует Serializable, на этапе компиляции автоматически\n" +
				"добавляется такое поле:\n\n" +
				"" +
				"private static final long serialVersionUID\n\n" +
				"" +
				"Это поле на основе содержимого класса, создает уникальный хеш идентификатор версии класса. При создании\n" +
				"этого идентификатора учитывается все: поля класса, методы класса и даже порядок их объявления в классе.\n" +
				"И если вы измените в классе хоть что-нибудь – идентификатор класса изменится и не пройдет проверку при\n" +
				"десериализации, выполненной с этим же классом до его изменения! Если вас не устраивает такое поведение\n" +
				"при десериализации, можно использовать следующую хитрость: явно объявите в своем классе такое поле и\n" +
				"инициализируйте его произвольным значением:\n\n" +
				"" +
				"private static final long serialVersionUID = 15325645745823598L;\n\n" +
				"" +
				"Такое поле не будет изменять свое значение при изменениях класса. В официальной документации Java,\n" +
				"начиная с 5 версии дается прямой совет: явно добавлять в класс, наследующий Serializable, поле serialVersionUID.\n" +
				"Поэтому у вас есть возможность выбора: оставить контроль версии класса при десериализации или нет.\n\n");

		System.out.println("\n\nИнтерфейс Externalizable\n\n" +
				"Мы с вами рассмотрели сериализацию и десериализацию с использованием интерфейса Serializable. Вы\n" +
				"видели, что в этом случае все происходит автоматически, и мы не можем прямо вмешиваться в процессы\n" +
				"сохранения объекта в поток и его извлечения из потока. Если такая ситуация нас не устраивает и мы хотим\n" +
				"полностью управлять процессами сериализации и десериализации, тогда в нашем распоряжении интерфейс\n" +
				"Externalizable.\n\n" +
				"" +
				"Полное описание этого интерфейса можете посмотреть на официальном сайте по адресу:\n" +
				"http://docs.oracle.com/javase/8/docs/api/java/io/Externalizable.html#writeExternaljava. io.ObjectOutput.\n\n" +
				"" +
				"В отличии от интерфейса Serializable, в котором не описанио ни одного метода, в Externalizable описано два\n" +
				"метода: writeExternal() и readExternal(). При реализации первого метода надо выполнить запись объекта в поток,\n" +
				"при реализации второго – чтение объекта из потока. При создании процесса записи и чтения мы можем выполнять\n" +
				"любую логику, которую считаем необходимой при сериализации и десериализации объектов нашего класса.\n" +
				"Рассмотрим пример, где поработаем с двойником класса\n\n" +
				"" +
				"public class DeveloperExt implements Externalizable {\n" +
				"\n" +
				"\tprivate String firstName;\n" +
				"\tprivate int age;\n" +
				"\tprivate String language;\n" +
				"\tprivate int salary;\n" +
				"\n" +
				"\tpublic DeveloperExt(String firstName, int age, String language, int salary) {\n" +
				"\t\tthis.firstName = firstName;\n" +
				"\t\tthis.age = age;\n" +
				"\t\tthis.language = language;\n" +
				"\t\tthis.salary = salary;\n" +
				"\t}\n" +
				"\n" +
				"\tpublic DeveloperExt() {\n" +
				"\t}\n" +
				"\n" +
				"\t@Override\n" +
				"\tpublic String toString() {\n" +
				"\t\treturn \"DeveloperExt{\" +\n" +
				"\t\t\t\t\"firstName='\" + firstName + '\\'' +\n" +
				"\t\t\t\t\", age=\" + age +\n" +
				"\t\t\t\t\", language='\" + language + '\\'' +\n" +
				"\t\t\t\t\", salary=\" + salary +\n" +
				"\t\t\t\t'}';\n" +
				"\t}\n" +
				"\n" +
				"\t@Override\n" +
				"\tpublic void writeExternal(ObjectOutput out) throws IOException {\n" +
				"\t\tSystem.out.println(\"Inside writeExternal\");\n" +
				"\t\tout.writeObject(firstName);\n" +
				"\t\tout.writeInt(age);\n" +
				"\t\tout.writeObject(language);\n" +
				"\t}\n" +
				"\n" +
				"\t@Override\n" +
				"\tpublic void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {\n" +
				"\t\tSystem.out.println(\"Inside readExternal\");\n" +
				"\t\tthis.firstName = (String) in.readObject();\n" +
				"\t\tthis.age = in.readInt();\n" +
				"\t\tthis.language = (String) in.readObject();\n" +
				"\t}\n" +
				"}\n\n" +
				"" +
				"Обратите внимание, что чтение объекта из потока в методе readExternal() должно коррелировать с записью\n" +
				"этого объекта в поток в методе writeExternal(). Если вы при чтении, например, перепутаете очередность\n" +
				"считывания полей – вы получите неверный объект при десериализации. Теперь создадим два метода, которые будут\n" +
				"выполнять сериализацию объектов нового класса в файл и их десериализацию. Эти методы очень похожи на\n" +
				"методы, использованные при рассмотрении интерфейса Serializable:\n\n" +
				"" +
				"public class ExternalizeClass {\n" +
				"\tpublic static void main(String[] args) {\n" +
				"\t\tString fileName = \"test1.txt\";\n" +
				"\t\tDeveloperExt developerExt = new DeveloperExt(\"Bill\", 35, \"Java\", 50000);\n" +
				"\t\tDeveloperExt developerExt1 = new DeveloperExt(\"Bill1\", 35, \"Java1\", 50000);\n" +
				"\t\tserializeEx(developerExt, developerExt1, fileName);\n" +
				"\t\tdeserializeEx(fileName);\n" +
				"\t}\n" +
				"\n" +
				"\tpublic static void serializeEx(DeveloperExt obj, DeveloperExt obj1, String fileName) { // указываем явно класс объекта\n" +
				"\t\tFileOutputStream fos = null;\n" +
				"\t\ttry {\n" +
				"\t\t\tfos = new FileOutputStream(fileName);\n" +
				"\t\t\tObjectOutputStream oos = new ObjectOutputStream(fos);\n" +
				"\t\t\toos.writeObject(obj); \n" +
				"\t\t\toos.writeObject(obj1);\n" +
				"\t\t\toos.close();\n" +
				"\t\t} catch (FileNotFoundException e) {\n" +
				"\t\t\tSystem.err.println(\"File not found\");\n" +
				"\t\t\te.printStackTrace();\n" +
				"\t\t} catch (IOException e) {\n" +
				"\t\t\tSystem.err.println(\"IO error\");\n" +
				"\t\t\te.printStackTrace();\n" +
				"\t\t} finally {\n" +
				"\t\t\ttry {\n" +
				"\t\t\t\tfos.close();\n" +
				"\t\t\t} catch (IOException e) {\n" +
				"\t\t\t\tSystem.err.println(\"IO error by closing\");\n" +
				"\t\t\t\te.printStackTrace();\n" +
				"\t\t\t}\n" +
				"\t\t}\n" +
				"\t}\n" +
				"\n" +
				"\tpublic static void deserializeEx(String fileName) {\n" +
				"\t\tObject obj = null;\n" +
				"\t\tObject obj1 = null;\n" +
				"\t\ttry (FileInputStream fis = new FileInputStream(fileName)) {\n" +
				"\t\t\tObjectInputStream ois = new ObjectInputStream(fis);\n" +
				"\t\t\tobj = ois.readObject();\n" +
				"\t\t\tobj1 = ois.readObject();\n" +
				"\t\t\tois.close();\n" +
				"\t\t} catch (FileNotFoundException e) {\n" +
				"\t\t\tSystem.err.println(\"File not found\");\n" +
				"\t\t\te.printStackTrace();\n" +
				"\t\t} catch (IOException e) {\n" +
				"\t\t\tSystem.err.println(\"IO error\");\n" +
				"\t\t\te.printStackTrace();\n" +
				"\t\t} catch (ClassNotFoundException e) {\n" +
				"\t\t\tSystem.err.println(\"Class not found\");\n" +
				"\t\t\te.printStackTrace();\n" +
				"\t\t}\n" +
				"\t\tSystem.out.println(obj);\n" +
				"\t\tSystem.out.println(obj1);\n" +
				"\t}\n" +
				"}\n\n" +
				"" +
				"При выполнении этого кода вы должны получить ошибку, если у Вас явно не указан конструктор по умолчнию.\n" +
				"Дело в том, что в отличии от десериализации с использованием интерфейса Serializable, при использовании\n" +
				"интерфейса Externalizable десериализируемый объект создается конструктором по умолчанию. А уже\n" +
				"затем, созданный этим конструктором объект, вызывает метод readExternal() и заполняет свои поля данными из\n" +
				"потока. Поэтому, в каждом классе, который реализует интерфейс Externalizable, должен быть конструктор без\n" +
				"параметров. Даже больше – такой конструктор должен быть у всех потомков этого класса, поскольку они тоже\n" +
				"наследуют интерфейсу Externalizable.\n\n" +
				"" +
				"И еще одно. Я думаю, вы понимаете, что спецификатор transient при использовании Externalizable является\n" +
				"лишним, поскольку вы сами решаете, какое поле сериализовать, а какое нет.\n\n");
	}

	@Override
	public String getTHEME() {
		return THEME;
	}
}
