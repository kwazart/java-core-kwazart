package com.kwazart.java_core.main.theme023_serializable;

import com.kwazart.java_core.main.theme023_serializable.model.Developer;

import java.io.*;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 05.10.2020 0:28
 */

public class SerializeClass {
	public static void main(String[] args) {
		String fileName = "test.txt";
		Developer developer = new Developer("Tommy", "Black", 30, "Java", 100000);
		serialize(developer, fileName);
		Developer newDeveloper = (Developer) deserialize(fileName);
		System.out.println(newDeveloper);
	}
	public static void serialize(Object obj, String fileName) {
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(fileName);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(obj);
		} catch (FileNotFoundException e) {
			System.err.println("File not found");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("IO error");
			e.printStackTrace();
		} finally {
			try {
				fos.close();
			} catch (IOException e) {
				System.err.println("IO error by closing");
				e.printStackTrace();
			}
		}
	}

	public static Object deserialize(String fileName) {
		Object obj = null;
		try (FileInputStream fis = new FileInputStream(fileName)) {
			ObjectInputStream ois = new ObjectInputStream(fis);
			obj = ois.readObject();
			ois.close();
		} catch (FileNotFoundException e) {
			System.err.println("File not found");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("IO error");
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			System.err.println("Class not found");
			e.printStackTrace();
		}
		return obj;
	}
}
