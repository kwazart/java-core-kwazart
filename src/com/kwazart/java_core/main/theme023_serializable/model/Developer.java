package com.kwazart.java_core.main.theme023_serializable.model;

import java.io.Serializable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 05.10.2020 0:26
 */

public class Developer implements Serializable {
	//private static final long serialVersionUID = 15325645745823598L;

	private String firstName;
	private String surname;
	private int age;
	private String language;
	private int salary;

	public Developer(String firstName, String surname, int age, String language, int salary) {
		this.firstName = firstName;
		this.surname = surname;
		this.age = age;
		this.language = language;
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Developer{" +
				"firstName='" + firstName + '\'' +
				", surname='" + surname + '\'' +
				", age=" + age +
				", language='" + language + '\'' +
				", salary=" + salary +
				'}';
	}

//	public boolean checkSalary() {
//		return this.salary >= 100000;
//	}
}
