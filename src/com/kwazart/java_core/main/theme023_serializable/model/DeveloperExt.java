package com.kwazart.java_core.main.theme023_serializable.model;

import java.io.*;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 05.10.2020 0:26
 */

public class DeveloperExt implements Externalizable {

	private String firstName;
	private int age;
	private String language;
	private int salary;

	public DeveloperExt(String firstName, int age, String language, int salary) {
		this.firstName = firstName;
		this.age = age;
		this.language = language;
		this.salary = salary;
	}

	public DeveloperExt() {
	}

	@Override
	public String toString() {
		return "DeveloperExt{" +
				"firstName='" + firstName + '\'' +
				", age=" + age +
				", language='" + language + '\'' +
				", salary=" + salary +
				'}';
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		System.out.println("Inside writeExternal");
		out.writeObject(firstName);
		out.writeInt(age);
		out.writeObject(language);
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		System.out.println("Inside readExternal");
		this.firstName = (String) in.readObject();
		this.age = in.readInt();
		this.language = (String) in.readObject();
	}
}
