package com.kwazart.java_core.main.theme024_multithreading;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 19.10.2020 12:49
 */

public class MultiInfo implements InfoPrintable {
	private static final String THEME = "Многопоточность";

	// TODO дописать примеры и заполнить  info
	public void printInfo() {
		System.out.println("\n\n");
	}

	@Override
	public String getTHEME() {
		return THEME;
	}
}
