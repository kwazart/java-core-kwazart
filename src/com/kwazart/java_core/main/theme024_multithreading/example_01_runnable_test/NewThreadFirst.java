package com.kwazart.java_core.main.theme024_multithreading.example_01_runnable_test;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 06.10.2020 19:13
 */

public class NewThreadFirst implements Runnable{
	String threadName;

	public NewThreadFirst(String threadName) {
		this.threadName = threadName;
	}

	@Override
	public void run() {
		System.out.println(threadName + " is started");
		try {
			for (int i = 0; i < 10; i++) {
				Thread.sleep(500);
				System.out.println("Thread: " + threadName + "\tcounter: " + i);
			}
		} catch (InterruptedException e) {
			System.out.println("Thread: " + threadName + " is interrupted. Cause: " + e.getCause());
		}
		System.out.println(threadName + " is finished");
	}
}
