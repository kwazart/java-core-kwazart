package com.kwazart.java_core.main.theme024_multithreading.example_01_runnable_test;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 06.10.2020 19:40
 */

public class NewThreadSecond implements Runnable{
	public Thread thread;

	public NewThreadSecond(String name) {
		this.thread = new Thread(this, name);
	}

	public static NewThreadSecond createAndStart(String name) {
		NewThreadSecond myThread = new NewThreadSecond(name);
		myThread.thread.start();
		return myThread;
	}

	@Override
	public void run() {
		System.out.println(thread.getName() + " is started");
		try {
			for (int i = 0; i < 10; i++) {
				Thread.sleep(500);
				System.out.println("Thread: " + thread.getName() + "\tcounter: " + i);
			}
		} catch (InterruptedException e) {
			System.out.println("Thread: " + thread.getName() + " is interrupted. Cause: " + e.getCause());
		}
		System.out.println(thread.getName() + " is finished");
	}
}
