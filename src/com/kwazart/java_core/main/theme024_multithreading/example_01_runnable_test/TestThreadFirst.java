package com.kwazart.java_core.main.theme024_multithreading.example_01_runnable_test;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 06.10.2020 19:18
 */

public class TestThreadFirst {
	public static void main(String[] args) {
		System.out.println("Main thread started");

		NewThreadFirst newThreadFirst = new NewThreadFirst("New-Thread-1");
		Thread threadOne = new Thread(newThreadFirst);
		threadOne.start();

		for (int i = 0; i < 60; i++) {
			System.out.print(".");
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				System.out.println("Main thread is interrupted. Cause: " + e.getCause());
			}
		}

		System.out.println("Main thread finished");
	}
}
