package com.kwazart.java_core.main.theme024_multithreading.example_01_runnable_test;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 06.10.2020 20:56
 */

public class TestThreadSecond {
	public static void main(String[] args) {
		System.out.println("Main thread started");

		NewThreadSecond newThreadSecond = NewThreadSecond.createAndStart("New-Thread-1");

		for (int i = 0; i < 60; i++) {
			System.out.print(".");
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				System.out.println("Main thread is interrupted. Cause: " + e.getCause());
			}
		}

		System.out.println("Main thread finished");
	}
}
