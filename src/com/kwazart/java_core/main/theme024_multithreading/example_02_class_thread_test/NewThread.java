package com.kwazart.java_core.main.theme024_multithreading.example_02_class_thread_test;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 06.10.2020 23:07
 */

public class NewThread extends Thread {

	public NewThread(String name) {
		super(name);
	}

	public void run() {
		System.out.println(getName() + " is started");
		try {
			for (int i = 0; i < 10; i++) {
				Thread.sleep(500);
				System.out.println("Thread: " + getName() + "\tcounter: " + i);
			}
		} catch (InterruptedException e) {
			System.out.println("Thread: " + getName() + " is interrupted. Cause: " + e.getCause());
		}
		System.out.println(getName() + " is finished");
	}
}
