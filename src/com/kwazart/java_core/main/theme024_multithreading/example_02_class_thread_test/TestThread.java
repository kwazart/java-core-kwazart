package com.kwazart.java_core.main.theme024_multithreading.example_02_class_thread_test;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 06.10.2020 23:09
 */

public class TestThread {
	public static void main(String[] args) {
		System.out.println("Main thread started");

		NewThread newThread = new NewThread("New-Thread-1");

		newThread.start();

		for (int i = 0; i < 60; i++) {
			System.out.print(".");
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				System.out.println("Main thread is interrupted. Cause: " + e.getCause());
			}
		}

		System.out.println("Main thread finished");
	}
}
