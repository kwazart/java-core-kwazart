package com.kwazart.java_core.main.theme024_multithreading.example_03_three_threads_test;

import com.kwazart.java_core.main.theme024_multithreading.example_01_runnable_test.NewThreadSecond;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 06.10.2020 23:13
 */

public class ThreeThreadsTestWithIsALive {
	public static void main(String[] args) {
		System.out.println("Main thread started");

		NewThreadSecond nt1 = NewThreadSecond.createAndStart("New-Thread-1");
		NewThreadSecond nt2 = NewThreadSecond.createAndStart("New-Thread-2");
		NewThreadSecond nt3 = NewThreadSecond.createAndStart("New-Thread-3");

		do {
			System.out.print(".");
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				System.out.println("Main thread is interrupted. Cause: " + e.getCause());
			}
		} while (nt1.thread.isAlive() ||
				nt2.thread.isAlive() ||
				nt3.thread.isAlive());

		System.out.println("Main thread finished");
	}
}
