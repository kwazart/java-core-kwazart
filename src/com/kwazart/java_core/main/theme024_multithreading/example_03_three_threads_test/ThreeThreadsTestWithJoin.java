package com.kwazart.java_core.main.theme024_multithreading.example_03_three_threads_test;

import com.kwazart.java_core.main.theme024_multithreading.example_01_runnable_test.NewThreadSecond;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 06.10.2020 23:13
 */

public class ThreeThreadsTestWithJoin {
	public static void main(String[] args) {
		System.out.println("Main thread started");

		NewThreadSecond nt1 = NewThreadSecond.createAndStart("New-Thread-1");
		NewThreadSecond nt2 = NewThreadSecond.createAndStart("New-Thread-2");
		NewThreadSecond nt3 = NewThreadSecond.createAndStart("New-Thread-3");

		try {
			nt3.thread.join();
			System.out.println(nt3.thread.getName() + " - join");
			nt2.thread.join();
			System.out.println(nt2.thread.getName() + " - join");
			nt1.thread.join();
			System.out.println(nt1.thread.getName() + " - join");
		} catch (InterruptedException e) {
			System.out.println("Main thread is interrupted. Cause: " + e.getCause());
		}

		System.out.println("Main thread finished");
	}
}
