package com.kwazart.java_core.main.theme024_multithreading.example_04_priority_test;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 06.10.2020 23:28
 */

public class Priority implements Runnable{
	int count;
	Thread thread;

	static boolean stop = false;
	static String currentName;

	public Priority(String name) {
		thread = new Thread(this, name);
		count = 0;
		currentName = name;
	}

	@Override
	public void run() {
		System.out.println(thread.getName() + " started");
		do {
			count++;

			if (currentName.compareTo(thread.getName()) != 0) {
				currentName = thread.getName();
				//System.out.println("In " + currentName);
			}

		} while (stop == false && count < 10000000);
		stop = true;

		System.out.println("\n" + thread.getName() + " - interrupt");
	}
}
