package com.kwazart.java_core.main.theme024_multithreading.example_04_priority_test;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 06.10.2020 23:33
 */

public class PriorityTest {
	public static void main(String[] args) {
		Priority p1 = new Priority("High priority");
		Priority p2 = new Priority("Low priority");
		Priority p3 = new Priority("Normal priority");
		Priority p4 = new Priority("Normal priority");
		Priority p5 = new Priority("Normal priority");

		p1.thread.setPriority(Thread.NORM_PRIORITY+4);
		p2.thread.setPriority(Thread.NORM_PRIORITY-4);

		p1.thread.start();
		p2.thread.start();
		p3.thread.start();
		p4.thread.start();
		p5.thread.start();

		try {
			p1.thread.join();
			p2.thread.join();
			p3.thread.join();
			p4.thread.join();
			p5.thread.join();
		} catch (InterruptedException e) {
			System.out.println("Main thread is interrupted");
		}

		System.out.println("Thread1 counter with high priority: " + p1.count);
		System.out.println("Thread2 counter with low priority: " + p2.count);
		System.out.println("Thread3 counter with normal priority: " + p3.count);
		System.out.println("Thread4 counter with normal priority: " + p4.count);
		System.out.println("Thread5 counter with normal priority: " + p5.count);
	}
}
