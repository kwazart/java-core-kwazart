package com.kwazart.java_core.main.theme024_multithreading.example_05_synchronized;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 09.10.2020 6:42
 */

class SumArray {
	private int sum;

	// можем синхронизироваться на методе (1 способ)
	int sumArray(int nums[]) { // добавляем synchronized перед int если доступ к классу есть
		sum = 0;

		for (int i = 0; i < nums.length; i++) {
			sum += nums[i];
			System.out.println("Current sum value for " + Thread.currentThread().getName() + " - " + sum);
		}

		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			System.out.println("Thread is interrupted");
		}

		return sum;
	}
}

class MyThread implements Runnable {
	Thread thread;
	static SumArray sa = new SumArray();
	int a[];
	int answer;

	public MyThread(String name, int[] a) {
		thread = new Thread(this, name);
		this.a = a;
	}

	public static MyThread createAndStart(String name, int[] nums) {
		MyThread myThread = new MyThread(name, nums);

		myThread.thread.start();
		return myThread;
	}

	public void run() {
		int sum;

		System.out.println(thread.getName() + " - start");

		// если нет доступа к классу, то можем создать синхронизированный блок
		// и синхронизироваться на нужном
		synchronized (sa) {
			answer = sa.sumArray(a);
		}
		//answer = sa.sumArray(a); // оставляем если синхронизировались через метод sumArray
		System.out.println("Sum value for " + thread.getName() + " - " + answer);

		System.out.println(thread.getName() + " - finish");
	}
}

public class Sync {
	public static void main(String[] args) {
		int[] a = {1,2,3,4,5};

		MyThread mt1 = MyThread.createAndStart("Child thread #1", a);
		MyThread mt2 = MyThread.createAndStart("Child thread #2", a);

		try {
			mt1.thread.join();
			mt2.thread.join();
		} catch (InterruptedException e) {
			System.out.println("Main thread interrupted");
		}
	}
}
