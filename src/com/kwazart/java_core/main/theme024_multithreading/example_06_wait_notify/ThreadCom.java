package com.kwazart.java_core.main.theme024_multithreading.example_06_wait_notify;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 09.10.2020 7:25
 */

class TickTock {
	String state;

	synchronized void tick(boolean running) {
		if (!running) { // остановить часы
			state = "ticked";
			notify(); // уведомить ожидающие потоки
			return;
		}

		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.print("Tick ");

		state ="ticked"; // установить текущее состояние после такта "тик"

		notify(); // позволить выполняться методу tock()
		try {
			while (!state.equals("tocked"))
				wait(); // ожидать до завершения метода tock()
		} catch (InterruptedException e) {
			System.out.println("Thread interrupted");
		}
	}

	synchronized void tock(boolean running) {
		if (!running) { // остановить часы
			state = "tocked";
			notify(); // уведомить ожидающие потоки
			return;
		}

		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Tock");

		state = "tocked"; // установить текущее состояние после такта "так"

		notify(); // позволять выполняться методу tick()
		try {
			while (!state.equals("ticked"))
				wait();  // ожидать до завершения метода tick()
		} catch (InterruptedException e) {
			System.out.println("Thread interrupted");
		}
	}
}

class MyThread implements Runnable {
	Thread thread;
	TickTock tt;

	// конструктор нового потока
	MyThread(String name, TickTock tt) {
		thread = new Thread(this, name);
		this.tt	= tt;
	}

	public static MyThread createAndStart(String name, TickTock tt) {
		MyThread myThread = new MyThread(name, tt);
		myThread.thread.start();
		return myThread;
	}

	public void run() {
		if (thread.getName().compareTo("Tick") == 0) {
			for (int i = 0; i < 5; i++) {
				tt.tick(true);
			}
			tt.tick(false);
		} else {
			for (int i = 0; i < 5; i++) {
				tt.tock(true);
			}
			tt.tock(false);
		}
	}
}

public class ThreadCom {
	public static void main(String[] args) {
		TickTock tt= new TickTock();
		MyThread mt1 = MyThread.createAndStart("Tick", tt);
		MyThread mt2 = MyThread.createAndStart("Tock", tt);

		try {
			mt1.thread.join();
			mt2.thread.join();
		} catch (InterruptedException e) {
			System.out.println("Main thead interrupted");
		}
	}
}
