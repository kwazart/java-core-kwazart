package com.kwazart.java_core.main.theme024_multithreading.example_07_analog_suspend_resume_stop_thead;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 09.10.2020 8:01
 */

class MyThead implements Runnable {
	Thread thread;
	boolean suspended;
	boolean stopped;

	MyThead (String name) {
		thread = new Thread(this, name);
		suspended = false;
		stopped = false;
	}

	public static MyThead createAndStart(String name) {
		MyThead myThead = new MyThead(name);
		myThead.thread.start();
		return myThead;
	}

	public void run() {
		System.out.println(thread.getName() + " started");
		try {
			for (int i = 0; i < 1000; i++) {
				System.out.print(i + " ");
				if ((i % 10) == 0) {
					System.out.println();
					Thread.sleep(250);
				}

				// Использование синхронизированного блока для
				// проверки значения переменных suspended и stopped
				synchronized (this) {
					while (suspended)
						wait();
				}
				if (stopped) break;
			}
		} catch (InterruptedException e) {
			System.out.println(thread.getName() + " interrupted");
		}

		System.out.println(thread.getName() + " - finished");
	}

	synchronized void mystop() {
		stopped = true;
		// далее полностью останавливаем поток
		suspended = false;
		notify();
	}

	// приостановить поток
	synchronized void mysuspend() {
		suspended = true;
	}

	// возобновить поток
	synchronized void myresume() {
		suspended = false;
		notify();
	}
}

public class Syspend {
	public static void main(String[] args) {
		MyThead mt1 = MyThead.createAndStart("My thread");

		try {
			Thread.sleep(1000); // позволяем другим потокам начать
			mt1.mysuspend();
			System.out.println("thread suspended");
			Thread.sleep(1000);

			mt1.myresume();
			System.out.println("thread resumed");
			Thread.sleep(1000);

			mt1.mysuspend();
			System.out.println("thread suspended");
			Thread.sleep(1000);

			mt1.myresume();
			System.out.println("thread resumed");
			Thread.sleep(1000);

			mt1.mysuspend();
			System.out.println("thread stopped");
			mt1.mystop();
		} catch (InterruptedException e) {
			System.out.println("main thread interrupted");
		}

		try {
			mt1.thread.join();
		} catch (InterruptedException e) {
			System.out.println("main thread interrupted");
		}

		System.out.println("Main thread - finish");
	}
}
