package com.kwazart.java_core.main.theme024_multithreading.example_08_use_main;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 13.10.2020 7:46
 */

public class UseMain {
	public static void main(String[] args) {
		Thread thread;
		// get main thread
		thread = Thread.currentThread();
		System.out.println("Main thread name: " + thread.getName());
		System.out.println("Main thread priority: " + thread.getPriority());

		System.out.println("\nName and priority setting:");
		thread.setName("Thread-#1");
		thread.setPriority(Thread.NORM_PRIORITY+4);

		System.out.println("Mew main thread name: " + thread.getName());
		System.out.println("New main thread priority: " + thread.getPriority());
	}
}
