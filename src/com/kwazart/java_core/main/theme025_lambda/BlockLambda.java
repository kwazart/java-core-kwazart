package com.kwazart.java_core.main.theme025_lambda;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 19.10.2020 11:54
 */

// тестовый интерфейс, где мы работаем со списком разработчиков и вовзращаем изменный список
@FunctionalInterface
interface DeveloperInfo {
	List<Developer> selfFunctional(List<Developer> list);
}


public class BlockLambda {
	public static void main(String[] args) {
		//создаем список разработчиков
		List<Developer> developerList = new ArrayList<>();
		developerList.add(new Developer("Dev1", "Java", 4000));
		developerList.add(new Developer("Dev2", "Python", 3000));
		developerList.add(new Developer("Dev3", "PHP", 5000));
		developerList.add(new Developer("Dev4", "C++", 4500));
		developerList.add(new Developer("Dev5", "Python", 4000));
		developerList.add(new Developer("Dev6", "PHP", 4000));
		developerList.add(new Developer("Dev7", "Java", 4500));
		developerList.add(new Developer("Dev8", "Python", 2500));

		// задаем собственную логику реализации метода ФИ
		// которая возвращает новый List
		DeveloperInfo developerInfo = list -> {
			// создаем новый список
			List<Developer> newList = new ArrayList<>();
			for (Developer developer : list) {
				// выделяем Java разработчиков
				if (developer.getLanguage().equalsIgnoreCase("java")) {
					// добавляем в новый список
					newList.add(developer);
				}
			}
			// возвращаем и метод ФИ этот новый список
			return newList;
		};

		// проходимся по всем специалистам нового списка,
		// который получается после выполнения собственной реализации метода ФИ
		for (Developer dev : developerInfo.selfFunctional(developerList)) {
			System.out.println(dev);
		}
	}
}

// тестовый класс Разработчик
class Developer {
	private String name;
	private String language;
	private int salary;

	public Developer(String name, String language, int salary) {
		this.name = name;
		this.language = language;
		this.salary = salary;
	}

	public String getName() {
		return name;
	}

	public String getLanguage() {
		return language;
	}

	public int getSalary() {
		return salary;
	}

	@Override
	public String toString() {
		return "Developer{" +
				"name='" + name + '\'' +
				", language='" + language + '\'' +
				", salary=" + salary +
				'}';
	}
}
