package com.kwazart.java_core.main.theme025_lambda;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 18.10.2020 22:56
 */

public class LambdaInfo implements InfoPrintable {
	private static final String THEME = "Lambda";

	public void printInfo() {
		System.out.println("Анонимные классы и лямбда-выражения\n" +
				"Зачем же используются анонимные классы? Используются потому что, отсутствует такое понятие как ссылка на метод.\n" +
				"Есть примитивы, ссылочные типы на объекты. Но нет ссылок на методы. Есть множество ситуаций, когда нам необходимо\n" +
				"использовать метод, который неизвестен на этапе разработки нашего кода мы не знаем. Пример button в Swing.\n" +
				"Т.е какой-то объект будет в дальнейшем использовать какой-то метод, но не метод еще даже не существует.\n" +
				"Т.е. нужно вызвать метод, которого еще нет. В языках C/C++ есть ссылки на методы, в Java такого ранее не было.\n" +
				"В Java есть указатели на объекты, массивы, но не на методы.\n\n" +
				"" +
				"Если у нас есть указатели на объект, то им можно и воспользоваться. Поэтому нам нужен объект и его метод.\n" +
				"В частности интерфейс у которого есть какой-то метод нереализованный. Поэтому мы можем создать ссылку на объект\n" +
				"в переменную типа интерфейс.\n\n" +
				"" +
				"Разберем ситуацию. Есть класса Сигнализация с методом срабатывания сигнала:\n\n" +
				"" +
				"// Сигнализация\n" +
				"public class Signaling {\n" +
				"\t// срабатывание сигнала\n" +
				"\tpublic void alarmTrigger() {\n" +
				"\t\tSystem.out.println(\"UEEEEEEEEEEEEE!!! UEEEEEEEEEEEEE!!!\");\n" +
				"\t}\n" +
				"}\n\n" +
				"" +
				"Мы можем создать в main этот объект и вызвать соответствующий метод:\n\n" +
				"" +
				"public class LambdaMain {\n" +
				"\tpublic static void main(String[] args) {\n" +
				"\t\tSignaling sign = new Signaling();\n" +
				"\t\tsign.alarmTrigger();\n" +
				"\t}\n" +
				"}\n\n" +
				"" +
				"Но (например) ни полиция, ни руководитель магазина, ни служба безопасности не узнают об этом.\n" +
				"Создадим класс полиция:\n\n" +
				"" +
				"// класс Полиция\n" +
				"public class Police {\n" +
				"\t// полиция реагирует\n" +
				"\tpublic void policeReact() {\n" +
				"\t\t// Полицейские выехали к месту срабатывания сигнала\n" +
				"\t\tSystem.out.println(\"Police officers drive to the place where the alarm was triggered\");\n" +
				"\t}\n" +
				"}\n\n" +
				"" +
				"И можем добавить объект в main:\n\n" +
				"" +
				"\tSignaling sign = new Signaling();\n" +
				"\tPolice police = new Police();\n" +
				"\tsign.alarmTrigger();\n\n" +
				"" +
				"Проблема в том, что сигнализация и полиция никак не связаны. Сигнализация работает, а толку нет. Дело в том,\n" +
				"что сигнализация ничего не знает о полиции, да и не должна знать. Потому что эту сигнализация могла быть создана\n" +
				"задолго до появления класса полиции. Как же их связать? Как заставить сигнализацию вызвать метод из класса\n" +
				"полиции policeReact() не делая никаких указателей на класс полиция. Было бы здорово воспользоваться ссылкой\n" +
				"на метод, если бы такие были в Java, но увы. Поэтому мы используем ссылку типу интерфейс. Данный интерфейс\n" +
				"будет связующим звеном между сигнализацией и полицией.\n\n" +
				"" +
				"// интерфейс Реакция\n" +
				"public interface Reacting {\n" +
				"\t// реагирование\n" +
				"\tvoid response();\n" +
				"}\n\n" +
				"" +
				"Теперь в классе сигнализации мы можем задействовать данный интерфейс.\n\n" +
				"" +
				"// Сигнализация\n" +
				"public class Signaling {\n" +
				"\tpublic Reacting somebodyReacts;\n" +
				"\n" +
				"\t// срабатывание сигнала\n" +
				"\tpublic void alarmTrigger() {\n" +
				"\t\tSystem.out.println(\"UEEEEEEEEEEEEE!!! UEEEEEEEEEEEEE!!!\");\n" +
				"\n" +
				"\t\t// если есть объект по данной ссылке, то можем вызвать метод response()\n" +
				"\t\t// так как данная ссылка можем указывать только на объекты реализующие данный интерфейс\n" +
				"\t\tif (reacting != null) {\n" +
				"\t\t\treacting.response();\n" +
				"\t\t}\n" +
				"\t}\n" +
				"}\n\n" +
				"" +
				"Теперь можно в классе полиция реализовать интерфейс Reacting:\n\n" +
				"" +
				"// класс Полиция\n" +
				"public class Police implements Reacting{\n" +
				"\t// полиция реагирует\n" +
				"\tpublic void policeReact() {\n" +
				"\t\t// Полицейские выехали к месту срабатывания сигнала\n" +
				"\t\tSystem.out.println(\"Police officers drive to the place where the alarm was triggered\");\n" +
				"\t}\n" +
				"\n" +
				"\t@Override\n" +
				"\tpublic void response() {\n" +
				"\t\tpoliceReact();\n" +
				"\t}\n" +
				"}\n\n" +
				"" +
				"Затем мы должны занести поле ссылки public Reacting somebodyReacts; наш объект полиция:\n\n" +
				"" +
				"\tSignaling sign = new Signaling();\n" +
				"\tPolice police = new Police();\n" +
				"\n" +
				"\tsign.somebodyReacts = police;\n" +
				"\tsign.alarmTrigger();\n\n" +
				"" +
				"Сигнализация до сих пор ничего не знает о полиции, но она знает об интерфейсе в котором есть возможная\n" +
				"реализация данного интерфейса. То есть интерфейс является некой прослойкой. Ни полиция не знает о сигналзации,\n" +
				"ни сигналзация о полиции. Причем сигнализация узнает о полиции только во время runtime.\n" +
				"Присвоение ссылки типа интерфейс к объекту реализующему интерфейс называется \"подписка на событие\" или\n" +
				"\"event subscribe\". Потому что мы только один раз указываем о данной связи.\n\n" +
				"" +
				"Минус данной схемы: public Reacting somebodyReacts; - публичное поле; срабатывать на сигнализацию могут\n" +
				"несколько классов. Например добавит класс директор магазина.\n\n" +
				"" +
				"// класс Директор\n" +
				"public class Manager implements Reacting{\n" +
				"\t// реакция директора\n" +
				"\tpublic void managerReact() {\n" +
				"\t\t// директор в панике\n" +
				"\t\tSystem.out.println(\"Director in a panic\");\n" +
				"\t}\n" +
				"\n" +
				"\t@Override\n" +
				"\tpublic void response() {\n" +
				"\t\tmanagerReact();\n" +
				"\t}\n" +
				"}\n\n" +
				"" +
				"Проблема в том, что в классе Сигнализация у нас только одно поле public Reacting somebodyReacts, т.е.\n" +
				"может только один объект среагировать на сигнализацию. Можно сделать, например, коллекцию таких объектов.\n" +
				"Исправим наш класс Сигнализация:\n\n" +
				"" +
				"// Сигнализация\n" +
				"public class Signaling {\n" +
				"\t// список всех, кто среагирует на сигнализацию\n" +
				"\tprivate List<Reacting> responseObjects = new ArrayList<>();\n" +
				"\n" +
				"\t// добавляем объект реагирования - подписка на событие\n" +
				"\tpublic void addResponseObject(Reacting object) {\n" +
				"\t\t// проверяем, что объект существует и еще не содержится в списке\n" +
				"\t\t// затем добавляем\n" +
				"\t\tif (object != null && !(responseObjects.contains(object))) {\n" +
				"\t\t\tresponseObjects.add(object);\n" +
				"\t\t}\n" +
				"\t}\n" +
				"\n" +
				"\t// удаляем объект реагирования - отписка от событие\n" +
				"\tpublic void removeResponseObject(Reacting object) {\n" +
				"\t\t// проверяем, что объект существует и еще не содержится в списке\n" +
				"\t\t// затем удаляем\n" +
				"\t\tif (object != null && !(responseObjects.contains(object))) {\n" +
				"\t\t\tresponseObjects.remove(object);\n" +
				"\t\t}\n" +
				"\t}\n" +
				"\n" +
				"\t// срабатывание сигнала\n" +
				"\tpublic void alarmTrigger() {\n" +
				"\t\tSystem.out.println(\"UEEEEEEEEEEEEE!!! UEEEEEEEEEEEEE!!!\");\n" +
				"\n" +
				"\t\t// перебираем весь список реагирующих объектов\n" +
				"\t\tfor (Reacting reactingObject : responseObjects) {\n" +
				"\t\t\t// вызываем реакцию объектов\n" +
				"\t\t\treactingObject.response();\n" +
				"\t\t}\n" +
				"\t}\n" +
				"}\n\n" +
				"" +
				"Изменится и наш метод main, теперь можно добавлять объекты реагирования на сигнализацию через метод:\n\n" +
				"" +
				"public class LambdaMain {\n" +
				"\tpublic static void main(String[] args) {\n" +
				"\t\tSignaling sign = new Signaling();\n" +
				"\t\tPolice police = new Police();\n" +
				"\t\tManager manager = new Manager();\n" +
				"\t\t\n" +
				"\t\tsign.addResponseObject(police);\n" +
				"\t\tsign.addResponseObject(manager);\n" +
				"\t\tsign.alarmTrigger();\n" +
				"\t}\n" +
				"}\n\n" +
				"" +
				"Итого: у нас теперь одно срабатывание сигнализации, а объектов реагирования может быть много.\n" +
				"Все замечательно до тех пор, пока у нас отдельно созданы объекты классов реагирования и все оформлено сполна.\n" +
				"Но если нам нужен объект, который всего лишь выводит какое-то сообщение о своем реагировании?\n" +
				"Необходимо создавать класс, прописывать в нем методы, создавать объект класса, ссылку передавать классу\n" +
				"Сигнализация, вызывать метод объекта и т.д.\n" +
				"Для того, чтоб сократиить код можно воспользоваться анонимным классом. Допустим будет условный класс\n" +
				"Служба безопасности, которая также должна среагировать на сигнализацию, например, вывод в консоль информации\n" +
				"о том, что они тоже выехали на объект. Воспользуемся внутренним классом для того, чтобы создать объект\n" +
				"этого класса:\n\n" +
				"" +
				"public class LambdaMain {\n" +
				"\tpublic static void main(String[] args) {\n" +
				"\t\tSignaling sign = new Signaling();\n" +
				"\t\tPolice police = new Police();\n" +
				"\t\tManager manager = new Manager();\n" +
				"\n" +
				"\t\tsign.addResponseObject(police);\n" +
				"\t\tsign.addResponseObject(manager);\n" +
				"\n" +
				"\t\t// внутренний класс СлужбаБезопасности\n" +
				"\t\tclass SecurityService implements Reacting{\n" +
				"\t\t\t// служба безопасности реагирует\n" +
				"\t\t\tpublic void secServiceReact() {\n" +
				"\t\t\t\t// Служба безопасности выехала к месту срабатывания сигнала\n" +
				"\t\t\t\tSystem.out.println(\"The security service goes to the place where the alarm was triggered\");\n" +
				"\t\t\t}\n" +
				"\n" +
				"\t\t\t@Override\n" +
				"\t\t\tpublic void response() {\n" +
				"\t\t\t\tsecServiceReact();\n" +
				"\t\t\t}\n" +
				"\t\t}\n" +
				"\n" +
				"\t\tsign.addResponseObject(new SecurityService());\n" +
				"\n" +
				"\t\tsign.alarmTrigger();\n" +
				"\t}\n" +
				"}\n\n" +
				"" +
				"Выглядит огромно, хотя требуется всего лишь вывести в консоль строку с реакцией на срабатывание сигнализации.\n" +
				"Все можно упростить. Первое решение - использовать анонимный класс. Класс СлужбаБезопасности более нигде\n" +
				"не используется и служит только для вывода информации о реагировании. Создадим анонимный класс.\n\n" +
				"" +
				"public class LambdaMain {\n" +
				"\tpublic static void main(String[] args) {\n" +
				"\t\tSignaling sign = new Signaling();\n" +
				"\t\tPolice police = new Police();\n" +
				"\t\tManager manager = new Manager();\n" +
				"\n" +
				"\t\tsign.addResponseObject(police);\n" +
				"\t\tsign.addResponseObject(manager);\n" +
				"\n" +
				"\t\tsign.addResponseObject(new Reacting() {\n" +
				"\t\t\t@Override\n" +
				"\t\t\tpublic void response() {\n" +
				"\t\t\t\tSystem.out.println(\"The security service goes to the place where the alarm was triggered\");\n" +
				"\t\t\t}\n" +
				"\t\t});\n" +
				"\n" +
				"\t\tsign.alarmTrigger();\n" +
				"\t}\n" +
				"}\n\n" +
				"" +
				"Выглядит лучше, но все еще немалым. Вплоть до Java 8 необходимо было прописывать именно так.\n\n" +
				"" +
				"Лямбда-выражение представляет собой блок кода, который можно передать в другое место,\n" +
				"поэтому он может быть выполнен позже, один или несколько раз. \n" +
				"Особое значение для ясного представления о том, каким образом лямбда-выражения реализованы в Java,\n" +
				"имеют две языковые конструкции. Первой из них является само лямбда-выражение, а второй - функциональный интерфейс.\n\n" +
				"" +
				"Лямбда-выражение, по существу, является анонимным (т.е. безымянным) методом. Но этот метод не выполняется\n" +
				"самостоятельно, а служит для реализации метода, определяемого в функциональном интерфейсе. Таким образом,\n" +
				"лямбдавыражение приводит к некоторой форме анонимного класса. Нередко лямбда-выражения называют также замыканиями.\n\n" +
				"" +
				"Функциональным называется такой интерфейс, который содержит один и только один абстрактный метод.\n" +
				"Как правило, в таком методе определяется предполагаемое назначение интерфейса. Следовательно, функциональный\n" +
				"интерфейс представляет единственное действие. Например, стандартный интерфейс Runnable является функциональным,\n" +
				"поскольку в нем определяется единственный метод run () , который, в свою очередь, определяет действие самого интерфейса\n" +
				"Runnable. Кроме того, в функциональном интерфейсе определяется целевой тип лямбда-выражения. В связи с этим\n" +
				"необходимо подчеркнуть следующее: лямбда-выражение можно использовать только в том контексте, в котором\n" +
				"определен его целевой тип. И еще одно замечание: функциональный интерфейс иногда еще называют SАМ-типом,\n" +
				"где сокращение SAM обозначает Single Abstract Method - единственный абстрактный метод.\n\n" +
				"" +
				"Основные положения о лямбда-выражениях\n" +
				"Лямбда-выражение вносит новый элемент в синтаксис и операцию в язык Java. Эта новая операция называется\n" +
				"лямбда-операцией или операцией-стрелкой (- > ). Она разделяет лямбда-выражение на две части. В левой части\n" +
				"указываются любые параметры, требующиеся в лямбда-выражении. (Если же параметры не требуются, то они указываются\n" +
				"пустым списком.) А в правой части находится тело лямбда-выражения, где определяются действия, выполняемые\n" +
				"лямбда-выражением. Операция - > буквально означает \"становиться\" или \"переходить\"\n\n" +
				"" +
				"В языке Java определены две разновидности тел лямбда-выражений. Одна из них состоит из единственного выражения,\n" +
				"а другая - из блока кода.\n\n" +
				"" +
				"Функциональные интерфейсы\n" +
				"Как пояснялось ранее, функциональным называется такой интерфейс, в котором определяется единственный абстрактный метод.\n" +
				"Те, у кого имеется предыдущий опыт программирования на Java, могут возразить, что все методы интерфейса\n" +
				"неявно считаются абстрактными, но так было до внедрения лямбда-выражений. Как пояснялось в главе 9, начиная\n" +
				"с версии JDK 8, для метода, объявляемого в интерфейсе, можно определить стандартное поведение по умолчанию, и поэтому\n" +
				"он называется методом с реал.изац,ией по умолчанию. Отныне метод интерфейса считается абстрактным лишь в том случае,\n" +
				"если у него отсутствует реализация по умолчанию. А поскольку нестатические, незакрытые и не реализуемые по умолчанию\n" +
				"методы интерфейса неявно считаются абстрактными, то их не обязательно объявлять с модификатором доступа abstract,\n" +
				"хотя это и можно сделать при желании.\n\n" +
				"" +
				"Заметим, у нас есть интерфейс Reacting всего лишь с одним методом. Когда мы хотим его реализовать (интерфейс),\n" +
				"нет необходимости указывать какой именно метод нужно использовать.\n\n" +
				"1. Нет необходимости указания явно на интерфейс, т.к. компилятор сам в состоянии понять, какой тип параметра\n" +
				"должен быть подсунут методу, т.к. на этапе компляции он видит, что принимает в себя метод addResponseObject.\n" +
				"Поэтому его можно убрать.\n" +
				"2. Посколько интерфейс является функциональным, имя метода можно не писать\n" +
				"3. Возвращаемый тип и модификаторы доступа также можно не писать\n" +
				"4. Ставим () -> и тело метода\n" +
				"5. Если бы было несколько параметров, то их тип также можно не писать\n" +
				"6. Если бы был один параметр в методе, то круглые скобки также можно опустить и тип не указывать\n" +
				"7. В случае, если тело метода состоит из одного оператора, то фигурные скобки также не нужны\n" +
				"8. Если в теле метода должен быть только оператор return, то его также можно опустить\n\n" +
				"" +
				"Итого:\n\n" +
				"" +
				"public class LambdaMain {\n" +
				"\tpublic static void main(String[] args) {\n" +
				"\t\tSignaling sign = new Signaling();\n" +
				"\t\tPolice police = new Police();\n" +
				"\t\tManager manager = new Manager();\n" +
				"\n" +
				"\t\tsign.addResponseObject(police);\n" +
				"\t\tsign.addResponseObject(manager);\n" +
				"\n" +
				"\t\tsign.addResponseObject(new Reacting() {\n" +
				"\t\t\t@Override\n" +
				"\t\t\tpublic void response() {\n" +
				"\t\t\t\tSystem.out.println(\"The security service goes to the place where the alarm was triggered\");\n" +
				"\t\t\t}\n" +
				"\t\t});\n" +
				"\n" +
				"\t\t// Прохожие обратили внимание на сигнализацию.\n" +
				"\t\tsign.addResponseObject(() -> System.out.println(\"Passers-by noticed the alarm.\"));\n" +
				"\n" +
				"\t\tsign.alarmTrigger();\n" +
				"\t}\n" +
				"}\n\n" +
				"" +
				"Слева от -> параметры лямбда выражения, справа - тело выражения.\n" +
				"Можно сказать так: лямбда-выражение это способ использования объекта как функции.\n\n" +
				"" +
				"В случае если у нас есть методы, параметры которого в точности совпадают с парметрами метода лямбда-выражения,\n" +
				"можно воспользоваться следующей конструкцией:\n\n" +
				"" +
				"public class LambdaMain {\n" +
				"\t// Если у метода одинаковые параметры с методом функционального класса,\n" +
				"\t// то его можно использовать вместо лямбда-выражения\n" +
				"\tprivate static int signal() {\n" +
				"\t\tSystem.out.println(\"SIGNALLING!!!\");\n" +
				"\t\treturn 0;\n" +
				"\t}\n" +
				"\n" +
				"\tpublic static void main(String[] args) {\n" +
				"\t\tSignaling sign = new Signaling();\n" +
				"\t\tPolice police = new Police();\n" +
				"\t\tManager manager = new Manager();\n" +
				"\n" +
				"\t\tsign.addResponseObject(police);\n" +
				"\t\tsign.addResponseObject(manager);\n" +
				"\n" +
				"\t\tsign.addResponseObject(new Reacting() {\n" +
				"\t\t\t@Override\n" +
				"\t\t\tpublic void response() {\n" +
				"\t\t\t\tSystem.out.println(\"The security service goes to the place where the alarm was triggered\");\n" +
				"\t\t\t}\n" +
				"\t\t});\n" +
				"\n" +
				"\t\t// Прохожие обратили внимание на сигнализацию.\n" +
				"\t\tsign.addResponseObject(() -> System.out.println(\"Passers-by noticed the alarm.\"));\n" +
				"\t\t// так как совпадает набор типов параметров, можно записать так (возвращаемый тип не учитывается)\n" +
				"\t\tsign.addResponseObject(() -> LambdaMain.signal());\n" +
				"\t\t// но можно сократить\n" +
				"\t\tsign.addResponseObject(LambdaMain::signal);\n" +
				"\n" +
				"\t\tsign.alarmTrigger();\n" +
				"\t}\n" +
				"}\n\n" +
				"" +
				"Если метод нестатический, то указываем не класс, а ссылку на объект.\n\n" +
				"" +
				"Локальные переменные доступны и в анонимных классах и в ЛВ (захват переменных). Но только если переменные\n" +
				"финализирована. До Java 8 - необходим модификатор final, начиная с Java 8, final можно не писать, но мы\n" +
				"не должны изменять переменную. Попытка изменить переменную приведет к ошибке компиляции.\n" +
				"" +
				"Отлично видно преимущество работы с ЛВ при работе со Stream. Будет изучаться в дальнейшем.\n\n" +
				"" +
				"Примеры использования ЛВ, состоящих из единственного выражения:\n\n" +
				"" +
				"// ФИ с методом без параметров\n" +
				"@FunctionalInterface\n" +
				"interface MyInterface {\n" +
				"\t// возвращает имя\n" +
				"\tString getName();\n" +
				"}\n" +
				"\n" +
				"// ФИ с методом с двумя параметрами\n" +
				"@FunctionalInterface\n" +
				"interface MyInterface1 {\n" +
				"\t// сравнивает 2 числа\n" +
				"\tint compare(int a, int b);\n" +
				"}\n" +
				"\n" +
				"// ФИ с методом с двумя параметрами (шаблонный)\n" +
				"@FunctionalInterface\n" +
				"interface MyInterface2<T> {\n" +
				"\t// сравнивает 2 числа\n" +
				"\tboolean max(T a, T b);\n" +
				"}\n" +
				"\n" +
				"public class SimpleLambda {\n" +
				"\tpublic static void main(String[] args) {\n" +
				"\t\t// задаем логику реализации\n" +
				"\t\tMyInterface myInterface = () -> \"Ivan\";\n" +
				"\t\tSystem.out.println(myInterface.getName());\n" +
				"\t\tmyInterface = () -> new String(\"Alina\");\n" +
				"\t\tSystem.out.println(myInterface.getName());\n" +
				"\t\t\n" +
				"\t\tint x = 19;\n" +
				"\t\tint y = 25;\n" +
				"\t\t// задаем логику реализации\n" +
				"\t\tMyInterface1 myInterface1 = (a, b) -> a > b ? 1 : (a == b ? 0 : -1);\n" +
				"\t\tSystem.out.println(myInterface1.compare(x, y));\n" +
				"\n" +
				"\t\t// задаем логику реализации\n" +
				"\t\tMyInterface2<Double> maxValue = (a, b) -> a > b;\n" +
				"\t\tSystem.out.println(maxValue.max(18.2, 12.3));\n" +
				"\t}\n" +
				"}\n\n" +
				"" +
				"Пример работы с блочными ЛВ:\n\n" +
				"" +
				"// тестовый интерфейс, где мы работаем со списком разработчиков и вовзращаем изменный список\n" +
				"@FunctionalInterface\n" +
				"interface DeveloperInfo {\n" +
				"\tList<Developer> selfFunctional(List<Developer> list);\n" +
				"}\n" +
				"\n" +
				"\n" +
				"public class BlockLambda {\n" +
				"\tpublic static void main(String[] args) {\n" +
				"\t\t//создаем список разработчиков\n" +
				"\t\tList<Developer> developerList = new ArrayList<>();\n" +
				"\t\tdeveloperList.add(new Developer(\"Dev1\", \"Java\", 4000));\n" +
				"\t\tdeveloperList.add(new Developer(\"Dev2\", \"Python\", 3000));\n" +
				"\t\tdeveloperList.add(new Developer(\"Dev3\", \"PHP\", 5000));\n" +
				"\t\tdeveloperList.add(new Developer(\"Dev4\", \"C++\", 4500));\n" +
				"\t\tdeveloperList.add(new Developer(\"Dev5\", \"Python\", 4000));\n" +
				"\t\tdeveloperList.add(new Developer(\"Dev6\", \"PHP\", 4000));\n" +
				"\t\tdeveloperList.add(new Developer(\"Dev7\", \"Java\", 4500));\n" +
				"\t\tdeveloperList.add(new Developer(\"Dev8\", \"Python\", 2500));\n" +
				"\t\t\n" +
				"\t\t// задаем собственную логику реализации метода ФИ\n" +
				"\t\t// которая возвращает новый List\n" +
				"\t\tDeveloperInfo developerInfo = list -> {\n" +
				"\t\t\t// создаем новый список\n" +
				"\t\t\tList<Developer> newList = new ArrayList<>();\n" +
				"\t\t\tfor (Developer developer : list) {\n" +
				"\t\t\t\t// выделяем Java разработчиков\n" +
				"\t\t\t\tif (developer.getLanguage().equalsIgnoreCase(\"java\")) {\n" +
				"\t\t\t\t\t// добавляем в новый список\n" +
				"\t\t\t\t\tnewList.add(developer);\n" +
				"\t\t\t\t}\n" +
				"\t\t\t}\n" +
				"\t\t\t// возвращаем и метод ФИ этот новый список\n" +
				"\t\t\treturn newList;\n" +
				"\t\t};\n" +
				"\n" +
				"\t\t// проходимся по всем специалистам нового списка,\n" +
				"\t\t// который получается после выполнения собственной реализации метода ФИ\n" +
				"\t\tfor (Developer dev : developerInfo.selfFunctional(developerList)) {\n" +
				"\t\t\tSystem.out.println(dev);\n" +
				"\t\t}\n" +
				"\t}\n" +
				"}\n" +
				"\n" +
				"// тестовый класс Разработчик\n" +
				"class Developer {\n" +
				"\tprivate String name;\n" +
				"\tprivate String language;\n" +
				"\tprivate int salary;\n" +
				"\n" +
				"\tpublic Developer(String name, String language, int salary) {\n" +
				"\t\tthis.name = name;\n" +
				"\t\tthis.language = language;\n" +
				"\t\tthis.salary = salary;\n" +
				"\t}\n" +
				"\n" +
				"\tpublic String getName() {\n" +
				"\t\treturn name;\n" +
				"\t}\n" +
				"\n" +
				"\tpublic String getLanguage() {\n" +
				"\t\treturn language;\n" +
				"\t}\n" +
				"\n" +
				"\tpublic int getSalary() {\n" +
				"\t\treturn salary;\n" +
				"\t}\n" +
				"\n" +
				"\t@Override\n" +
				"\tpublic String toString() {\n" +
				"\t\treturn \"Developer{\" +\n" +
				"\t\t\t\t\"name='\" + name + '\\'' +\n" +
				"\t\t\t\t\", language='\" + language + '\\'' +\n" +
				"\t\t\t\t\", salary=\" + salary +\n" +
				"\t\t\t\t'}';\n" +
				"\t}\n" +
				"}\n\n" +
				"" +
				"ЛВ можно передавать в качестве аргумента метода. Для этого параметр, получающий это выражение в качестве\n" +
				"аргумента, должен иметь тип функционального интерфейса, совместимого с этим ЛВ.\n\n" +
				"" +
				"ЛВ может генерировать исключение. Но если оно генерирует проверяемое исключение, то последнее должно быть\n" +
				"совместимо с исключениями, перечесленными в выражении throws из объявления абстрактного метода в ФИ.\n" +
				"" +
				"" +
				"" +
				"" +
				"" +
				"");
	}

	@Override
	public String getTHEME() {
		return THEME;
	}
}
