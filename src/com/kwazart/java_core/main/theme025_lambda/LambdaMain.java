package com.kwazart.java_core.main.theme025_lambda;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 18.10.2020 23:24
 */

public class LambdaMain {
	// Если у метода одинаковые параметры с методом функционального класса,
	// то его можно использовать вместо лямбда-выражения
	private static int signal() {
		System.out.println("SIGNALLING!!!");
		return 0;
	}

	public static void main(String[] args) {
		Signaling sign = new Signaling();
		Police police = new Police();
		Manager manager = new Manager();

		sign.addResponseObject(police);
		sign.addResponseObject(manager);

		sign.addResponseObject(new Reacting() {
			@Override
			public void response() {
				System.out.println("The security service goes to the place where the alarm was triggered");
			}
		});

		// Прохожие обратили внимание на сигнализацию.
		sign.addResponseObject(() -> System.out.println("Passers-by noticed the alarm."));
		// так как совпадает набор типов параметров, можно записать так (возвращаемый тип не учитывается)
		sign.addResponseObject(() -> LambdaMain.signal());
		// но можно сократить
		sign.addResponseObject(LambdaMain::signal);

		sign.alarmTrigger();
	}
}
