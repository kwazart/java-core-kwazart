package com.kwazart.java_core.main.theme025_lambda;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 19.10.2020 0:07
 */

// класс Директор
public class Manager implements Reacting{
	// реакция директора
	public void managerReact() {
		// директор в панике
		System.out.println("Director in a panic");
	}

	@Override
	public void response() {
		managerReact();
	}
}
