package com.kwazart.java_core.main.theme025_lambda;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 18.10.2020 23:28
 */
// класс Полиция
public class Police implements Reacting{
	// полиция реагирует
	public void policeReact() {
		// Полицейские выехали к месту срабатывания сигнала
		System.out.println("Police officers drive to the place where the alarm was triggered");
	}

	@Override
	public void response() {
		policeReact();
	}
}
