package com.kwazart.java_core.main.theme025_lambda;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 18.10.2020 23:38
 */

// аннотация, которая заставляет компилятор контролировать, что интерфейс функциональный (т.е. только один метод)
@FunctionalInterface
public interface Reacting {
	// реагирование
	void response();
}
