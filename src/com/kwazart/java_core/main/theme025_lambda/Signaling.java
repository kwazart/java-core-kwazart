package com.kwazart.java_core.main.theme025_lambda;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 18.10.2020 23:22
 */

// Сигнализация
public class Signaling {
	// список всех, кто среагирует на сигнализацию
	private List<Reacting> responseObjects = new ArrayList<>();

	// добавляем объект реагирования - подписка на событие
	public void addResponseObject(Reacting object) {
		// проверяем, что объект существует и еще не содержится в списке
		// затем добавляем
		if (object != null && !(responseObjects.contains(object))) {
			responseObjects.add(object);
		}
	}

	// удаляем объект реагирования - отписка от событие
	public void removeResponseObject(Reacting object) {
		// проверяем, что объект существует и еще не содержится в списке
		// затем удаляем
		if (object != null && !(responseObjects.contains(object))) {
			responseObjects.remove(object);
		}
	}

	// срабатывание сигнала
	public void alarmTrigger() {
		System.out.println("UEEEEEEEEEEEEE!!! UEEEEEEEEEEEEE!!!");

		// перебираем весь список реагирующих объектов
		for (Reacting reactingObject : responseObjects) {
			// вызываем реакцию объектов
			reactingObject.response();
		}
	}
}
