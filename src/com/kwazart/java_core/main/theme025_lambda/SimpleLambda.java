package com.kwazart.java_core.main.theme025_lambda;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 19.10.2020 11:35
 */

// ФИ с методом без параметров
@FunctionalInterface
interface MyInterface {
	// возвращает имя
	String getName();
}

// ФИ с методом с двумя параметрами
@FunctionalInterface
interface MyInterface1 {
	// сравнивает 2 числа
	int compare(int a, int b);
}

// ФИ с методом с двумя параметрами (шаблонный)
@FunctionalInterface
interface MyInterface2<T> {
	// сравнивает 2 числа
	boolean max(T a, T b);
}

public class SimpleLambda {
	public static void main(String[] args) {
		// задаем логику реализации
		MyInterface myInterface = () -> "Ivan";
		System.out.println(myInterface.getName());
		myInterface = () -> new String("Alina");
		System.out.println(myInterface.getName());

		int x = 19;
		int y = 25;
		// задаем логику реализации
		MyInterface1 myInterface1 = (a, b) -> a > b ? 1 : (a == b ? 0 : -1);
		System.out.println(myInterface1.compare(x, y));

		// задаем логику реализации
		MyInterface2<Double> maxValue = (a, b) -> a > b;
		System.out.println(maxValue.max(18.2, 12.3));
	}
}
