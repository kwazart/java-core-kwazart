package com.kwazart.java_core.main.theme05_methods;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 23.09.2020 2:09
 */

public class MethodClass {
	private int x;

	public void methodA() {
		final int y = 100;

		class TestClass{
			int a;
			public TestClass(int a) {
				this.a = a;
			}

			public void printValues() {
				System.out.println(x);
				System.out.println(y);
			}

			@Override
			public String toString() {
				return "TestClass{" +
						"a=" + a +
						'}';
			}
		}
		// y = 101; // если уберем коммент из переменной y,
		// и уберем final, то произойдет ошибка компиляции
		// вывод: класс в методе, может обрабатывать
		// только константы метода, но любые поля объекта

		TestClass testClass = new TestClass(10);
		methodB(testClass);
	}

	private void methodB(Object obj) {
		System.out.println(obj);
	}
}
