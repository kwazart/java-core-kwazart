package com.kwazart.java_core.main.theme06_class_object;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 28.09.2020 0:38
 */

public class NewClassInfo implements InfoPrintable {
	public static String str = "Эта строка принадлежит классу, а не объекту";
	public static int level = 5;
	public static final String THEME = "Классы и объекты";

	public void printInfo() {
		System.out.println("Класс - это сущность, которая описывает какой-либо предмет с помощью характеристик и поведений." +
				"\nОбъект явялется экземпляром класса, который имеет свои значения заданных в класса характеристик." +
				"\n\t- Все классы в Java наследуются от класса Object." +
				"\n\t- Доступ к полю или методу объекта осуществляется через оператор точка - ." +
				"\n\t- Если объект объявить, но не проинициализировать, тогда он будет равен null (пустая ссылка)." +
				"\n\t  Если обратиться к методам или полям такого объекта, произойдет ошибка." +
				"\n\nДля создания простого класса необходимо использовать следующий синтаксис:" +
				"\n\n\t[public] [abstract] class <ClassName> {\n\t\tтело класса\n\t}" +
				"\n\nДля создания простого объекта класса ClassName необходимо использовать следующий синтаксис:" +
				"\n\n\tClassName <object name> = new ClassName();");

		System.out.println("\n\nВ случае если метод или свойство объявлено с ключевым словом static, оно считается" +
				"\nпринадлежащим класса, а не объекту. Запомните, что статичные методы могут обращаться только" +
				"\nк статичным полям или методам." +
				"\nВ данном коде Вы можете найти несколько свойств и методов," +
				"\nкоторые принадлежат текущему класса NewClass. Переменные класса:" +
				"\n\n\tpublic static String str = \"Эта строка принадлежит классу, а не объекту\";" +
				"\n\tpublic static int level = 5;" +
				"\n\nМетоды класса:" +
				"\n\n\tpublic static String invokeMethod() {\n" +
				"\t\tprintInfoAboutThisClass();\n" +
				"\t\treturn \"Был вызван метод printInfo() из текущего класса.\";\n" +
				"\t}" +
				"\n\nДля того, чтобы обратиться к данным полям и методам, необходимо использовать имя класса," +
				"\nно и обращение через объекты данного класса также возможно:" +
				"\n\n\tNewClass.str;" +
				"\n\tNewClass.level;" +
				"\n\tNewClass.invokeMethod();");

		System.out.println("\nКонстанты - это статические поля класса, у которых установлено ключевое слово final." +
				"\nТ.е. это поле изменить нельзя. Например:" +
				"\n\n\tpublic static final String FIRST_STRING_FOR_CLASS = \"Какая-то строка\";" +
				"\n\nИмена таких констант принятон обозначать заглвными буквами, а слова разделять" +
				"\nнижними подчеркиваниями.");
	}

	public static String invokeMethod() {
		method();
		return "Был вызван метод method() из текущего класса.";
	}

	private static void method() {
	}

	public String getTHEME() {
		return THEME;
	}
}
