package com.kwazart.java_core.main.theme07_constructors;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 21.09.2020 10:02
 */

// Перегруженные конструкторы
public class ConstClassSecond {
    int a;
    int b;

    public ConstClassSecond(int a) {
        this(a, a);
    }

    public ConstClassSecond(int a, int b) {
        this.a = a;
        this.b = b;
    }
}
