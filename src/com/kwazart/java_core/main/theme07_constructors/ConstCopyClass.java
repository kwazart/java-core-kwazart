package com.kwazart.java_core.main.theme07_constructors;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 21.09.2020 14:24
 */

public class ConstCopyClass {
    private int a;
    private int b;

    public ConstCopyClass(ConstCopyClass object) {
        this.a = object.getA();
        this.b = object.getB();
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }
}
