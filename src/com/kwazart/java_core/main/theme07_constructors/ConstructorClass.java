package com.kwazart.java_core.main.theme07_constructors;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 20.09.2020 22:50
 */

public class ConstructorClass {
    int a;
    int b;
    int c;

    public ConstructorClass(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public int calculateMultiplication() {
        return a * b * c;
    }
}
