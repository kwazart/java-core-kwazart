package com.kwazart.java_core.main.theme07_constructors;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 28.09.2020 0:41
 */

public class ConstructorClassInfo implements InfoPrintable {
	public static final String THEME = "Конструкторы";

	public void printInfo() {
		System.out.println("\nКонструктор - это специальный метод, который вызывается при создании нового объекта. " +
				"\nНе всегда удобно инициализировать все переменные класса при создании его экземпляра." +
				"\nКонструктор инициализирует объект непосредственно во время создания. Имя конструктора совпадает" +
				"\nс именем класса, включая регистр, а по синтаксису конструктор похож на метод без возвращаемого значения." +
				"\nКонструктор данного класса можно написать так:" +
				"\n\n\tpublic NewClass() {" +
				"\n\t}" +
				"\n\nДанный конструктор называется конструктором по умолчанию. Он по default установлен в любом классе." +
				"\nКонструктор можно как и методы перегружать, то есть задавать разные тип и/или количество аргументов," +
				"\nберя во внимание поля класса. Приведен в пример класс ConstructorClass." +
				"\n\n\tpublic class ConstructorClass {\n" +
				"\t\tint a;\n" +
				"\t\tint b;\n" +
				"\t\tint c;\n" +
				"    \n" +
				"\t\tpublic ConstructorClass(int a, int b, int c) {\n" +
				"\t\t\tthis.a = a;\n" +
				"\t\t\tthis.b = b;\n" +
				"\t\t\tthis.c = c;\n" +
				"\t\t}\n" +
				"    \n" +
				"\t\tpublic int calculateMultiplication() {\n" +
				"\t\t\treturn a * b * c;\n" +
				"\t\t}\n" +
				"\t}" +
				"\n\nЧтобы создать объект класса ConstructorClass необходимо использовать следующий синтаксис:" +
				"\n\n\tConstructorClass newObject = new ConstructorClass(value1, value2, value3);" +
				"\n\nПричем value1, value2, value3 должны быть целыми числами." +
				"\nКлючевое слово this говорит, что мы обращаемся к текущему объекту." +
				"\n\nЕсли в классе присутствует явные конструктор, то он задает правила по которым будут создаваться" +
				"\nобъекты. Если в классе будет несколько конструкторов, то действует принцип перегруженных методов." +
				"\nПример:" +
				"\n\n\tpublic class ConstClassSecond {\n" +
				"\t\tint a;\n" +
				"\t\tint b;\n" +
				"    \n" +
				"\t\tpublic ConstClassSecond(int a) {\n" +
				"\t\t\tthis(a, a);\n" +
				"\t\t}\n" +
				"    \n" +
				"\t\tpublic ConstClassSecond(int a, int b) {\n" +
				"\t\t\tthis.a = a;\n" +
				"\t\t\tthis.b = b;\n" +
				"\t\t}\n" +
				"\t}" +
				"\n\nЧерез ключевое слово this мы вызываем конструктор с соответствующим типом и количеством праметров.");

		System.out.println("\n\nТакже с помощью конструкторов можно копировать объекты:" +
				"\n\n\tpublic class ConstCopyClass {\n" +
				"\t\tprivate int a;\n" +
				"\t\tprivate int b;\n" +
				"    \n" +
				"\t\tpublic ConstCopyClass(ConstCopyClass object) {\n" +
				"\t\t\tthis.a = object.getA();\n" +
				"\t\t\tthis.b = object.getB();\n" +
				"\t\t}\n" +
				"\n" +
				"\t\tpublic int getA() {\n" +
				"\t\t\treturn a;\n" +
				"\t\t}\n" +
				"\n" +
				"\t\tpublic int getB() {\n" +
				"\t\t\treturn b;\n" +
				"\t\t}\n" +
				"\t}");
	}

	public String getTHEME() {
		return THEME;
	}
}
