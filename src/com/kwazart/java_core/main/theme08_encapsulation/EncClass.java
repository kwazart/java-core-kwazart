package com.kwazart.java_core.main.theme08_encapsulation;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 21.09.2020 10:23
 */

public class EncClass {
    private int firstValue;
    private int secondValue;

    public void action(int ch) {
        switch (ch) {
            case 1:
                System.out.println(sum());
                break;
            case 2:
                System.out.println(sub());
                break;
            default:
                System.out.println("Wrong input.");
        }
    }

    private int sum() {
        return firstValue + secondValue;
    }

    private int sub() {
        return firstValue - secondValue;
    }

    public int getFirstValue() {
        return firstValue;
    }

    public void setFirstValue(int firstValue) {
        this.firstValue = firstValue;
    }

    public int getSecondValue() {
        return secondValue;
    }

    public void setSecondValue(int secondValue) {
        this.secondValue = secondValue;
    }
}
