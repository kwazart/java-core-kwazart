package com.kwazart.java_core.main.theme08_encapsulation;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 21.09.2020 10:07
 */

public class EncapsulationInfo implements InfoPrintable {
    public static final String THEME = "Инкапсуляция";

    public void printInfo() {
        System.out.println("\nИнкапсуляция - сокрытие доступа к некоторым полям и методам класса извне." +
                "\nИнкапсуляция скрывает от пользователя реализацию кода, который не должен быть доступен" +
                "\nпользователю. Т.е. инкапсуляция помогает разграничить доступ к тем или иным частям кода." +
                "\nСокрытие данных достигается путем добавления модификаторов доступа отличным от public, и доступ" +
                "\nк тем или иным частям кода через методы (в случае с полями класса через геттеры и сеттеры.");

        System.out.println("\nМодификаторы доступа:" +
                "\nprivate\t - доступ к данным только из текущего класса." +
                "\n[default]\t - модификатор доступа по умолчанию (не указывается) предоставляет доступ к данным" +
                "\nтолько в том же пакете, где находится класс в котором Вы сейчас работаете." +
                "\nprotected\t - то же самое, что и default, но доступ также предоставляется классам наследникам." +
                "\npublic\t - доступ к элементу кода из любой части вашей программы.");

        System.out.println("\n\nПример:" +
                "\n\n\tpublic class EncClass {\n" +
                "\t\tprivate int firstValue;\n" +
                "\t\tprivate int secondValue;\n" +
                "    \n" +
                "\t\tpublic void action(int ch) {\n" +
                "\t\t\tswitch (ch) {\n" +
                "\t\t\t\tcase 1:\n" +
                "\t\t\t\t\tSystem.out.println(sum());\n" +
                "\t\t\t\t\tbreak;\n" +
                "\t\t\t\tcase 2:\n" +
                "\t\t\t\t\tSystem.out.println(sub());\n" +
                "\t\t\t\t\tbreak;\n" +
                "\t\t\t\tdefault:\n" +
                "\t\t\t\t\tSystem.out.println(\"Wrong input.\");\n" +
                "\t\t\t}\n" +
                "\t\t}\n" +
                "    \n" +
                "\t\tprivate int sum() {\n" +
                "\t\t\treturn firstValue + secondValue;\n" +
                "\t\t}\n" +
                "    \n" +
                "\t\tprivate int sub() {\n" +
                "\t\t\treturn firstValue - secondValue;\n" +
                "\t\t}\n" +
                "\n" +
                "\t\tpublic int getFirstValue() {\n" +
                "\t\t\treturn firstValue;\n" +
                "\t\t}\n" +
                "\n" +
                "\t\tpublic void setFirstValue(int firstValue) {\n" +
                "\t\t\tthis.firstValue = firstValue;\n" +
                "\t\t}\n" +
                "\n" +
                "\t\tpublic int getSecondValue() {\n" +
                "\t\t\treturn secondValue;\n" +
                "\t\t}\n" +
                "\n" +
                "\t\tpublic void setSecondValue(int secondValue) {\n" +
                "\t\t\tthis.secondValue = secondValue;\n" +
                "\t\t}\n" +
                "\t}" +
                "\n\nВ данном примере доступ к полям класса осуществлется через методы get, а значения " +
                "\nустанавливаются через методы set. Также закрыт доступ к методам sum и sub, т.к. пользователю" +
                "\nнет смысла в данных методах, они являются важными только для методов нашего класса.");
    }

    public String getTHEME() {
        return THEME;
    }
}
