package com.kwazart.java_core.main.theme09_inheritance;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 21.09.2020 11:10
 */

public class InhClass extends C {
}

class A {
    int a;
    protected int b;
    A(int a) {
        this.a = a;
        this.b = a;
    }

    public void methodA() {
        System.out.println("text from A class");
    }
}

class B extends A{

    public B() { super(10); }

    B(int a) { super(a); }

    protected void methodB() {
        methodA(); // доступ есть к данному методу
        System.out.println(super.a + b);
    }
}

class C extends B {
    public void methodC() {
        methodA();
        methodB();
    }
}

