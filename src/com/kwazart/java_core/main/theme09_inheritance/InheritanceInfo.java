package com.kwazart.java_core.main.theme09_inheritance;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 21.09.2020 11:03
 */

public class InheritanceInfo implements InfoPrintable {
    public static final String THEME = "Наследование";

    public void printInfo() {
        System.out.println("\nНаследование - процесс перенимания свойств и поведение другого класса." +
                "\nПри наследовании получается построить иерархическую цепочку класса, где есть классы родители" +
                "\nи классы наследники. Класс, который наследует поля и поведение, называется подклассом или" +
                "\nклассом наследником. А класс свойства и поведение которого наследуются, называется суперклассом." +
                "\nНаследование происходит путем добавления ключевого слова extends и указания имени суперкласса." +
                "\nПример:" +
                "\n\n\tpublic class InhClass extends C { }\n" +
                "\n" +
                "\tclass A {\n" +
                "\t\tint a;\n" +
                "\t\tprotected int b;\n" +
                "\t\tA(int a) {\n" +
                "\t\t\tthis.a = a;\n" +
                "\t\t\tthis.b = a;\n" +
                "\t\t}\n" +
                "\n" +
                "\t\tprotected void methodA() {\n" +
                "\t\t\tSystem.out.println(\"text from A class\");\n" +
                "\t\t}\n" +
                "\t}\n" +
                "\n" +
                "\tclass B extends A{\n" +
                "\n" +
                "\t\tB() { super(10); }\n" +
                "\n" +
                "\t\tB(int a) { super(a); }\n" +
                "\n" +
                "\t\tvoid methodB() {\n" +
                "\t\t\tmethodA(); // доступ есть к данному методу\n" +
                "\t\t\tSystem.out.println(super.a + b);\n" +
                "\t\t}\n" +
                "\t}\n" +
                "\n" +
                "\tclass C extends B {\n" +
                "\t\tpublic void methodC() {\n" +
                "\t\t\tmethodA();\n" +
                "\t\t\tmethodB();\n" +
                "\t\t}\n" +
                "\t}" +
                "\n\nТеперь мы может вызывать из объекта класса InhClass методы methodA() и methodC(). А метод" +
                "\nmethodB доступ в классе наследнике C, помимо самого объекта класса B." +
                "\nМы может обращаться к суперклассу через ключевое слово super. " +
                "\nК полям (super.<value>), методам (super.<method name>(), конструкторам (super()).");

        System.out.println("\n\nКлассы могут быть абстрактными, т.е. от них невозможно создать никакого объекта через" +
                "\nконструкторы, но они имеют те же самые свойства и поведения, что и обычные класса, в том числе" +
                "\nи конструкторы." +
                "\nМетоды также могут быть абстрактными, это означает, что данные методы не имеют тела метода. И все" +
                "\nклассы наследники должны будут их переопределить (т.е. методы должны быть реализованы в подклассах)." +
                "\nЕсли в классе будет хотя бы один абстрактный метод, то такой класс является абстрактным и должен" +
                "\nбыть соответствующим образом помечен - public abstract class ClassName() {}. Методы помечаются " +
                "\nпосле указания модификатора доступа по такому же принципу.");
    }

    public String getTHEME() {
        return THEME;
    }
}
