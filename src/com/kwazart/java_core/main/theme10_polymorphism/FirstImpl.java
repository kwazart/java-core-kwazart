package com.kwazart.java_core.main.theme10_polymorphism;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 21.09.2020 15:29
 */

public class FirstImpl implements InterfacePrintable {
    @Override
    public void print() {
        System.out.println("First Implementation: method print without args");
    }

    @Override
    public void print(String str) {
        System.out.println("First Implementation: method print with args: " + str);
    }
}
