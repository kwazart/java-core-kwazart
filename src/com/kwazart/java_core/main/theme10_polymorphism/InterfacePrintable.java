package com.kwazart.java_core.main.theme10_polymorphism;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 21.09.2020 15:23
 */
public interface InterfacePrintable {
    void print();
    void print(String str);

    int x = 10;

    default void printText() {
        System.out.println("text from interface default method");
    }
}
