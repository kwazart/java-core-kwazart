package com.kwazart.java_core.main.theme10_polymorphism;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 21.09.2020 14:32
 */

public abstract class PolClass {
    public static void main(String[] args) {
        PolClass objectA = new TestFirst();
        PolClass objectB = new TestSecond();

        invokeMethods(objectA);
        invokeMethods(objectB);
    }

    private static void invokeMethods(PolClass object) {
        object.methodA();
        System.out.println(object.methodB());
        System.out.println("=====================");
    }

    protected abstract void methodA();
    protected String methodB() {
        return "methodB() from PolClass";
    }
}

class TestFirst extends PolClass {

    @Override
    protected void methodA() {
        System.out.println("methodA() from TestFirst");
    }
}

class TestSecond extends PolClass {
    @Override
    protected String methodB() {
        return "methodB() from TestSecond";
    }

    @Override
    protected void methodA() {
        System.out.println("methodA() from TestSecond");
    }
}
