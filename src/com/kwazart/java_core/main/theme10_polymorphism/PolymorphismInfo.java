package com.kwazart.java_core.main.theme10_polymorphism;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 21.09.2020 14:29
 */

public class PolymorphismInfo implements InfoPrintable {
    public static final String THEME = "Полиморфизм";

    public void printInfo() {
        System.out.println("\nПолиморфизм - способность объекта принимать множество различных форм." +
                "\nПолиморфизм требует либо наследования, либо реализации интерфейса." +
                "\nПример:" +
                "\n\n\tpublic abstract class PolClass {\n" +
                "\t\tpublic static void main(String[] args) {\n" +
                "\t\t\tPolClass objectA = new TestFirst();\n" +
                "\t\t\tPolClass objectB = new TestSecond();\n" +
                "\n" +
                "\t\t\tinvokeMethods(objectA);\n" +
                "\t\t\tinvokeMethods(objectB);\n" +
                "\t\t}\n" +
                "\n" +
                "\t\tprivate static void invokeMethods(PolClass object) {\n" +
                "\t\t\tobject.methodA();\n" +
                "\t\t\tSystem.out.println(object.methodB());\n" +
                "\t\t\tSystem.out.println(\"=====================\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\tprotected abstract void methodA();\n" +
                "\t\tprotected String methodB() {\n" +
                "\t\t\treturn \"methodB() from PolClass\";\n" +
                "\t\t}\n" +
                "\t}\n" +
                "\n" +
                "\tclass TestFirst extends PolClass {\n" +
                "\n" +
                "\t\t@Override\n" +
                "\t\tprotected void methodA() {\n" +
                "\t\t\tSystem.out.println(\"methodA() from TestFirst\");\n" +
                "\t\t}\n" +
                "\t}\n" +
                "\n" +
                "\tclass TestSecond extends PolClass {\n" +
                "\t\t@Override\n" +
                "\t\tprotected String methodB() {\n" +
                "\t\t\treturn \"methodB() from TestSecond\";\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t@Override\n" +
                "\t\tprotected void methodA() {\n" +
                "\t\t\tSystem.out.println(\"methodA() from TestSecond\");\n" +
                "\t\t}\n" +
                "\t}" +
                "\n\nТ.е. мы можем создать 2 объекта имеющих тип суперкласса, и обращаться с ним именно как с объектами" +
                "\nсуперкласса, но как только мы будем обращаться к каким либо методам, будем происходить динамическая" +
                "\nдиспетчеризация, которая появляется именно во время выполнения байт-кода, но не во время компиляции." +
                "\nПроизойдет проверка объекта на который ссылается переменная суперкласса, затем произойдет поиск" +
                "\nпереопределенного метода в данном классе, и в случае обнаружения будет выполенен именно он.");

        System.out.println("Интерфейс - абстрактная сущность в которой указано поведение (методы), которые еще не" +
                "\nреализованы (начиная с Java 8 появляется реализация методов по умолчанию). Все сигнатуры методов" +
                "\nнеыявно указаны как public. Все переменные, которые могут быть созданы в интерфесе неявно создаются" +
                "\nкак static final (т.е. константы)." +
                "\nПример:" +
                "\n\n\tpublic class StartClass {\n" +
                "\t\tpublic static void main(String[] args) {\n" +
                "\t\t\tInterfacePrintable object1 = new FirstImpl();\n" +
                "\t\t\tInterfacePrintable object2 = new SecondImpl();\n" +
                "\n" +
                "\t\t\tinvokeMethods(object1);\n" +
                "\t\t\tinvokeMethods(object2);\n" +
                "\t\t}\n" +
                "\n" +
                "\t\tprivate static void invokeMethods(InterfacePrintable object) {\n" +
                "\t\t\tobject.print();\n" +
                "\t\t\tobject.print(\"from invokeMethods()\");\n" +
                "\t\t\tobject.printText();\n" +
                "\t\t\tSystem.out.println(\"==================\");\n" +
                "\t\t}\n" +
                "\t}" +
                "\n\n\tpublic interface InterfacePrintable {\n" +
                "\t\tvoid print();\n" +
                "\t\tvoid print(String str);\n" +
                "\n" +
                "\t\tint x = 10;\n" +
                "\n" +
                "\t\tdefault void printText() {\n" +
                "\t\t\tSystem.out.println(\"text from interface default method\");\n" +
                "\t\t}\n" +
                "\t}" +
                "\n\n\tpublic class FirstImpl implements InterfacePrintable {\n" +
                "\t\t@Override\n" +
                "\t\tpublic void print() {\n" +
                "\t\t\tSystem.out.println(\"First Implementation: method print without args\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t@Override\n" +
                "\t\tpublic void print(String str) {\n" +
                "\t\t\tSystem.out.println(\"First Implementation: method print with args: \" + str);\n" +
                "\t\t}\n" +
                "\t}" +
                "\n\n\tpublic class SecondImpl implements InterfacePrintable {\n" +
                "\t\t@Override\n" +
                "\t\tpublic void print() {\n" +
                "\t\t\tSystem.out.println(\"Second Implementation: method print without args\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t@Override\n" +
                "\t\tpublic void print(String str) {\n" +
                "\t\t\tSystem.out.println(\"Second Implementation: method print with args: \" + str);\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t@Override\n" +
                "\t\tpublic void printText() {\n" +
                "\t\t\tSystem.out.println(\"new realize from Second Implementation\");\n" +
                "\t\t}\n" +
                "\t}");

        System.out.println("\n\nРазница между интерфейсом и абстрактным классом:" +
                "\n\t1. Интерфейс описывает только поведение. У него нет состояния. А у абстрактного класса состояние " +
                "\n\tесть: он описывает и то, и другое." +
                "\n\t2. Абстрактный класс связывает между собой и объединяет классы, имеющие очень близкую связь. " +
                "\n\tВ то же время, один и тот же интерфейс могут реализовать классы, у которых вообще нет ничего общего." +
                "\n\t3. Классы могут реализовывать сколько угодно интерфейсов, но наследоваться можно только от одного класса.");
    }

    public String getTHEME() {
        return THEME;
    }
}
