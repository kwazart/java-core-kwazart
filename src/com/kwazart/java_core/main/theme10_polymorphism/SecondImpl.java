package com.kwazart.java_core.main.theme10_polymorphism;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 21.09.2020 15:31
 */

public class SecondImpl implements InterfacePrintable {
    @Override
    public void print() {
        System.out.println("Second Implementation: method print without args");
    }

    @Override
    public void print(String str) {
        System.out.println("Second Implementation: method print with args: " + str);
    }

    @Override
    public void printText() {
        System.out.println("new realize from Second Implementation");
    }
}
