package com.kwazart.java_core.main.theme10_polymorphism;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 21.09.2020 15:33
 */

public class StartClass {
    public static void main(String[] args) {
        InterfacePrintable object1 = new FirstImpl();
        InterfacePrintable object2 = new SecondImpl();

        invokeMethods(object1);
        invokeMethods(object2);
    }

    private static void invokeMethods(InterfacePrintable object) {
        object.print();
        object.print("from invokeMethods()");
        object.printText();
        System.out.println("==================");
    }
}
