package com.kwazart.java_core.main.theme11_enum;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 20.09.2020 22:06
 */

public enum EnumClass {
    FIRST_VALUE("one"){
        public String getValue() { return "first"; }
    },
    SECOND_VALUE("two") {
        public String getValue() { return "second"; }
    },
    THIRD_VALUE("three"){
        public String getValue() { return "third"; }
    };

    private String value;
    EnumClass(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
