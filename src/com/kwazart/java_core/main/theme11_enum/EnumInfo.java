package com.kwazart.java_core.main.theme11_enum;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 28.09.2020 0:49
 */

public class EnumInfo implements InfoPrintable {
	public static final String THEME = "Enum";

	public void printInfo() {
		System.out.println("\n\nКроме отдельных примитивных типов данных и классов в Java есть такой тип " +
				"\nкак enum или перечисление. Перечисления представляют набор логически связанных констант. " +
				"\nОбъявление перечисления происходит с помощью оператора enum, после которого " +
				"\nидет название перечисления. Затем идет список элементов перечисления через запятую." +
				"\nПример:" +
				"\n\n\tpublic enum EnumClass {\n" +
				"\t\tFIRST_VALUE,\n" +
				"\t\tSECOND_VALUE,\n" +
				"\t\tTHIRD_VALUE;\n" +
				"\t}" +
				"\n\nПеречисления, как и обычные классы, могут определять конструкторы, поля и методы." +
				"\nСлудеющий пример:" +
				"\n\n\tpublic enum EnumClass {\n" +
				"\t\tFIRST_VALUE(\"one\"),\n" +
				"\t\tSECOND_VALUE(\"two\"),\n" +
				"\t\tTHIRD_VALUE(\"three\");\n" +
				"    \n" +
				"\t\tprivate String value;\n" +
				"\t\tEnumClass(String value) {\n" +
				"\t\t\tthis.value = value;\n" +
				"\t\t}\n" +
				"\n" +
				"\t\tpublic String getValue() {\n" +
				"\t\t\treturn value;\n" +
				"\t\t}\n" +
				"\t}" +
				"\n\nПеречисление фактически представляет новый тип, поэтому мы можем определить " +
				"\nпеременную данного типа и использовать ее (используем первый пример):" +
				"\n\n\tEnumClass value = EnumClass.SECOND_VALUE;" +
				"\n\nКаждое перечисление имеет статический метод values(). Он возвращает массив всех констант перечисления." +
				"\n\n\tEnumClass[] enums = EnumClass.values();\n" +
				"\n\n\tfor (EnumClass s : enums) { System.out.println(s); }" +
				"\n\nМетод ordinal() возвращает порядковый номер определенной константы (нумерация начинается с 0)" +
				"\n\n\tSystem.out.println(EnumClass.THIRD_VALUE.ordinal());" +
				"\n\nПеречисление EnumClass (второй пример) определяет приватное поле value для хранения значения, " +
				"\nа с помощью метода getValue оно возвращается. Через конструктор передается для него значение" +
				"\nСледует отметить, что конструктор по умолчанию приватный, то есть имеет модификатор private." +
				"\nЛюбой другой модификатор будет считаться ошибкой. Поэтому создать константы перечисления с " +
				"\nпомощью конструктора мы можем только внутри перечисления." +
				"\n\nТакже можно определять методы для отдельных констант:" +
				"\n\n\tpublic enum EnumClass {\n" +
				"\t\tFIRST_VALUE(\"one\"){\n" +
				"\t\t\tpublic String getValue() { return \"first\"; }\n" +
				"\t\t},\n" +
				"\t\tSECOND_VALUE(\"two\") {\n" +
				"\t\t\tpublic String getValue() { return \"second\"; }\n" +
				"\t\t},\n" +
				"\t\tTHIRD_VALUE(\"three\"){\n" +
				"\t\t\tpublic String getValue() { return \"third\"; }\n" +
				"\n" +
				"\t\tpublic String getValue() {\n" +
				"\t\t\treturn value;\n" +
				"\t\t}\n" +
				"\t}" +
				"\n\nЭтими методами можно воспользоваться так:" +
				"\n\n\tEnumClass ec = EnumClass.SECOND_VALUE;\n" +
				"\tSystem.out.println(ec.getValue());");
	}

	public String getTHEME() {
		return THEME;
	}
}
