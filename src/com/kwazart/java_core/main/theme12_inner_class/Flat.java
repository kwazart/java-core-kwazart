package com.kwazart.java_core.main.theme12_inner_class;

import java.util.Random;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 23.09.2020 1:17
 */

public class Flat {
	private int high;

	public Flat(int high) {
		this.high = high;
	}

	private class Floor {
		public void colorFloor() {
			System.out.println("Floor is colored in brown color");
		}
	}

	private class Wall {
		public String printWall(String sign) {
			return "create print at the wall";
		}
	}

	private class Roof {
		double calculateRoof() {
			return high * 1.6;
		}
	}

	public void buildFlat() {
		new Floor().colorFloor();
		Wall wall = new Wall();
		wall.printWall("test sign");
		Roof roof = new Roof();
		double high = roof.calculateRoof();
		System.out.println(high);
	}
}
