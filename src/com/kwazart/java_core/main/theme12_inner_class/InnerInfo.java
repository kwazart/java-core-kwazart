package com.kwazart.java_core.main.theme12_inner_class;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.1
 * Created 28.09.2020 0:52
 */

public class InnerInfo implements InfoPrintable {
	public static final String THEME = "Внутренние классы";

	public void printInfo() {
		System.out.println("\nВнутренний INNER (нестатический класс). " +
				"\nОбычно являются приватными (не обязательно). Служат для разграничения логики." +
				"\nОтносятся к самому объекту. Имеют доступ к полям объекта." +
				"\nДанные классы служат для конструирования более сложных классов или для разделения" +
				"\nболее сложного объекта на более простые. " +
				"\nПример:" +
				"\n\n\tpublic class Flat {\n" +
				"\t\tprivate int high;\n" +
				"\n" +
				"\t\tpublic Flat(int high) {\n" +
				"\t\t\tthis.high = high;\n" +
				"\t\t}\n" +
				"\n" +
				"\t\tprivate class Floor {\n" +
				"\t\t\tpublic void colorFloor() {\n" +
				"\t\t\t\tSystem.out.println(\"Floor is colored in brown color\");\n" +
				"\t\t\t}\n" +
				"\t\t}\n" +
				"\n" +
				"\t\tprivate class Wall {\n" +
				"\t\t\tpublic String printWall(String sign) {\n" +
				"\t\t\t\\treturn \"create print at the wall\";\n" +
				"\t\t\t}\n" +
				"\t\t}\n" +
				"\n" +
				"\t\tprivate class Roof {\n" +
				"\t\t\tdouble calculateRoof() {\n" +
				"\t\t\t\treturn high * 1.6;\n" +
				"\t\t\t}\n" +
				"\t\t}\n" +
				"\n" +
				"\t\tpublic void buildFlat() {\n" +
				"\t\t\tnew Floor().colorFloor();\n" +
				"\t\t\tWall wall = new Wall();\n" +
				"\t\t\twall.printWall(\"test sign\");\n" +
				"\t\t\tRoof roof = new Roof();\n" +
				"\t\t\tdouble high = roof.calculateRoof();\n" +
				"\t\t\tSystem.out.println(high);\n" +
				"\t\t}\n" +
				"\t}" +
				"\n\nЕще пример:" +
				"\n\n\tpublic class Test\n" +
				"\t{\n" +
				"\t\tpublic static void main(String[] args)\n" +
				"\t\t{\n" +
				"\t\t\tOuterClass3 outerClass3 = new OuterClass3();\n" +
				"\t\t\tSystem.out.println(outerClass3.getX());\n" +
				"\t\t\tOuterClass3.Inner inner = outerClass3.new Inner();\n" +
				"\t\t\tinner.x(4);\n" +
				"\t\t\tSystem.out.println(outerClass3.getX());\n" +
				"\n" +
				"\t\t}\n" +
				"\t}\n" +
				"\n" +
				"\t/**\n" +
				"\t * Обрамляющий класс\n" +
				"\t */\n" +
				"\tclass OuterClass3\n" +
				"\t{\n" +
				"\t\t// переменная обрамляющего класса\n" +
				"\t\tprivate int x;\n" +
				"\n" +
				"\t\tpublic int getX()\n" +
				"\t\t{\n" +
				"\t\t\treturn x;\n" +
				"\t\t}\n" +
				"\t/**\n" +
				"\t * внутренний класс\n" +
				"\t */\n" +
				"\tclass Inner\n" +
				"\t{\n" +
				"\t\t// переменная внутреннего класса\n" +
				"\t\tprivate int x;\n" +
				"\n" +
				"\t\tpublic void x(int x)\n" +
				"\t\t{\n" +
				"\t\t\tx = 1;\n" +
				"\t\t\tthis.x = 2;\n" +
				"\t\t\tOuterClass3.this.x = 3;\n" +
				"\t\t}\n" +
				"\n" +
				"\t\tpublic int getX()\n" +
				"\t\t{\n" +
				"\t\t\treturn x;\n" +
				"\t\t}\n" +
				"\t\t//нельзя объявлять статические методы и переменные\n" +
				"\t\t//public static void test(){}\n" +
				"\n" +
				"\t\t\t// внутренний класс в Inner\n" +
				"\t\t\tclass Inner2\n" +
				"\t\t\t{\n" +
				"\t\t\t\tint x;\n" +
				"\n" +
				"\t\t\t\tpublic void getX(int x)\n" +
				"\t\t\t\t{\n" +
				"\t\t\t\t\tOuterClass3.Inner.this.x = 5;\n" +
				"\t\t\t\t}\n" +
				"\t\t\t}\n" +
				"\t\t}\n" +
				"\t}");
	}

	public String getTHEME() {
		return THEME;
	}
}
