package com.kwazart.java_core.main.theme13_nested_class;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 23.09.2020 1:56
 */

public class Kitchen {
	public static void main(String[] args) {
		Kitchen.Table table = new Table(); // если создаем объект в этом же классе
		table.buyItem();
		Kitchen.Chair chair = new Kitchen.Chair();
		chair.buyItem();
	}

	public static String color = "white";
	public static class Table {
		public void buyItem() {
			System.out.println("now table is in the " + color + " kitchen");
		}
	}

	public static class Chair {
		public void buyItem() {
			System.out.println("now chair is in the " + color + " kitchen");
		}
	}
}
