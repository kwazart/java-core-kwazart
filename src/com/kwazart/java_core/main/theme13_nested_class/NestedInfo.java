package com.kwazart.java_core.main.theme13_nested_class;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.1
 * Created 28.09.2020 0:54
 */

public class NestedInfo implements InfoPrintable {
	public static final String THEME = "Вложенные классы";
	public void printInfo() {
		System.out.println("\nВложенный NESTED (статический класса)." +
				"\nОбычно являются публичными. Имеет невысокую меньшее отношение к классу в котороым он определен." +
				"\nНет доступа к свойствам объекта. Вложенные классы нужны для обращения к статическим полям и" +
				"\nи для обращения извне." +
				"\nПример:" +
				"\tpublic class Kitchen {\n" +
				"\t\tpublic static void main(String[] args) {\n" +
				"\t\t\tKitchen.Table table = new Table(); // если создаем объект в этом же классе\n" +
				"\t\t\ttable.buyItem();\n" +
				"\t\t\tKitchen.Chair chair = new Kitchen.Chair();\n" +
				"\t\t\tchair.buyItem();\n" +
				"\t\t}\n" +
				"\n" +
				"\t\tpublic static String color = \"white\";\n" +
				"\t\tpublic static class Table {\n" +
				"\t\t\tpublic void buyItem() {\n" +
				"\t\t\t\tSystem.out.println(\"now table is in the \" + color + \" kitchen\");\n" +
				"\t\t\t}\n" +
				"\t\t}\n" +
				"\n" +
				"\t\tpublic static class Chair {\n" +
				"\t\t\tpublic void buyItem() {\n" +
				"\t\t\t\tSystem.out.println(\"now chair is in the \" + color + \" kitchen\");\n" +
				"\t\t\t}\n" +
				"\t\t}\n" +
				"\t}" +
				// -----------------------------------
				"\n\nЕще пример:" +
				"\n\n\t" +
				"public class Test {\n" +
				"\t\tpublic static void main(String[] args){\n" +
				"\t\t\t// можно создавать объекты вложенного класса \n" +
				"\t\t\tOuterClass.NestedClass innerClass = new OuterClass.NestedClass();\n" +
				"\t\t\tinnerClass.main();\n" +
				"\t\t}\n" +
				"\t}\n" +
				"\n" +
				"\t/**\n" +
				"\t * Обрамляющий класс\n" +
				"\t */\n" +
				"\tclass OuterClass{\n" +
				"\t\tstatic int i;\n" +
				"\t\tint j;\n" +
				"\t\tfinal int k = 1;\n" +
				"\n" +
				"\t\tstatic void outherStatic(){\n" +
				"\t\t\ti++;\n" +
				"\t\t\t// c++;\n" +
				"\t\t\t// можно создавать объекты вложенного класса в статических методах\n" +
				"\t\t\tnew NestedClass();\n" +
				"\t\t}\n" +
				"\n" +
				"\t\tvoid outher() {\n" +
				"\t\t\ti++;\n" +
				"\t\t\t// можно создавать объекты вложенного класса в методах экземпляра\n" +
				"\t\t\tnew NestedClass();\n" +
				"\t\t}\n" +
				"\n" +
				"\t\t/**\n" +
				"\t\t * Вложенный класс\n" +
				"\t\t */\n" +
				"\t\tpublic static class NestedClass\n" +
				"\t\t\tint c;\n" +
				"\n" +
				"\t\t\tpublic void main() {\n" +
				"\t\t\t\ti++;\n" +
				"\t\t\t\t// j++;\n" +
				"\t\t\t\toutherStatic();\n" +
				"\t\t\t\tSystem.out.println(i); // можно обращаться к статическим полям обрамляющего класа\n" +
				"\t\t\t\t// System.out.println(k); // нельзя обращаться к нестатическим полям обрамляющего класа \n" +
				"\t\t\t}\n" +
				"\t\t}\n" +
				"\t}" +
				// ------------------------------
				"\n\nЕще пример:" +
				"\n\n\tpublic class Task03\n" +
				"\t{\n" +
				"\t\tpublic static void main(String[] args)\n" +
				"\t\t{\n" +
				"\t\t\tQuestion question = new Question(Question.Type.TEXT);\n" +
				"\t\t\t// Cнаружи обращение к классу Type всегда осуществляется через имя\n" +
				"\t\t\t// обрамляющего класса - Question.Type.\n" +
				"\t\t\tQuestion.Type type = question.getType();\n" +
				"\t\t\tif (type == Question.Type.TEXT)\n" +
				"\t\t\t{\n" +
				"\t\t\t\tSystem.out.println(\"Тип вопроса текст\");\n" +
				"\t\t\t}\n" +
				"\t\t}\n" +
				"\t}\n" +
				"\n" +
				"\tclass Question\n" +
				"\t{\n" +
				"\t\tprivate Type type;\n" +
				"\n" +
				"\t\tpublic Question(Type type)\n" +
				"\t\t{\n" +
				"\t\t\tthis.type = type;\n" +
				"\t\t}\n" +
				"\n" +
				"\t\tpublic Type getType()\n" +
				"\t\t{\n" +
				"\t\t\treturn type;\n" +
				"\t\t}\n" +
				"\n" +
				"\t\tpublic static enum Type\n" +
				"\t\t{\n" +
				"\t\t\tSINGLE_CHOICE, MULIT_CHOICE, TEXT\n" +
				"\t\t}\n" +
				"\t}");
	}

	public String getTHEME() {
		return THEME;
	}
}
