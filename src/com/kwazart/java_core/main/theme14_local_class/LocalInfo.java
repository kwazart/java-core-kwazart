package com.kwazart.java_core.main.theme14_local_class;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 28.09.2020 0:56
 */

public class LocalInfo implements InfoPrintable {
	public static final String THEME = "Локальные классы";

	public void printInfo() {
		System.out.println("\nКлассы в методе" +
				"\nИмеет доступ и к полям класа, и к переменным метода, в котором определен класс." +
				"\nПеременные метода должны быть константами (final). Альтернатива анонимному классу." +
				"\nПример:" +
				"\n\n\tpublic class MethodClass {\n" +
				"\t\tprivate int x;\n" +
				"\n" +
				"\t\tpublic void methodA() {\n" +
				"\t\t\tfinal int y = 100;\n" +
				"\n" +
				"\t\t\tclass TestClass{\n" +
				"\t\t\t\tint a;\n" +
				"\t\t\t\tpublic TestClass(int a) {\n" +
				"\t\t\t\t\tthis.a = a;\n" +
				"\t\t\t\t}\n" +
				"\n" +
				"\t\t\t\tpublic void printValues() {\n" +
				"\t\t\t\t\tSystem.out.println(x);\n" +
				"\t\t\t\t\tSystem.out.println(y);\n" +
				"\t\t\t\t}\n" +
				"\n" +
				"\t\t\t\t@Override\n" +
				"\t\t\t\tpublic String toString() {\n" +
				"\t\t\t\t\treturn \"TestClass{\" +\n" +
				"\t\t\t\t\t\t\t\"a=\" + a +\n" +
				"\t\t\t\t\t\t\t'}';\n" +
				"\t\t\t\t}\n" +
				"\t\t\t}\n" +
				"\t\t\t// y = 101; // если уберем коммент из переменной y,\n" +
				"\t\t\t// и уберем final, то произойдет ошибка компиляции\n" +
				"\t\t\t// вывод: класс в методе, может обрабатывать\n" +
				"\t\t\t// только константы метода, но любые поля объекта\n" +
				"\n" +
				"\t\t\tTestClass testClass = new TestClass(10);\n" +
				"\t\t\tmethodB(testClass);\n" +
				"\t\t}\n" +
				"\n" +
				"\t\tprivate void methodB(Object obj) {\n" +
				"\t\t\tSystem.out.println(obj);\n" +
				"\t\t}\n" +
				"\t}");
	}

	public String getTHEME() {
		return THEME;
	}
}
