package com.kwazart.java_core.main.theme15_anonymous_class;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 28.09.2020 0:59
 */

public class AnonymousInfo implements InfoPrintable {
	public static final String THEME = "Анонимные классы";

	public void printInfo() {
		System.out.println("\nАнонимные класса." +
				"\nСлужат для переопределения логики класса или интерфейса однократно. Т.е. создается новые объект" +
				"\nкласса без названия, который является наследником суперкласса, или реализует какой-либо интерфейс." +
				"\nПример:" +
				"\n\n\tpublic class Person {\n" +
				"\t\tpublic static void main(String[] args) {\n" +
				"\t\t\tAction action = new Action() {\n" +
				"\t\t\t\t@Override\n" +
				"\t\t\t\tpublic void run() {\n" +
				"\t\t\t\t\tSystem.out.println(\"speed - 60 km/h\");\n" +
				"\t\t\t\t}\n" +
				"\t\t\t};\n" +
				"\t\t\taction.run();\n" +
				"\n" +
				"\t\t\tHouse house = new House() {\n" +
				"\t\t\t\t@Override\n" +
				"\t\t\t\tpublic void build() {\n" +
				"\t\t\t\t\tSystem.out.println(\"high building\");\n" +
				"\t\t\t\t}\n" +
				"\t\t\t};\n" +
				"\t\t\thouse.build();\n" +
				"\t\t}\n" +
				"\t}" +
				"\n\nЕще пример:" +
				"\n\n\tpublic class Task03\n" +
				"\t{\n" +
				"\t\tpublic static void main(String[] args)\n" +
				"\t{\n" +
				"\t\t// создание объекта используя анонимный класс \n" +
				"\t\tStartable t2 = new Startable()\n" +
				"\t\t\t{\n" +
				"\t\t\t\t@Override\n" +
				"\t\t\t\tvoid test()\n" +
				"\t\t\t\t{\n" +
				"\t\t\t\t\tSystem.out.println(\"test\");\n" +
				"\t\t\t\t}\n" +
				"\t\t\t};\n" +
				"\t\t\tt2.test();\n" +
				"\t\t\tt2.start();\n" +
				"\t\t}\n" +
				"\t}\n" +
				"\n" +
				"\tabstract class Startable // implements Test\n" +
				"\t{\n" +
				"\t\tabstract void test();\n" +
				"\n" +
				"\t\tvoid start()\n" +
				"\t\t{\n" +
				"\t\t\tSystem.out.println(\"start\");\n" +
				"\t\t}\n" +
				"\t" +
				"\n\nЕще пример:" +
				"\n\n\tpublic class Test {\n" +
				"\t\tpublic static void main(String[] args) {\n" +
				"\t\t\tTestInt test = () -> {\n" +
				"\t\t\t\tSystem.out.println(\"test\");\n" +
				"\t\t\t};\n" +
				"\t\t\ttest.testMethod();\n" +
				"\t\t\ttest.testMethod2();\n" +
				"\t\t}\n" +
				"\t}\n" +
				"\n" +
				"\tinterface TestInt {\n" +
				"\t\tvoid testMethod();\n" +
				"\n" +
				"\t\tdefault void testMethod2() {\n" +
				"\t\t\tSystem.out.println(\"test from testMethod2()\");\n" +
				"\t\t}\n" +
				"\t}");
	}

	public String getTHEME() {
		return THEME;
	}
}
