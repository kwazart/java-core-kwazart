package com.kwazart.java_core.main.theme15_anonymous_class;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 23.09.2020 2:27
 */

interface Action {
	void run();
}

class House {
	public void build() {
		System.out.println("low building");
	}
}

public class Person {
	public static void main(String[] args) {
		Action action = new Action() {
			@Override
			public void run() {
				System.out.println("speed - 60 km/h");
			}
		};
		action.run();

		House house = new House() {
			@Override
			public void build() {
				System.out.println("high building");
			}
		};
		house.build();
	}
}

