package com.kwazart.java_core.main.theme16_date;

import com.kwazart.java_core.main.model.InfoPrintable;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 23.09.2020 10:53
 */

public class DateInfo implements InfoPrintable {
	public static final String THEME = "Date";

	public static void exampleMethod() throws ParseException {
		Date date = new Date(); // время в милисекундах с 1 января 1970
		System.out.println(date); // вывод текущей даты
		date = new Date(0);
		System.out.println(date); // учитывает часовой пояс

		// Календарь
		Calendar calendar = Calendar.getInstance(); // позволяет работать с датами более гибко
		calendar.setTime(new Date());
		System.out.println(calendar.getTime());
		calendar.add(Calendar.YEAR, 19); // позволяет модифиццировать даты
		System.out.println(calendar.getTime());

		// Для форматирования даты - выбираем из существующего
		DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.FULL);
		DateFormat dateFormat1 = DateFormat.getDateInstance(DateFormat.SHORT);
		System.out.println(dateFormat.format(new Date()));
		System.out.println(dateFormat1.format(new Date()));

		// Для форматированной даты - создаем сами формат
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY HH:mm:ss");
		System.out.println(sdf.format(new Date()));
		SimpleDateFormat formatForDateNow = new SimpleDateFormat("E yyyy.MM.dd 'и время' hh:mm:ss a zzz");
		System.out.println("Текущая дата " + formatForDateNow.format(new Date()));

		//Для сравнения объектов date
		date = new Date();
		Date date1 = new Date(43986579);
		System.out.println("after - " + (date.after(date1)));
		System.out.println("before - " + (date.before(date1)));
		Date date2 = (Date) date1.clone();
		System.out.println(date1);
		System.out.println(date2);
		System.out.println(date1.compareTo(date));
		System.out.println(date2.equals(date1));

		// Парсинг дат
		sdf = new SimpleDateFormat("dd//MM//yyyy");
		sdf = new SimpleDateFormat("dd//MM//yyyy HH:mm:ss");
		date = sdf.parse("27//05//1991 05:38:40");
		System.out.println(date);

		// Работа с календарем
		int year1 = calendar.get(Calendar.YEAR);
		int month1 = calendar.get(Calendar.MONDAY);
		System.out.println(year1 + " " + month1);
		calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		// забираем нужные нам текущие значения
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int hours = calendar.get(Calendar.HOUR_OF_DAY);
		System.out.println(year + " " + month + " " + hours); // текущие показатели
		// изменяем значения текущих значений (выбраны для примеры)
		calendar.add(Calendar.YEAR, -15);
		calendar.add(Calendar.MONTH, 8);
		calendar.add(Calendar.HOUR, 10);
		// забираем нужные нам измененные текущие значения
		year = calendar.get(Calendar.YEAR);
		month = calendar.get(Calendar.MONTH);
		hours = calendar.get(Calendar.HOUR_OF_DAY);
		System.out.println(year + " " + month + " " + hours); // измененные показатели

	}

	public void printInfo() {
		System.out.println("\nДАТА");
		System.out.println("\nКЛАСС DATE - базовый класс при работе с датами. Вычисляет количество милисекунд, которые" +
				"\nпрошли с 1 января 1970 года" +
				"\n\n\tDate date = new Date(); // текущая дата" +
				"\n\tdate = new Date();" +
				"\n\tDate date1 = new Date(43986579);" +
				"\n\tDate date1 = new Date(27356); // количество мс, прошедших с полуночи 1 января 1970 года" +
				"\n\nМетоды:" +
				"\n\tboolean after(Date date) - возвращает true, если вызывающий date раньше заданного, иначе false" +
				"\n\n\t\tSystem.out.println(\"after - \" + (date.after(date1))); // true" +
				"\n\n\tbooleam before(Date date) - return true, если вызывающий date позже заданного, иначе false" +
				"\n\n\t\tSystem.out.println(\"before - \" + (date.before(date1))); // false" +
				"\n\n\tObject clone() - дублирует вызывающий объект date" +
				"\n\n\t\tDate date2 = (Date) date1.clone();" +
				"\n\n\tint compareTo(Date date) - возвращает 0 если date1 = date2, возвращает -1 если date1 < date2, иначе 1" +
				"\n\n\t\tSystem.out.println(date1.compareTo(date)); // -1" +
				"\n\n\tboolean equals(Object date) - возвращает true если date1 = date2" +
				"\n\tlong getTime() - возвращает милисекунды прошедшие с 1.01.1970" +
				"\n\tint hashcode() - возвращает hashcode вызывающего объекта" +
				"\n\tvoid setTime(long time) - задает дату и время, в виде мс, которые прошли после 1.01.1970" +
				"\n\tString toString() - преобразует вызывающий объект к String и возвращает результат");

		System.out.println("\n\nCALENDAR" +
				"\nЭтот класс позволяет более гибко работать с датами. Модифицировать и возвращать отдельные значения." +
				"\nПример:" +
				"\n\n\tCalendar calendar = Calendar.getInstance();\n" +
				"\tcalendar.setTime(new Date());\n" +
				"\t// забираем нужные нам текущие значения\n" +
				"\tint year = calendar.get(Calendar.YEAR);\n" +
				"\tint month = calendar.get(Calendar.MONTH);\n" +
				"\tint hours = calendar.get(Calendar.HOUR_OF_DAY);\n" +
				"\tSystem.out.println(year + \" \" + month + \" \" + hours); // текущие показатели\n" +
				"\t// изменяем значения текущих значений (выбраны для примеры)\n" +
				"\tcalendar.add(Calendar.YEAR, -15);\n" +
				"\tcalendar.add(Calendar.MONTH, 8);\n" +
				"\tcalendar.add(Calendar.HOUR, 10);\n" +
				"\t// забираем нужные нам измененные текущие значения\n" +
				"\tyear = calendar.get(Calendar.YEAR);\n" +
				"\tmonth = calendar.get(Calendar.MONTH);\n" +
				"\thours = calendar.get(Calendar.HOUR_OF_DAY);\n" +
				"\tSystem.out.println(year + \" \" + month + \" \" + hours); // измененные показатели");

		System.out.println("\n\nSIMPLEDATEFORMAT" +
				"\n\nЭто конкретный класс для парсинга и форматирования даты в Java." +
				"\nSimpleDateFormat позволяет начать с выбора любых пользовательских шаблонов для форматирования" +
				"\nдаты и времени." +
				"\nПример:" +
				"\n\n\tSimpleDateFormat formatForDateNow = new SimpleDateFormat(\"E yyyy.MM.dd 'и время' hh:mm:ss a zzz\");" +
				"\n\tSystem.out.println(formatForDateNow.format(new Date()));" +
				"\n\nТ.е. пользователь самостоятельно создает нужныё ему шаблон даты." +
				"\nФормат-коды:" +
				"\n\n\tG - обозначение эры" +
				"\n\ty - год" +
				"\n\tM - номер месяца года" +
				"\n\td - число месяца" +
				"\n\th - формат часа от 1 до 12 (AM/PM)" +
				"\n\tH - формат часа от 0 до 23" +
				"\n\tm - минуты" +
				"\n\ts - секунды" +
				"\n\tS - милисекунды" +
				"\n\tE - день недели" +
				"\n\tD - номер дня в году" +
				"\n\tF - номер дня недели в месяце" +
				"\n\tw - номер едели в году" +
				"\n\tW - номер недели в месяце" +
				"\n\ta - маркер AP/PM" +
				"\n\tk - формат часа от 1 до 24" +
				"\n\tK - формат часа от 0 до 11 (AM/PM)" +
				"\n\tz - часовой пояс" +
				"\n\nМетод parse(String str) позволяет по паттерну из конструктора обнаружить дату и вернуть" +
				"\nобъект Date" +
				"\nПример:" +
				"\n\n\tSimpleDateFormat sdf = new SimpleDateFormat(\"dd//MM//yyyy HH:mm:ss\");" +
				"\n\tdate = sdf.parse(\"27//05//1991 05:38:40\");" +
				"\n\tSystem.out.println(date);" +
				"\n\nПри парсинге может вылететь исключение ParseException, поэтому нужно обрабатывать.");

		System.out.println("\n\nВы можете ознакомиться с примерами в данном коде - DateClass.exampleMethod()");
	}

	public String getTHEME() {
		return THEME;
	}
}
