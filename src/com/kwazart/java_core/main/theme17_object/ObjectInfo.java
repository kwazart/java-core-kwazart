package com.kwazart.java_core.main.theme17_object;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 27.09.2020 22:53
 */

public class ObjectInfo implements InfoPrintable {
	public static final String THEME = "Класс Object";

	public void printInfo() {
		System.out.println("\n\nOBJECT\nВ Java существует специальный класс, который является" +
				"\nродителем для всех классов, даже если это не указано явно. Этот класс" +
				"\nназывается Object. Т.е. все классы, которые создает разработчик," +
				"\nнаследуются от класса Object." +
				"\nЭто значит, что любой объект может использовать методы определенные" +
				"\nв классе Object." +
				"\nМЕТОДЫ класса Object:" +
				"\n\tprotected Object clone() - создает новый объект, не отличающийся от клонируемого" +
				"\n\tpublic boolean equals(Object obj) - определяет, равне ли один объект другому" +
				"\n\tprotected void finalize() - вызывается перед удалением неиспользуемого объекта" +
				"\n\tpublic final Class<?> getClass - получает класс объекта во время выполнения" +
				"\n\tpublic int hashCode() - возвращает хеш-код, связанный с вызывающим объектом" +
				"\n\tpublic final void notify() - возобновляет исполнение потока, ожидающего вызывающешл объекта" +
				"\n\tpublic final void notifyAll() - возобновляет исполнение всех потоков, ожидающих вызывающего объекта" +
				"\n\tpublic String toString() - возвращает символьную строку, описывающую объект" +
				"\n\tpublic final void wait() - ожидает другого потока исполнения" +
				"\n\tpublic final void wait(long timeout) - ожидает другого потока исполнения" +
				"\n\tpublic final void wait(long timeout, int nanos) - ожидает другого потока исполнения");

		System.out.println("\n\nМетод equals" +
				"\nВ Java сравнение объектов производится с помощью метода equals() класса Object. " +
				"\nЭтот метод сравнивает содержимое объектов и выводит значение типа boolean. " +
				"\nЗначение true - если содержимое эквивалентно, и false — если нет." +
				"\nОперация == не рекомендуется для сравнения объектов в Java. Дело в том, что при сравнение объектов," +
				"\nоперация == вернет true лишь в одном случае — когда ссылки указывают на один и тот же объект. " +
				"\nВ данном случае не учитывается содержимое переменных класса." +
				"\nПри создании пользовательского класса, принято переопределять метод equals() таким образом," +
				"\nчто бы учитывались переменные объекта.");

		System.out.println("\n\nМодфификатор native" +
				"\nПриложение на языке Java может вызывать методы, написанные на языке С++." +
				"\nТакие методы объявляются в языке Java с ключевым словом native, которое сообщает компилятору, " +
				"\nчто метод реализован в другом месте. Например:" +
				"\n\tpublic native int hashCode();" +
				"\nМетоды, помеченные native, можно переопределять обычными методами в подклассах.");

		System.out.println("\n\nКОНТРАКТЫ EQUALS" +
				"\nДля того чтобы метод equals сравнивал сожержимое объекта, а не адреса в памяти, " +
				"\nнеобходимо чтобы переопределенный метод equals выполнял 5 правил (контрактов):" +
				"\n\t1. Рефлексивность" +
				"\n\tДля каждого экземпляра х (причем x != null) должно выполняться условие:" +
				"\n\t\tassert x.equals(x) = true;" +
				"\n\t2. Симметричность" +
				"\n\tДля каждого экземпляра x и y x.equals(y) должнен возвращать true только тогда, когда" +
				"\n\ty.equals(x) возвращает true:" +
				"\n\t\t if (x.equals(y))" +
				"\n\t\t\tassert y.equals(x) == true;" +
				"\n\t3. Переносимость" +
				"\n\tДля каждого экземпляра x, y и z должно выполняться условие: если x.equals(y) возвращает" +
				"\n\ttrue и y.equals(z) возвращает true, тогда x.equals(z) должно возвращать true" +
				"\n\t\tif (x.equals(y) && y.equals(x))" +
				"\n\t\t\tassert x.equals(z) == true;" +
				"\n\t4. Консистентность (согласованность)" +
				"\n\tДля каждого экземпляра x и y повторное выполнение x.equals(y) должно возвращать" +
				"\n\tодинаковый результат:" +
				"\n\t\tif (x.equals(y))" +
				"\n\t\t\tassert x.equals(y) == true;" +
				"\n\t5. Сравнение с null" +
				"\n\tДля каждого экземпляра x x.equals(null) должно возвращать false:" +
				"\n\t\tassert x.equals(null) == false;");

		System.out.println("\n\nКОНТРАКТЫ HASHCODE" +
				"\nДля того чтобы не проводилась полная проверка на равенство через метод equals," +
				"\nпервоначально объекты проверяются на равенство hash значений (целые значения int)." +
				"\nПереопределение hashCode также сопровождается рядом правил (контрактов):" +
				"\n\t1. Консистентность" +
				"\n\tДля каждого экземпляра x повторное выполрение x.hashCode() должно возвращать одинаковый хэш:" +
				"\n\t\tint hash = x.hashCode();" +
				"\n\t\t\tassert hash == x.hashCode();" +
				"\n\t2. Схожие объекты имеют одинаковый хеш" +
				"\n\tЕсли два экземплара схожи друг с другом, то вызов hashCOde() у каждого из них должен возвращать" +
				"\n\tодинаковый результат:" +
				"\n\t\tif (x.equals(y))" +
				"\n\t\t\tassert x.hashCode() == y.hashCode(y);");

		System.out.println("\n\nСделаем вывод:" +
				"\nУ одинаковых объектов хеш одинаковый, однако если мы встретим два объекта у которых одинаковый" +
				"\nхеш, это не будет означать, что они равны. Необходима будет проверка через метод equals. " +
				"\nС другой стороны, если мы встретим два объекта, у которых будет разный хещ, можно со 100%-ой" +
				"\nуверенностью сказать, что они не равны.");

		System.out.println("\n\nПример:\n\n\t" +
				"public class TestClass {\n" +
				"\t\tpublic static void main(String[] args) {\n" +
				"\t\t\tPerson person1 = new Person(20, \"Ivan\");\n" +
				"\t\t\tPerson person2 = new Person(20, \"Ivan\");\n" +
				"\t\t\tSystem.out.println(person1 == person2);\n" +
				"\t\t\tSystem.out.println(person1.equals(person2));\n" +
				"\t\t}\n" +
				"\t}\n" +
				"\n" +
				"\tclass Person {\n" +
				"\t\tint age;\n" +
				"\t\tString name;\n" +
				"\n" +
				"\t\tpublic Person(int age, String name) {\n" +
				"\t\t\tthis.age = age;\n" +
				"\t\t\tthis.name = name;\n" +
				"\t\t}\n" +
				"\n" +
				"\t\t@Override\n" +
				"\t\tpublic boolean equals(Object o) {\n" +
				"\t\t\tif (this == o) {\n" +
				"\t\t\t\treturn true;\n" +
				"\t\t\t}\n" +
				"\t\t\tif (o == null || getClass() != o.getClass()) {\n" +
				"\t\t\t\treturn false;\n" +
				"\t\t\t}\n" +
				"\n" +
				"\t\t\tPerson person = (Person) o;\n" +
				"\n" +
				"\t\t\tif (age != person.age) {\n" +
				"\t\t\t\treturn false;\n" +
				"\t\t\t}\n" +
				"\t\t\treturn name.equals(person.name);\n" +
				"\t\t}\n" +
				"\n" +
				"\t\t@Override\n" +
				"\t\tpublic int hashCode() {\n" +
				"\t\t\tint result = age;\n" +
				"\t\t\tresult = 31 * result + name.hashCode();\n" +
				"\t\t\treturn result;\n" +
				"\t\t}\n" +
				"\t}");
	}

	public String getTHEME() {
		return THEME;
	}
}
