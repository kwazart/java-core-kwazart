package com.kwazart.java_core.main.theme17_object;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 27.09.2020 23:13
 */

public class TestClass {
	public static void main(String[] args) {
		Person person1 = new Person(20, "Ivan");
		Person person2 = new Person(20, "Ivan");
		System.out.println(person1 == person2);
		System.out.println(person1.equals(person2));
	}
}

class Person {
	int age;
	String name;

	public Person(int age, String name) {
		this.age = age;
		this.name = name;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Person person = (Person) o;

		if (age != person.age) {
			return false;
		}
		return name.equals(person.name);
	}

	@Override
	public int hashCode() {
		int result = age;
		result = 31 * result + name.hashCode();
		return result;
	}
}
