package com.kwazart.java_core.main.theme18_generics;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 27.09.2020 22:07
 */

public class GenericInfo implements InfoPrintable {
	public static final String THEME = "Generics (Обобщения)";

	public void printInfo() {
		System.out.println("\n\nGENERICS (ОБОБЩЕНИЯ)" +
				"\nОбобщения - подмножество обощенного программирования, содержаший подход к описанию данных" +
				"\nи алгоритмов, который позволяет их использовать с различными типами данных без изменения их описания." +
				"\nОсновано на синтаксисе C++" +
				"\n\nСоздадим несколько классов, которые будут использоваться в следующих примерах:" +
				"\n\t//Дыня, которая будет помещаться в коробку" +
				"\n\n\t" +
				"public class Melon {\n" +
				"\t\t@Override\n" +
				"\t\tpublic String toString()\n" +
				"\t\t{\n" +
				"\t\t\treturn \"дыня\";\n" +
				"\t\t}\n" +
				"\t}\n" +
				"\n" +
				"\n" +
				"\n\t// Простая коробка, демонстрирующая старый подход\n" +
				"\tpublic class SimpleBox {\n" +
				"\t\tprivate Object object;\n" +
				"\n" +
				"\t\tpublic void put(Object o) {\n" +
				"\t\t\tSystem.out.println(\"Открываем ящик\");\n" +
				"\t\t\tSystem.out.println(\"Предмет \" + o + \" помещен в ящик\");\n" +
				"\t\t\tthis.object = o;\n" +
				"\t\t\tSystem.out.println(\"Закрываем ящик\\n===================\");\n" +
				"\t\t}\n" +
				"\n" +
				"\t\tpublic Object get() {\n" +
				"\t\t\tSystem.out.println(\"Открываем ящик\");\n" +
				"\t\t\tif (object != null) {\n" +
				"\t\t\t\tSystem.out.println(\"Предмет \" + object + \" доступен\");\n" +
				"\t\t\t\tSystem.out.println(\"Забираем предмет и закрываем ящик\\n===================\");\n" +
				"\t\t\t} else {\n" +
				"\t\t\t\tSystem.out.println(\"Ящик пуст\");\n" +
				"\t\t\t}\n" +
				"\t\t\treturn object;\n" +
				"\t\t}\n" +
				"\t}" +
				"\n\n" +
				"\n\t//Тестовый класс, содердащий метод main" +
				"\n\tpublic class TestClass01 {\n" +
				"\t\tpublic static void main(String[] args) {\n" +
				"\t\t\tSimpleBox box = new SimpleBox();\n" +
				"\t\t\tMelon melon = new Melon();\n" +
				"\t\t\tbox.put(melon);\n" +
				"\n" +
				"\t\t\tMelon newMelon = (Melon) box.get();\n" +
				"\t\t\tSystem.out.println(\"Достали \" + newMelon + \"\\n\\n\");\n" +
				"\n" +
				"\t\t\t// вместо объекта типа Melon помешаем в ящик объект типа String\n" +
				"\t\t\t//box.put(\"Melon\");\n" +
				"\n" +
				"\t\t\t// здесь ошибка, так как в ящик поместили объкт типа String\n" +
				"\t\t\t//newMelon = (Melon) box.get();\n" +
				"\t\t\t//System.out.println(\"Достали \" + newMelon + \"\\n\\n\");\n" +
				"\t\t}\n" +
				"\t}" +
				"\n\nТ.е. до введения обощения мы не были застрахованы от ошибок приведения типов и работы" +
				"\nс неверными данными." +
				"\n\nПример с использованием generics. Создадим шаблонную \"коробку\"" +
				"\n\n\t" +
				"public class Box <T> { // шаблонный тип\n" +
				"\t\tprivate T object;\n" +
				"\n" +
				"\t\tpublic void put(T object) {\n" +
				"\t\t\tSystem.out.println(\"Открываем ящик\");\n" +
				"\t\t\tSystem.out.println(\"Предмет \" + object + \" помещен в ящик\");\n" +
				"\t\t\tthis.object = object;\n" +
				"\t\t\tSystem.out.println(\"Закрываем ящик\\n===================\");\n" +
				"\t\t}\n" +
				"\n" +
				"\t\tpublic T get() {\n" +
				"\t\t\tSystem.out.println(\"Открываем ящик\");\n" +
				"\t\t\tif (object != null) {\n" +
				"\t\t\t\tSystem.out.println(\"Предмет \" + object + \" доступен\");\n" +
				"\t\t\t\tSystem.out.println(\"Забираем предмет и закрываем ящик\\n===================\");\n" +
				"\t\t\t\tobject = null;\n" +
				"\t\t\t} else {\n" +
				"\t\t\t\tSystem.out.println(\"Ящик пуст\");\n" +
				"\t\t\t}\n" +
				"\t\t\treturn object;\n" +
				"\t\t}\n" +
				"\t}\n\n" +
				"" +
				"\nТеперь мы можем создать коробку и сразу же указать, что в ней может быть." +
				"\n\n\tpublic class TestClass02 {\n" +
				"\t\tpublic static void main(String[] args) {\n" +
				"\t\t\tBox<Melon> box1 = new Box<>();\n" +
				"\t\t\tMelon melon1 = new Melon();\n" +
				"\t\t\tbox1.put(melon1);\n" +
				"\t\t\t// приведение типа не требуется\n" +
				"\t\t\tMelon newMelon1 = box1.get();\n" +
				"\t\t\tSystem.out.println(\"Достали \" + newMelon1 + \"\\n\\n\");\n" +
				"\n" +
				"\t\t\t// теперь нельзя поместить в ящик объекты других типов\n" +
				"\t\t}\n" +
				"\t}" +
				"\n\nВ случае, если мы используем явное указание типа (в примере ниже), это зачастую может" +
				"\nоказать не столь гибким как хотелось бы." +
				"\n\n\tpublic class TestClass03 {\n" +
				"\t\tpublic static void main(String[] args) {\n" +
				"\t\t\tBox<Melon> boxMelon = new Box<>();\n" +
				"\t\t\tboxMelon.put(new Melon());\n" +
				"\t\t\topenBox(boxMelon);\n" +
				"\n" +
				"\t\t\tBox<Apple> boxApple = new Box<>();\n" +
				"\t\t\tboxApple.put(new Apple());\n" +
				"\t\t\t// openBox(boxApple);  - ошибка так как в методе явно задан ТИП (Melon)\n" +
				"\t\t}\n" +
				"\n" +
				"\t\tpublic static void openBox(Box<Melon> box)\n" +
				"\t\t{\n" +
				"\t\t\tbox.get();\n" +
				"\t\t}\n" +
				"\t}" +
				"\n\nДанная проблема решается через использование wildcard (маска типа):" +
				"\n\n\t//Wildcards. Переупаковка\n" +
				"\tpublic class TestClass04 {\n" +
				"\t\tpublic static void main(String[] args) {\n" +
				"\t\t\tBox<Melon> box = new Box<>();\n" +
				"\t\t\tbox.put(new Melon());\n" +
				"\t\t\topenBox(box);\n" +
				"\n" +
				"\t\t\tBox<Apple> boxApple = new Box<>();\n" +
				"\t\t\tboxApple.put(new Apple());\n" +
				"\t\t\topenBox(boxApple);\n" +
				"\n" +
				"\t\t\t// минус, что мы можем создать какую угодно коробку и передать ее в метод\n" +
				"\t\t\tBox<String> testBox = new Box<>();\n" +
				"\t\t\ttestBox.put(new String());\n" +
				"\t\t\topenBox(testBox);\n" +
				"\t\t}\n" +
				"\n" +
				"\t\tprivate static void openBox(Box<?> box) { // проблему можем исправить добавив extend Fruit\n" +
				"\t\t\t// т.е. принимаются типы наследники  класса Fruit\n" +
				"\t\t\tSystem.out.println(box.get());\n" +
				"\t\t}\n" +
				"\n" +
				"\t\t// исправленный метод\n" +
				"\t\tprivate static void newOpenBox(Box<? extends Fruit> box) {\n" +
				"\t\t\tSystem.out.println(box.get());\n" +
				"\t\t}\n" +
				"\n" +
				"\t\t// еще вариант\n" +
				"\t\t// принимаются классы только Apple или его суперклассы\n" +
				"\t\tprivate static void newSuperOpenBox(Box<? super Apple> box) {\n" +
				"\t\t\tSystem.out.println(box.get());\n" +
				"\t\t}\n" +
				"\t}" +
				"\n\nДженерики в массивах:" +
				"\n\n\t" +
				"public class TestClass05 {\n" +
				"\t\tpublic static void main(String[] args) {\n" +
				"\t\t\t// нельзя создать массив дженериков\n" +
				"\t\t\t// Box<Apple>[] box = new Box<Apple>[10];\n" +
				"\t\t\t//Box<Apple>[] boxes = new Box[10];\n" +
				"\n" +
				"\t\t\t// но можно создать массив ящиков неопределенных типов\n" +
				"\t\t\tBox<?>[] box = new Box<?>[10];\n" +
				"\n" +
				"\t\t\t// в такой массив можно положить различные ящики\n" +
				"\t\t\tbox[0] = new Box<Apple>();\n" +
				"\t\t\tbox[1] = new Box<Melon>();\n" +
				"\n" +
				"\t\t\t// такое приведение типова корректно Box<?> является подтипом Object\n" +
				"\t\t\tObject[] objectBox = box;\n" +
				"\n" +
				"\t\t\tBox<Apple> appleBox = new Box<Apple>();\n" +
				"\t\t\tappleBox.put(new Apple());\n" +
				"\t\t\tbox[0] = appleBox;\n" +
				"\t\t}\n" +
				"\t}" +
				"\n\nШаблонные методы:\n\n" +
				"\tpublic class TestClass06 {\n" +
				"\t\tpublic static void main(String[] args) {\n" +
				"\t\t\tprint(new Integer(35));\n" +
				"\t\t\tprint(new Boolean(\"true\"));\n" +
				"\t\t}\n" +
				"\n" +
				"\t\t// шаблонный метод\n" +
				"\t\tpublic static <T> void print(T number) {\n" +
				"\t\t\tSystem.out.println(number);\n" +
				"\t\t}\n" +
				"\t\t\n" +
				"\t\tpublic static <T1, T2> void method(T1 t1, T2 t2) {\n" +
				"\t\t\t// доступны только стандартные методы класса Object\n" +
				"\t\t}\n" +
				"\t}" +
				"" +
				"\n\nДобавим несколько классов для следующих примеров:" +
				"\n\n\tpublic class Fruit\n" +
				"\t{\n" +
				"\t\t@Override\n" +
				"\t\tpublic String toString()\n" +
				"\t\t{\n" +
				"\t\t\treturn \"фрукт\";\n" +
				"\t\t}\n" +
				"\t}" +
				"\n\n" +
				"\tpublic class Apple extends Fruit\n" +
				"\t{\n" +
				"\t\t@Override\n" +
				"\t\tpublic String toString()\n" +
				"\t\t{\n" +
				"\t\t\treturn \"яблоко\";\n" +
				"\t\t}\n" +
				"\t}\n" +
				"" +
				"\n" +
				"\tpublic class RottenApple extends Apple {\n" +
				"\t\t@Override\n" +
				"\t\tpublic String toString()\n" +
				"\t\t{\n" +
				"\t\t\treturn \"гнилое яблоко\";\n" +
				"\t\t}\n" +
				"\t}" +
				"" +
				"\n\nШаблонный класс:" +
				"\n\n\t" +
				"public class TestClass07 {\n" +
				"\t\t// шаблонный класс\n" +
				"\t\tstatic class Box<T> {\n" +
				"\t\t\tprivate T t;\n" +
				"\t\t\tpublic void set(T t) {this.t = t; }\n" +
				"\t\t\tpublic T get() { return t; }\n" +
				"\t\t}\n" +
				"\n" +
				"\t\tpublic static void main(String[] args) {\n" +
				"\t\t\tBox<Apple> boxApple = new Box<>();\n" +
				"\t\t\tBox<RottenApple> boxRottenApple = new Box<>();\n" +
				"\t\t\tboxRottenApple.set(new RottenApple());\n" +
				"\t\t\tmerge(boxApple, boxRottenApple);\n" +
				"\t\t\tSystem.out.println(boxApple.get());\n" +
				"\t\t}\n" +
				"\n" +
				"\t\tpublic static <M, N extends M> void merge(Box<M> box1, Box<N> box2) {\n" +
				"\t\t\tN n = box2.get();\n" +
				"\t\t\tbox1.set(n);\n" +
				"\t\t}\n" +
				"\t}" +
				"" +
				"\n\nДженерики в List:" +
				"\n\n\t" +
				"public class TestClass08 {\n" +
				"\tpublic static void main(String[] args) {\n" +
				"\t\tList languages = new ArrayList();\n" +
				"\t\tlanguages.add(\"Java\");\n" +
				"\t\tlanguages.add(\"C++\");\n" +
				"\t\tlanguages.add(\"Python\");\n" +
				"\t\tlanguages.add(\"PHP\");\n" +
				"\t\tlanguages.add(new Object());\n" +
				"\n" +
				"\t\tString language = (String) languages.get(2);\n" +
				"\t\tSystem.out.println(language);\n" +
				"\t\tSystem.out.println(languages.get(4));\n" +
				"\n" +
				"\t\tList<String> languagesList = new ArrayList<>();\n" +
				"\t\tlanguagesList.add(\"Java\");\n" +
				"\t\tlanguagesList.add(\"C++\");\n" +
				"\t\tlanguagesList.add(\"Python\");\n" +
				"\t\tlanguagesList.add(\"PHP\");\n" +
				"\t\t// languagesList.add(new Object()); ошибка\n" +
				"\n" +
				"\t\tlanguage = languagesList.get(0);\n" +
				"\t\tSystem.out.println(language);\n" +
				"\t}\n" +
				"}");
	}

	public String getTHEME() {
		return THEME;
	}
}




