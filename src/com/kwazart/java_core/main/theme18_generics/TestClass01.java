package com.kwazart.java_core.main.theme18_generics;

import com.kwazart.java_core.main.theme18_generics.model.Melon;
import com.kwazart.java_core.main.theme18_generics.model.SimpleBox;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 27.09.2020 19:52
 */

public class TestClass01 {
	public static void main(String[] args) {
		SimpleBox box = new SimpleBox();
		Melon melon = new Melon();
		box.put(melon);

		Melon newMelon = (Melon) box.get();
		System.out.println("Достали " + newMelon + "\n\n");

		// вместо объекта типа Melon помешаем в ящик объект типа String
		//box.put("Melon");

		// здесь ошибка, так как в ящик поместили объкт типа String
		//newMelon = (Melon) box.get();
		//System.out.println("Достали " + newMelon + "\n\n");
	}
}
