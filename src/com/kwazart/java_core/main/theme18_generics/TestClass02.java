package com.kwazart.java_core.main.theme18_generics;

import com.kwazart.java_core.main.theme18_generics.model.Box;
import com.kwazart.java_core.main.theme18_generics.model.Melon;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 27.09.2020 20:09
 */

public class TestClass02 {
	public static void main(String[] args) {
		Box<Melon> box1 = new Box<>();
		Melon melon1 = new Melon();
		box1.put(melon1);
		// приведение типа не требуется
		Melon newMelon1 = box1.get();
		System.out.println("Достали " + newMelon1 + "\n\n");

		// теперь нельзя поместить в ящик объекты других типов
	}
}
