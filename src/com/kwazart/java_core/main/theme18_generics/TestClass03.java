package com.kwazart.java_core.main.theme18_generics;

import com.kwazart.java_core.main.theme18_generics.model.Apple;
import com.kwazart.java_core.main.theme18_generics.model.Box;
import com.kwazart.java_core.main.theme18_generics.model.Melon;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 27.09.2020 20:09
 */

public class TestClass03 {
	public static void main(String[] args) {
		Box<Melon> boxMelon = new Box<>();
		boxMelon.put(new Melon());
		openBox(boxMelon);

		Box<Apple> boxApple = new Box<>();
		boxApple.put(new Apple());
		// openBox(boxApple);  - ошибка так как в методе явно задан ТИП (Melon)
	}

	public static void openBox(Box<Melon> box)
	{
		box.get();
	}
}
