package com.kwazart.java_core.main.theme18_generics;

import com.kwazart.java_core.main.theme18_generics.model.Apple;
import com.kwazart.java_core.main.theme18_generics.model.Box;
import com.kwazart.java_core.main.theme18_generics.model.Fruit;
import com.kwazart.java_core.main.theme18_generics.model.Melon;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 27.09.2020 20:14
 */

//Wildcards. Переупаковка
public class TestClass04 {
	public static void main(String[] args) {
		Box<Melon> box = new Box<>();
		box.put(new Melon());
		openBox(box);

		Box<Apple> boxApple = new Box<>();
		boxApple.put(new Apple());
		openBox(boxApple);

		// минус, что мы можем создать какую угодно коробку и передать ее в метод
		Box<String> testBox = new Box<>();
		testBox.put(new String());
		openBox(testBox);
	}

	private static void openBox(Box<?> box) { // проблему можем исправить добавив extend Fruit
		// т.е. принимаются типы наследники  класса Fruit
		System.out.println(box.get());
	}

	// исправленный метод
	private static void newOpenBox(Box<? extends Fruit> box) {
		System.out.println(box.get());
	}

	// еще вариант
	// принимаются классы только Apple или его суперклассы
	private static void newSuperOpenBox(Box<? super Apple> box) {
		System.out.println(box.get());
	}
}
