package com.kwazart.java_core.main.theme18_generics;

import com.kwazart.java_core.main.theme18_generics.model.Apple;
import com.kwazart.java_core.main.theme18_generics.model.Box;
import com.kwazart.java_core.main.theme18_generics.model.Melon;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 27.09.2020 20:49
 */

public class TestClass05 {
	public static void main(String[] args) {
		// нельзя создать массив дженериков
		// Box<Apple>[] box = new Box<Apple>[10];
		//Box<Apple>[] boxes = new Box[10];

		// но можно создать массив ящиков неопределенных типов
		Box<?>[] box = new Box<?>[10];

		// в такой массив можно положить различные ящики
		box[0] = new Box<Apple>();
		box[1] = new Box<Melon>();

		// такое приведение типова корректно Box<?> является подтипом Object
		Object[] objectBox = box;

		Box<Apple> appleBox = new Box<Apple>();
		appleBox.put(new Apple());
		box[0] = appleBox;
	}
}
