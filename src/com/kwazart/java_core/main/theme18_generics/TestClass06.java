package com.kwazart.java_core.main.theme18_generics;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 27.09.2020 21:02
 */

public class TestClass06 {
	public static void main(String[] args) {
		print(new Integer(35));
		print(new Boolean("true"));
	}

	// шаблонный метод
	public static <T> void print(T number) {
		System.out.println(number);
	}

	public static <T1, T2> void method(T1 t1, T2 t2) {
		// доступны только стандартные методы класса Object
	}
}
