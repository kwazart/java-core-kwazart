package com.kwazart.java_core.main.theme18_generics;

import com.kwazart.java_core.main.theme18_generics.model.Apple;
import com.kwazart.java_core.main.theme18_generics.model.RottenApple;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 27.09.2020 21:18
 */

public class TestClass07 {
	// шаблонный класс
	static class Box<T> {
		private T t;
		public void set(T t) {this.t = t; }
		public T get() { return t; }
	}

	public static void main(String[] args) {
		Box<Apple> boxApple = new Box<>();
		Box<RottenApple> boxRottenApple = new Box<>();
		boxRottenApple.set(new RottenApple());
		merge(boxApple, boxRottenApple);
		System.out.println(boxApple.get());
	}

	public static <M, N extends M> void merge(Box<M> box1, Box<N> box2) {
		N n = box2.get();
		box1.set(n);
	}
}
