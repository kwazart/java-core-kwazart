package com.kwazart.java_core.main.theme18_generics;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 27.09.2020 22:28
 */

public class TestClass08 {
	public static void main(String[] args) {
		List languages = new ArrayList();
		languages.add("Java");
		languages.add("C++");
		languages.add("Python");
		languages.add("PHP");
		languages.add(new Object());

		String language = (String) languages.get(2);
		System.out.println(language);
		System.out.println(languages.get(4));

		List<String> languagesList = new ArrayList<>();
		languagesList.add("Java");
		languagesList.add("C++");
		languagesList.add("Python");
		languagesList.add("PHP");
		// languagesList.add(new Object()); ошибка

		language = languagesList.get(0);
		System.out.println(language);
	}
}
