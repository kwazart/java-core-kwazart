package com.kwazart.java_core.main.theme18_generics.model;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 27.09.2020 20:08
 */

public class Apple extends Fruit
{
	@Override
	public String toString()
	{
		return "яблоко";
	}
}
