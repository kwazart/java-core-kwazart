package com.kwazart.java_core.main.theme18_generics.model;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 27.09.2020 19:59
 */
//	2
public class Box <T> { // шаблонный тип
	private T object;

	public void put(T object) {
		System.out.println("Открываем ящик");
		System.out.println("Предмет " + object + " помещен в ящик");
		this.object = object;
		System.out.println("Закрываем ящик\n===================");
	}

	public T get() {
		System.out.println("Открываем ящик");
		if (object != null) {
			System.out.println("Предмет " + object + " доступен");
			System.out.println("Забираем предмет и закрываем ящик\n===================");
			object = null;
		} else {
			System.out.println("Ящик пуст");
		}
		return object;
	}
}
