package com.kwazart.java_core.main.theme18_generics.model;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 27.09.2020 21:18
 */

public class RottenApple extends Apple {
	@Override
	public String toString()
	{
		return "гнилое яблоко";
	}
}