package com.kwazart.java_core.main.theme18_generics.model;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 27.09.2020 19:46
 */
//	1
public class SimpleBox {
	private Object object;

	public void put(Object o) {
		System.out.println("Открываем ящик");
		System.out.println("Предмет " + o + " помещен в ящик");
		this.object = o;
		System.out.println("Закрываем ящик\n===================");
	}

	public Object get() {
		System.out.println("Открываем ящик");
		if (object != null) {
			System.out.println("Предмет " + object + " доступен");
			System.out.println("Забираем предмет и закрываем ящик\n===================");
		} else {
			System.out.println("Ящик пуст");
		}
		return object;
	}
}

