package com.kwazart.java_core.main.theme20_exception;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 28.09.2020 13:04
 */

public class ExceptionInfo implements InfoPrintable {
	public static final String THEME = "Исключения";

	public void printInfo() {
		System.out.println("\n\nEXCEPTION (ИСКЛЮЧЕНИЯ)" +
				"\nИсключение – это ошибка, которая возникает во время выполнения приложения.\n" +
				"\nВ языке Java исключения являются объектами, которые являются наследниками Throwable. Существует\n" +
				"ряд стандартных исключительных ситуаций, которые предопределены. В данном случае объекты исключений\n" +
				"создаются автоматически, при возникновении исключительной ситуации. Также мы можем создавать\n" +
				"собственные исключения, если наследуем базовый классException или RuntimeException (исключим ошибки Error).\n" +
				"\nИсключения могут возникать во многих случаях, например: передача неправильного аргумента, обращение\n" +
				"к объекту, который равен null, выход за пределы массива, ошибка при приведении типов (неверный формат) и т.д.\n" +
				"\nОбработка исключительных ситуаций (exception handling) – это механизм языков программирования,\n" +
				"предназначенный для описания реакции программы на ошибки времени выполнения и другие возможные\n" +
				"проблемы (называемые исключениями), которые могут возникнуть при выполнении программы и приводят к\n" +
				"невозможности дальнейшей отработки программой ее базового алгоритма.");

		System.out.println("\n\nИерархия исключений:" +
				"\n\n" +
				"\nThrowable (checked)" +
				"\n----- Error (unchecked)" +
				"\n---------- ThreadDeath" +
				"\n---------- VirtualMachineError" +
				"\n--------------- UnknownError" +
				"\n--------------- StackOverFlowError" +
				"\n--------------- OutOfMemoryError" +
				"\n----- Exception (checked)" +
				"\n---------- RuntimeException (unchecked)" +
				"\n--------------- NoSuchElementException" +
				"\n--------------- IndexOutOfBoundsException" +
				"\n--------------- NullPointerException" +
				"\n--------------- ArithmeticException" +
				"\n--------------- IllegalArgumentException" +
				"\n--------------- ClassCastException" +
				"\n---------- ReflectiveOperationException" +
				"\n--------------- NoSuchFieldException" +
				"\n--------------- ClassNotFoundException" +
				"\n--------------- IllegalAccessException" +
				"\n--------------- NoSuchMethodException" +
				"\n---------- SQLException" +
				"\n---------- IOException" +
				"\n--------------- FileNotFoundException" +
				"\n--------------- EOFException");

		System.out.println("\n\nВ языке Java присутствуют два типа исключений:\n" +
				"checked и unchecked, а также – ошибки (Error), которые следует рассматривать отдельно.\n" +
				"Checked исключения обязательно должны обрабатываться блоком catch или описываться в сигнатуре метода\n" +
				"(рассмотрим дальше), в отличие от unchecked, которые обрабатывать не обязательно, их можно избежать\n" +
				"проверками. Сhecked исключения являются наследниками класса Exception (исключая ветку RuntimeException).\n" +
				"Unchecked – это необязательные для обработки исключения, которые наследованные от RuntimeException.\n" +
				"Примеры исключений:\n" +
				"(unchecked) NullPointerException,\n" +
				"(checked) IOException.\n" +
				"NullPointerException возникает при обращении к неинициализированной переменной.\n" +
				"Это исключениеможно предупредить, сделав проверку на null, приобращении к свойствам этого объекта. " +
				"Исключение IOException возникает, например при открытии файла. Даже если файл существует, может\n" +
				"произойти ряд причин, которые нельзя предугадать, поэтому это исключение обязательно необходимо обработать.\n" +
				"Если во время работы приложения возникли ошибки (Error), это означает наличие серьезных проблем\n" +
				"Большинство из этих ошибок влечет за собой ненормальный ход выполнения программы. Ошибки не рекомендуется\n" +
				"отмечать методах посредством throws-объявления, поэтому они также очень часто называются\n" +
				"не проверяемые (unchecked).");

		System.out.println("\n\n TRY CATCH" +
				"\n\nКлючевое слово try используется для задания блока программного кода, который может спровоцировать\n" +
				"исключительную ситуацию. Сразу после блока try должен располагаться блок catch, задающий тип исключения,\n" +
				"которого необходимо обрабатывать.\n" +
				"Синтаксис:\n\n" +
				"\ttry{\n" +
				"\t\t<блок кода, с возможной исключительной\n" +
				"\t\tситуацией>\n" +
				"\t}\n" +
				"\tcatch (<тип и объект исключения, которое мы отлавливаем>) {\n" +
				"\t\t<блок кода действия, при возникновенииисключения>\n" +
				"\t}" +
				"\n\nСatch можно разделить на несколько блоков для того, чтобы на каждое отдельное исключение, которые может\n" +
				"произойти в блоке try, выполнять разные действия.");

		System.out.println("\n\nПример исключительных ситуаций:\n" +
				"\tpublic class Main {\n" +
				"\t\tstatic Object object;\n" +
				"\t\tpublic static void foo() {\n" +
				"\t\t\tSystem.out.println(object.toString());\n" +
				"\t\t\tSystem.out.println(1 / 0);\n" +
				"\t\t}\n" +
				"\t\tpublic static void main(String[] args) {\n" +
				"\t\t\tfoo();\n" +
				"\t\t}\n" +
				"\t}\n\n" +
				"В методе foo() написан код, который вызывает два исключения:\n" +
				" - NullPointerException – обращение к неинициализированному объекту;\n" +
				" - ArithmeticException: / by zero – деление на ноль.\n" +
				"Обработаем исключения:\n\n" +
				"\tpublic static void foo() {\n" +
				"\t\ttry {\n" +
				"\t\t\tSystem.out.println(object.toString());\n" +
				"\t\t\tSystem.out.println(1 / 0);\n" +
				"\t\t} catch (Exception e) {\n" +
				"\t\t\tSystem.out.println(\"Exception\");\n" +
				"\t\t}\n" +
				"\t}" +
				"\n\n" +
				"Класс Exception является родителем для NullPointerException и ArithmeticException,\n" +
				"поэтому независимо от того, где возникнет ошибка, мы попадем в блок catch. Следует обратить внимание,\n" +
				"что если мы получим исключение в первой строке блока try, то дальше мы не пойдем, поэтому мы отловим\n" +
				"только одно исключение. Не забываем, что перехватываем исключения в блоке catch в порядке от младешго класса" +
				"\nк старшему.\n\n" +
				"\tpublic static void foo() {\n" +
				"\t\ttry {\n" +
				"\t\t\tSystem.out.println(object.toString());\n" +
				"\t\t\tSystem.out.println(1 / 0);\n" +
				"\t\t} catch (NullPointerException e) {\n" +
				"\t\t\tSystem.out.println(\"Объект равен null\");\n" +
				"\t\t} catch (ArithmeticException e) {\n" +
				"\t\t\tSystem.out.println(\"На ноль делить нельзя\");\n" +
				"\t\t}\n" +
				"\t}\n\n" +
				"Записав блок catch в таком виде, мы можем определить различные действия, при том или ином исключении,\n" +
				"но опять же, мы сможем обработать только одно исключение; если мы хотим однозначно обработать обе\n" +
				"строки, тогда их следует вынести в разные блоки try:\n\n" +
				"\tpublic static void foo() {\n" +
				"\t\ttry {\n" +
				"\t\t\tSystem.out.println(object.toString());\n" +
				"\t\t} catch (NullPointerException e) {\n" +
				"\t\t\tSystem.out.println(\"Объект равен null\");\n" +
				"\t\t}\n" +
				"\t\ttry {\n" +
				"\t\t\tSystem.out.println(1 / 0);\n" +
				"\t\t}catch (ArithmeticException e) {\n" +
				"\t\t\tSystem.out.println(\"На ноль делить нельзя\");\n" +
				"\t\t}\n" +
				"\t}");

		System.out.println("\n\nЛовим сразу 2 типа исключений:" +
				"\n\n" +
				"\tpublic class TestExc01 {\n" +
				"\t\tpublic static void main(String[] args) {\n" +
				"\t\t\tScanner sc = new Scanner(System.in);\n" +
				"\t\t\tint[] m = new int[1];\n" +
				"\t\t\ttry {\n" +
				"\t\t\t\tSystem.out.println(\"Введите 0 или 1\");\n" +
				"\t\t\t\tint a = sc.nextInt();\n" +
				"\n" +
				"\t\t\t\t// если значение a==0 возникает исключительная ситуация\n" +
				"\t\t\t\tm[a] = 4 / a;\n" +
				"\t\t\t\t// если значение a > 1 или a < 0 возникает исключительная ситуация\n" +
				"\t\t\t\tSystem.out.println(m[a]);\n" +
				"\t\t\t} catch (ArithmeticException e) {\n" +
				"\t\t\t\tSystem.out.println(\"Произошла недопустимая арифметическая операция\");\n" +
				"\t\t\t} catch (ArrayIndexOutOfBoundsException e) {\n" +
				"\t\t\t\tSystem.out.println(\"Обращение по недопустимому индексу массива\");\n" +
				"\t\t\t}\n" +
				"\t\t\tsc.close();\n" +
				"\t\t}\n" +
				"\t}");

		System.out.println("\n\nSYSTEM.ERR - выводит сообщение, не буферизируя его" +
				"\nПример:" +
				"\n\n\t" +
				"\tpublic class TestExc06 {\n" +
				"\t\tpublic static void main(String[] args)\n" +
				"\t\t\t{\n" +
				"\t\t\t//FIXME Запустите пример несколько раз\n" +
				"\t\t\ttry\n" +
				"\t\t\t{\n" +
				"\t\t\t\tint a = 1;\n" +
				"\t\t\t\t\tSystem.out.println(\"NO Error\");\n" +
				"\t\t\t\tSystem.err.println(\"Error\"); // опережает вывод через System.out, т.к. отсутствует буферизация\n" +
				"\t\t\t\tSystem.out.println(a / 0);\n" +
				"\t\t\t}\n" +
				"\t\t\tcatch (ArithmeticException e)\n" +
				"\t\t\t{\n" +
				"\t\t\t\tSystem.out.println(\"Произошла недопустимая арифметическая операция\");\n" +
				"\t\t\t}\n" +
				"\t\t}\n" +
				"\t}");

		System.out.println("\n\nЕще пример:\n\n" +
				"\t/**\n" +
				"\t *\n" +
				"\t * Когда поймали одно исключение, в блоке catch можем бросить другое, которое не\n" +
				"\t * будет поймано блоком try\n" +
				"\t *\n" +
				"\t */\n" +
				"\tpublic class TestExc07 {\n" +
				"\t\tpublic static void main(String[] args) {\n" +
				"\t\t\ttry {\n" +
				"\t\t\t\tthrow new NullPointerException();\n" +
				"\t\t\t\t// System.out.println(\"опа\");\n" +
				"\t\t\t} catch (NullPointerException npe) {\n" +
				"\t\t\t\tSystem.err.println(1);\n" +
				"\t\t\t\tthrow new ArithmeticException();\n" +
				"\t\t\t} catch (ArithmeticException ae) {\n" +
				"\t\t\t\tSystem.err.println(2);\n" +
				"\t\t\t}\n" +
				"\t\t\tSystem.err.println(3);\n" +
				"\t\t}\n" +
				"\t}");

		System.out.println("\n\nЕще пример:\n\n" +
				"\t/**\n" +
				"\t *\n" +
				"\t * Пойманое исключени можно бросить еще раз.\n" +
				"\t *\n" +
				"\t */\n" +
				"\tpublic class TestExc08\n" +
				"\t{\n" +
				"\t\tpublic static void main(String[] args)\n" +
				"\t\t{\n" +
				"\t\t\ttry\n" +
				"\t\t\t{\n" +
				"\t\t\t\tString test = null;\n" +
				"\t\t\t\ttest = test.trim();\n" +
				"\t\t\t}\n" +
				"\t\t\tcatch (NullPointerException npe)\n" +
				"\t\t\t{\n" +
				"\t\t\t\tSystem.err.println(\"Поймали NPE\");\n" +
				"\t\t\t\tthrow npe; // и снова бросаем\n" +
				"\t\t\t}\n" +
				"\t\t\tcatch (ArithmeticException ae) {\n" +
				"\t\t\t\tSystem.err.println(2);\n" +
				"\t\t\t}\n" +
				"\t\t\tSystem.err.println(3);\n" +
				"\t\t}\n" +
				"\t}");

		System.out.println("\n\nВложенные try catch. Пример:\n\n\t" +
				"public class TestExc09 {\n" +
				"\t\tpublic static void main(String[] args) {\n" +
				"\t\t\ttry {\n" +
				"\t\t\t\tSystem.err.print(\" 0\");\n" +
				"\t\t\t\t\tif (true) {throw new RuntimeException();}\n" +
				"\t\t\t\tSystem.err.print(\" 1\");\n" +
				"\t\t\t} catch (RuntimeException e) { // перехватили RuntimeException\n" +
				"\t\t\t\tSystem.err.print(\" 2.1\");\n" +
				"\t\t\t\ttry {\n" +
				"\t\t\t\t\tSystem.err.print(\" 2.2\");\n" +
				"\t\t\t\t\tif (true) {throw new Error();} // и бросили новый Error\n" +
				"\t\t\t\t\tSystem.err.print(\" 2.3\");\n" +
				"\t\t\t\t} catch (Throwable t) {            // перехватили Error\n" +
				"\t\t\t\t\tSystem.err.print(\" 2.4\");\n" +
				"\t\t\t\t}\n" +
				"\t\t\t\tSystem.err.print(\" 2.5\");\n" +
				"\t\t\t} catch (Error e) { // хотя есть catch по Error \"ниже\", но мы в него не попадаем\n" +
				"\t\t\t\tSystem.err.print(\" 3\");\n" +
				"\t\t\t}\n" +
				"\t\t\tSystem.err.println(\" 4\");\n" +
				"\t\t}\n" +
				"\t}" +
				"\n\nВывод: 0 2.1 2.2 2.4 2.5 4");

		System.out.println("\n\nЕще пример:\n\n\t" +
				"/**\n" +
				"\t *\n" +
				"\t * Будет поймано, то что брошено, а не то что формально объявлено\n" +
				"\t *\n" +
				"\t */\n" +
				"\tpublic class TestExc10 {\n" +
				"\t\tpublic static void main(String[] args) {\n" +
				"\t\t\ttry {\n" +
				"\t\t\t\t// ссылка типа Throwable указывает на объект типа Exception\n" +
				"\t\t\t\tThrowable t = new Exception();\n" +
				"\t\t\t\tthrow t;\n" +
				"\t\t\t} catch (RuntimeException e) {\n" +
				"\t\t\t\tSystem.err.println(\"catch RuntimeException\");\n" +
				"\t\t\t} catch (Exception e) {\n" +
				"\t\t\t\tSystem.err.println(\"catch Exception\");\n" +
				"\t\t\t} catch (Throwable e) {\n" +
				"\t\t\t\tSystem.err.println(\"catch Throwable\");\n" +
				"\t\t\t}\n" +
				"\t\t}\n" +
				"\t}");

		System.out.println("\n\nКлючевое слово FINALLY\n" +
				"Для объявления участка кода, который будет гарантированно выполняться, независимо от того, какие\n" +
				"исключения были возбуждены и перехвачены, необходимо использовать блок finally.\n" +
				"Этот блок отработает при успешном выполнении блока try или при обработке исключения; в обоих случаях\n" +
				"блок finally будет выполнен после try-catch и до того, как управление перейдет к операторам, следующим\n" +
				"после try. Блок finally очень удобен для закрытия файлов и освобождения любых ресурсов, захваченных для\n" +
				"временного использования в начале выполнения метода.");

		System.out.println("\n\nFinally секция:" +
				"\n\n\tpublic class TestExc02 {\n" +
				"\t\tpublic static void main(String[] args) {\n" +
				"\t\t\tScanner sc = new Scanner(System.in);\n" +
				"\t\t\ttry {\n" +
				"\t\t\t\tSystem.out.println(\"Введите 0 или 1\");\n" +
				"\t\t\t\tint a = sc.nextInt();\n" +
				"\t\t\t\t// если значение a==0 возникает исключительная ситуация\n" +
				"\t\t\t\tint b = 4 / a;\n" +
				"\t\t\t\tSystem.out.println(b);\n" +
				"\t\t\t\t//System.exit(42); \n" +
				"\t\t\t} catch (ArithmeticException e) {\n" +
				"\t\t\t\tSystem.out.println(\"Произошла недопустимая арифметическая операция\");\n" +
				"\t\t\t}\n" +
				"\t\t\tfinally {\n" +
				"\t\t\t\tSystem.out.println(\"Выполняется в любом случае\");\n" +
				"\t\t\t}\n" +
				"\t\t\tsc.close();\n" +
				"\t\t}\n" +
				"\t}");

		System.out.println("\n\nСлучаи, когда finally не будет вызван, это:\n" +
				"\n" +
				"\t1. Если вы вызовете System.exit()\n" +
				"\t2. Если JVM вылетает первым\n" +
				"\t3. Если JVM достигает бесконечного цикла (или какого - либо другого не прерываемого, не завершающего" +
				"\n\tоператора) в блоке try или catch\n" +
				"\t4. Если OS принудительно завершает процесс JVM; например, kill -9 <pid> на UNIX\n" +
				"\t5. Если хост-система умирает; например, сбой питания, аппаратная ошибка, паника OS и т. д.\n" +
				"\t6. Если блок finally будет выполняться потоком-демоном, а все остальные потоки, не являющиеся демонами," +
				"\n\tзавершатся до вызова finally");

		System.out.println("\n\nПример:\n\n\t" +
				"public class TestExc11 {\n" +
				"\t\tpublic static void main(String[] args) {\n" +
				"\t\t\ttry {\n" +
				"\t\t\t\t// прекращение работы виртуальной java-машины (JVM)\n" +
				"\t\t\t\tRuntime.getRuntime().exit(42);\n" +
				"\t\t\t\t// прекращение работы виртуальной java-машины (JVM)\n" +
				"\t\t\t\tSystem.exit(42);\n" +
				"\t\t\t\t// осуществляет немедленное прекращение работы виртуальной\n" +
				"\t\t\t\t// java-машины (JVM). Не завершает запушенных паралельно процессов.\n" +
				"\t\t\t\tRuntime.getRuntime().halt(42);\n" +
				"\n" +
				"\t\t\t} finally {\n" +
				"\t\t\t\tSystem.err.println(\"finally\"); // не будет вызван\n" +
				"\t\t\t}\n" +
				"\t\t}\n" +
				"\t}");

		System.out.println("\n\nКлючевое слово THROW\n" +
				"Оператор throw используется для того, чтобы программно возбудить исключение. Для того чтобы сделать\n" +
				"это, нужно иметь объект исключения (который можно получить в блоке catch, либо создать с помощью\n" +
				"оператора new).\n" +
				"Пример:\n\n" +
				"\tthrow new NullPointerException();\n\n" +
				"При достижении этого оператора выполнение кода прекращается, поэтому следующий за ним оператор не\n" +
				"выполнится. Вы можете возбуждать исключения в собственных методах, если, к примеру, входные данные не\n" +
				"проходят валидацию и метод может работать с ними некорректно.");

		System.out.println("\n\nКлючевое слово THROWS\n" +
				"Ключевое слово throws в методе явно задает те исключения, которые необходимо обработать при вызове\n" +
				"этого метода. Его следует применять, если метод способен возбуждать исключения, которые он сам не\n" +
				"обрабатывает.\n" +
				"Для определения списка исключений, которые могут возбуждаться методом, используется ключевое слово\n" +
				"throws и список исключений через запятую, пример:\n\n" +
				"\tpublic class Main {\n" +
				"\t\tpublic static char getCharFromString(String str, int index) throws IllegalArgumentException, IndexOutOfBoundsException {\n" +
				"\t\t\tif (index < 0)\n" +
				"\t\t\t\tthrow new IllegalArgumentException();\n" +
				"\t\t\treturn str.toCharArray()[index];\n" +
				"\t\t}\n" +
				"\t\tpublic static void main(String[] args) {\n" +
				"\t\t\ttry {\n" +
				"\t\t\t\tSystem.out.println(getCharFromString(\"hello\", 20));\n" +
				"\t\t\t} catch (IllegalArgumentException e) {\n" +
				"\t\t\t\tSystem.out.println(\"Индекс не может быть меньше нуля\");\n" +
				"\t\t\t} catch (IndexOutOfBoundsException e) {\n" +
				"\t\t\t\tSystem.out.println(\"Вы вышли за пределы массива\");\n" +
				"\t\t\t}\n" +
				"\t\t}\n" +
				"\t}" +
				"\n\nЕще пример:" +
				"\n\n\t" +
				"public class TestExc03 {\n" +
				"\t\tpublic static void main(String[] args)  {\n" +
				"\t\t\t// создаем объект исключения\n" +
				"\t\t\tError ref = new Error();\n" +
				"\t\t\t// \"бросаем\" его\n" +
				"\t\t\tthrow ref;\n" +
				"\n" +
				"\t\t\t//test(3);\n" +
				"\t\t}\n" +
				"\n" +
				"\t\t/**\n" +
				"\t\t * Ключевое слово throws в объявлении метода, говорит о том, что при\n" +
				"\t\t * выплнении данного метода, может возникнуть исключение указаного типа.\n" +
				"\t\t *\n" +
				"\t\t */\n" +
				"\t\tprivate static void test(int a) /**/ throws /**/ Exception {\n" +
				"\t\t\tif (a < 0) {\n" +
				"\t\t\t\t// не смотря на то, что это проверяемое исключение\n" +
				"\t\t\t\t// обработка не нужна, так как исключение проброщено на наверх.\n" +
				"\t\t\t\tthrow new Exception(\"a < 0\");\n" +
				"\t\t\t}\n" +
				"\t\t}\n" +
				"\t}" +
				"\n\nЕще пример:\n\n\t" +
				"\tpublic class TestExc04 {\n" +
				"\t\t\tpublic static void main(String[] args) {\n" +
				"\t\t\ttry {\n" +
				"\t\t\t\ttest();\n" +
				"\t\t\t} catch (IOException e) {\n" +
				"\t\t\t\te.printStackTrace();\n" +
				"\t\t\t}\n" +
				"\t\t}\n" +
				"\n" +
				"\t\t/**\n" +
				"\t\t * Объявление метода обязывает всех кто его будет использовать обрабатывать\n" +
				"\t\t * исключение IOException, хотя внутри метода исключение не бросается. \n" +
				"\t\t */\n" +
				"\t\tprivate static void test() throws IOException {\n" +
				"\t\t\tthrow new IOException(); \n" +
				"\t\t}\n" +
				"\t}" +
				"" +
				"\n\nЕще пример:\n\n\t" +
				"\tpublic class TestExc05 {\n" +
				"\t\t\tpublic static void main(String[] args) throws FileNotFoundException {\n" +
				"\t\t\t f();\n" +
				"\t\t}\n" +
				"\n" +
				"\t\t/**\n" +
				"\t\t *\n" +
				"\t\t * В методе может быть брошено два проверяемых исключения, но наверх\n" +
				"\t\t * пробрасываем только одно, другое ловится внутри метода.\n" +
				"\t\t */\n" +
				"\t\t\tpublic static void f() throws FileNotFoundException {\n" +
				"\t\t\ttry {\n" +
				"\t\t\t\tif (System.currentTimeMillis() % 2 == 0) {\n" +
				"\t\t\t\t\t// перехвачено в блоке try\n" +
				"\t\t\t\t\tthrow new EOFException(\"EOF\");\n" +
				"\t\t\t\t} else {\n" +
				"\t\t\t\t\t// не перехвачено\n" +
				"\t\t\t\t\tthrow new FileNotFoundException();\n" +
				"\t\t\t\t}\n" +
				"\t\t\t} catch (EOFException e) {\n" +
				"\t\t\t\tSystem.out.println(\"Catched \" + e.getMessage());\n" +
				"\t\t\t}\n" +
				"\t\t}\n" +
				"\t}");
	}

	public String getTHEME() {
		return THEME;
	}
}
