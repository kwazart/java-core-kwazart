package com.kwazart.java_core.main.theme20_exception;

import java.util.Scanner;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 28.09.2020 14:28
 */

public class TestExc01 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int[] m = new int[1];
		try {
			System.out.println("Введите 0 или 1");
			int a = sc.nextInt();

			// если значение a==0 возникает исключительная ситуация
			m[a] = 4 / a;
			// если значение a > 1 или a < 0 возникает исключительная ситуация
			System.out.println(m[a]);
		} catch (ArithmeticException e) {
			System.out.println("Произошла недопустимая арифметическая операция");
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Обращение по недопустимому индексу массива");
		}
		sc.close();
	}
}
