package com.kwazart.java_core.main.theme20_exception;

import java.util.Scanner;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 28.09.2020 14:30
 */

public class TestExc02 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		try {
			System.out.println("Введите 0 или 1");
			int a = sc.nextInt();
			// если значение a==0 возникает исключительная ситуация
			int b = 4 / a;
			System.out.println(b);
			//System.exit(42);
		} catch (ArithmeticException e) {
			System.out.println("Произошла недопустимая арифметическая операция");
		}
		finally {
			System.out.println("Выполняется в любом случае");
		}
		sc.close();
	}
}
