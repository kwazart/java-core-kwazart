package com.kwazart.java_core.main.theme20_exception;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 28.09.2020 14:38
 */

public class TestExc03 {
	public static void main(String[] args)  {
		// создаем объект исключения
		Error ref = new Error();
		// "бросаем" его
		throw ref;

		//test(3);
	}

	/**
	 * Ключевое слово throws в объявлении метода, говорит о том, что при
	 * выплнении данного метода, может возникнуть исключение указаного типа.
	 *
	 */
	private static void test(int a) /**/ throws /**/ Exception {
		if (a < 0) {
			// не смотря на то, что это проверяемое исключение
			// обработка не нужна, так как исключение проброщено на наверх.
			throw new Exception("a < 0");
		}
	}
}
