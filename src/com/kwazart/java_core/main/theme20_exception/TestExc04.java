package com.kwazart.java_core.main.theme20_exception;

import java.io.IOException;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 28.09.2020 14:45
 */

public class TestExc04 {
	public static void main(String[] args) {
		try {
			test();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Объявление метода обязывает всех кто его будет использовать обрабатывать
	 * исключение IOException, хотя внутри метода исключение не бросается.
	 */
	private static void test() throws IOException {
		throw new IOException();
	}
}
