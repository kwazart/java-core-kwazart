package com.kwazart.java_core.main.theme20_exception;

import java.io.EOFException;
import java.io.FileNotFoundException;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 28.09.2020 14:48
 */

public class TestExc05 {
	public static void main(String[] args) throws FileNotFoundException {
		 f();
	}

	/**
	 *
	 * В методе может быть брошено два проверяемых исключения, но наверх
	 * пробрасываем только одно, другое ловится внутри метода.
	 */
	public static void f() throws FileNotFoundException {
		try {
			if (System.currentTimeMillis() % 2 == 0) {
				// перехвачено в блоке try
				throw new EOFException("EOF");
			} else {
				// не перехвачено
				throw new FileNotFoundException();
			}
		} catch (EOFException e) {
			System.out.println("Catched " + e.getMessage());
		}
	}
}
