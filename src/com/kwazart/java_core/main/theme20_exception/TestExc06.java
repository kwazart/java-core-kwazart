package com.kwazart.java_core.main.theme20_exception;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 28.09.2020 14:50
 */

public class TestExc06 {
	public static void main(String[] args)
	{
		//FIXME Запустите пример несколько раз
		try
		{
			int a = 1;
			System.out.println("NO Error");
			System.err.println("Error"); // опережает вывод через System.out, т.к. отсутствует буферизация
			System.out.println(a / 0);
		}
		catch (ArithmeticException e)
		{
			System.out.println("Произошла недопустимая арифметическая операция");
		}
	}
}
