package com.kwazart.java_core.main.theme20_exception;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 28.09.2020 14:55
 */

/**
 *
 * Когда поймали одно исключение, в блоке catch можем бросить другое, которое не
 * будет поймано блоком try
 *
 */
public class TestExc07 {
	public static void main(String[] args) {
		try {
			throw new NullPointerException();
			// System.out.println("опа");
		} catch (NullPointerException npe) {
			System.err.println(1);
			throw new ArithmeticException();
		} catch (ArithmeticException ae) {
			System.err.println(2);
		}
		System.err.println(3);
	}
}
