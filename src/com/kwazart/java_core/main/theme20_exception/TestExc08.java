package com.kwazart.java_core.main.theme20_exception;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 28.09.2020 15:01
 */

/**
 *
 * Пойманое исключени можно бросить еще раз.
 *
 */
public class TestExc08
{
	public static void main(String[] args)
	{
		try
		{
			String test = null;
			test = test.trim();
		}
		catch (NullPointerException npe)
		{
			System.err.println("Поймали NPE");
			throw npe; // и снова бросаем
		}
		catch (ArithmeticException ae) {
			System.err.println(2);
		}
		System.err.println(3);
	}
}