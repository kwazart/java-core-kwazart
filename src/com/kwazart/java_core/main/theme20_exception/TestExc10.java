package com.kwazart.java_core.main.theme20_exception;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 28.09.2020 15:07
 */

/**
 *
 * Будет поймано, то что брошено, а не то что формально объявлено
 *
 */
public class TestExc10 {
	public static void main(String[] args) {
		try {
			// ссылка типа Throwable указывает на объект типа Exception
			Throwable t = new Exception();
			throw t;
		} catch (RuntimeException e) {
			System.err.println("catch RuntimeException");
		} catch (Exception e) {
			System.err.println("catch Exception");
		} catch (Throwable e) {
			System.err.println("catch Throwable");
		}
	}
}
