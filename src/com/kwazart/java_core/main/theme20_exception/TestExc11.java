package com.kwazart.java_core.main.theme20_exception;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 28.09.2020 15:10
 */

public class TestExc11 {
	public static void main(String[] args) {
		try {
			// прекращение работы виртуальной java-машины (JVM)
			Runtime.getRuntime().exit(42);
			// прекращение работы виртуальной java-машины (JVM)
			System.exit(42);
			// осуществляет немедленное прекращение работы виртуальной
			// java-машины (JVM). Не завершает запушенных паралельно процессов.
			Runtime.getRuntime().halt(42);

		} finally {
			System.err.println("finally"); // не будет вызван
		}
	}
}
