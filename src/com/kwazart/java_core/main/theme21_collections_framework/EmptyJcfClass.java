package com.kwazart.java_core.main.theme21_collections_framework;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 29.09.2020 21:30
 */

// класс-заглушка для основного меню
public class EmptyJcfClass implements InfoPrintable {
	private static final String THEME = "Java Collection Framework";
	@Override
	public void printInfo() {}

	@Override
	public String getTHEME() {
		return THEME;
	}
}
