package com.kwazart.java_core.main.theme21_collections_framework.list;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 29.09.2020 14:28
 */

public class ListInfo implements InfoPrintable {
	public static final String THEME = "List";

	public void printInfo() {
		System.out.println("\n\nLIST\n\n" +
				"List является базовым типом для коллекций, называемых последовательностями (sequence). Такие коллекции\n" +
				"являются неупорядоченными и допускают наличиеэлементов с равными значениями. Элементы в последо\n" +
				"вательностях пронумерованы и допускают обращениепо индексу.\n\n" +
				"На приведенной ранее схеме видно, что ин терфейсу List наследуют два класса: ArrayList и LinkedList.\n" +
				"Класс Vector считается устаревшим. Он не будет рассматриваться далее.\n" +
				"Для наглядности на этой схеме не указаны еще некоторые промежуточные интерфейсы и абстрактные классы.\n" +
				"Объекты классов ArrayList и LinkedList представляют собой коллекции.\n");
		// для дальнейшего разделения меню все закидываю в отдельные методы
		// TODO добавить информацию о сложности добавления в листы

		printCompareArrayAndLinkedListsInfo();
	}

	public void printCompareArrayAndLinkedListsInfo() {
		System.out.println("\n\nСравнение ArrayList и LinkedList\n" +
				"Отличия этих двух коллекций обусловлены способами хранения элементов. ArrayList хранит свои элементы как\n" +
				"массив, а LinkedList – как список. Это приводит к тому, что доступ по индексу у первой коллекции намного быстрее,\n" +
				"чем у второй. Однако операции добавления и удаления элементов в произвольном месте коллекции в LinkedList\n" +
				"выполняются быстрее. Правда, для этого надо использо вать ListIterator, который позволит быстро добраться до\n" +
				"требуемого места коллекции, а не перебирать все элементы от хвоста или головы списка.\n" +
				"Если говорить о потребляемой памяти, то LinkedList требует больше памяти, чем ArrayList, по причине\n" +
				"ипользования ссылок на соседние элементы.");
	}

	public String getTHEME() {
		return THEME;
	}
}
