package com.kwazart.java_core.main.theme21_collections_framework.map;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 29.09.2020 21:39
 */

public class HashMapInfo implements InfoPrintable {
	private static final String THEME = "HashMap";

	@Override
	public void printInfo() {
		System.out.println("\n\nHashMap\n" +
				"HashMap – это неупорядоченная коллекция пар \"ключ-значение\". Ключу каждой пары сопоставляется\n" +
				"хеш код, что ускоряет работу с элементами коллекции. Как ключ, так и значение могут быть равны null. Элементы\n" +
				"коллекции хранятся в неупорядоченном виде.\n" +
				"Полное описание этого класса можете посмотреть на официальном сайте по адресу:\n" +
				"https://docs.oracle.com/javase/8/docs/api/java/util/HashMap.html.\n\n" +
				"HashMap имеет такие конструкторы:\n\n" +
				"\tHashMap () создает пустую коллекцию с емкостью 16 и фактором загрузки 0.75;\n" +
				"\tHashMap (int capacity) создает коллекцию с начальной емкостью capacity и фактором загрузки 0.75;\n" +
				"\tHashMap (int capacity, float loadfactor) создает коллекцию с емкостью capacity и с фактором загрузки loadfactor;\n" +
				"\tHashMap ((Map<? extends K,? extends V> m)) создает коллекцию, содержащую те же элементы, что и коллекция m.\n\n" +
				"Создадим коллекцию с ключами типа String и значе ниями типа Integer.\n\n" +
				"\tHashMap<String, Integer> hm = new HashMap<String,Integer>();\n" +
				"\thm.put(\"Argentina\",1);\n" +
				"\thm.put(\"Norway\",12);\n" +
				"\thm.put(\"Canada\",10);\n" +
				"\thm.put(\"USA\",5);\n" +
				"\tfor(Map.Entry m: hm.entrySet())\n" +
				"\t{\n" +
				"\t\tSystem.out.println(m.getKey()+\" \"+m.getValue());\n" +
				"\t}\n\n" +
				"Вывод этого кода будет таким:\n\n" +
				"\tCanada 10\n" +
				"\tArgentina 1\n" +
				"\tUSA 5\n" +
				"\tNorway 12\n\n" +
				"" +
				"Как видите, порядок элементов в коллекции НЕ определяется порядком их вставки. Давайте увеличим на 5\n" +
				"значение для элемента с ключом, равным \"Argentina\".\n\n" +
				"\tint value = hm.get(\"Argentina\");\n" +
				"\thm.put(\"Argentina\", value + 5);\n" +
				"\tSystem.out.println(\"New value of Argentina:\" + hm.get(\"Argentina\"));\n\n" +
				"Вывод этого кода будет таким:\n\n" +
				"\tNew value of Argentina: 6\n\n" +
				"Теперь создадим отображение с нашим классом Fish.\n\n" +
				"" +
				"\tMap<Integer,Fish> map=new HashMap<>();\n" +
				"\tFish f1=new Fish(\"eel\",1.5,120);\n" +
				"\tFish f2=new Fish(\"salmon\",2.5,180);\n" +
				"\tFish f3=new Fish(\"carp\",2.8,80);\n" +
				"\tFish f4=new Fish(\"trout\",2.2,150);\n" +
				"\tmap.put(1,f1);\n" +
				"\tmap.put(2,f2);\n" +
				"\tmap.put(3,f3);\n" +
				"\tmap.put(4,f4);\n" +
				"\tfor(Map.Entry<Integer, Fish> entry:map.entrySet())\n" +
				"\t{\n" +
				"\t\tint key=entry.getKey();\n" +
				"\t\tFish b=entry.getValue();\n" +
				"\t\tSystem.out.println(key+\"->\"+b);\n" +
				"\t}\n\n" +
				"Вывод этого кода будет таким:\n\n" +
				"\t1->eel weight:1.5 price:120.0\n" +
				"\t2->salmon weight:2.5 price:180.0\n" +
				"\t3->carp weight:2.8 price:80.0\n" +
				"\t4->trout weight:2.2 price:150.0");
	}

	@Override
	public String getTHEME() {
		return THEME;
	}
}
