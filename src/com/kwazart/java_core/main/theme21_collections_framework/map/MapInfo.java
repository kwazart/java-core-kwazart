package com.kwazart.java_core.main.theme21_collections_framework.map;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 29.09.2020 18:14
 */

public class MapInfo implements InfoPrintable {
	public static final String THEME = "Map";

	public void printInfo() {
		System.out.println("\n\n4. Интерфейс Map\n" +
				"Интерфейс Map является базовым для коллекций, элементы которых являются не отдельными значениями,\n" +
				"а парами \"ключ-значение\". Ключ играет роль индекса для доступа к конкретной паре. Ключи в коллекции\n" +
				"уникальны. Типы ключа и значения могут быть произвольными и отличаться друг от друга. Такие коллекции еще называют\n" +
				"отображениями или словарями (Dictionary), или ассоциативными массивами. Отметьте, что интерфейс Map является\n" +
				"самостоятельным и никак не связан с интерфейсом Collection.\n\n" +
				"" +
				"Map входит в состав JCF как и интерфейс Collection.\n" +
				"На приведенной схеме видно, что интерфейсу Map наследуют четыре класса: HashMap,\n" +
				"LinkedHashMap, TreeMap и WeakHashMap. Для наглядности на этой диаграмме не указаны еще некоторые " +
				"\nпромежуточные интерфейсы и абстрактные классы.");

		System.out.println("" +
				"                         +--------------------------------------------------+\n" +
				"                         |                     <<Interface>>                |\n" +
				"                         |                       Map<K,V>                   |\n" +
				"                         +--------------------------------------------------+\n" +
				"                         | + size() : int                                   |           +-------------------------+\n" +
				"                         | + isEmpty() : boolean                            |           |      <<Interface>>      |\n" +
				"                         | + containsKey(Object o) : boolean                |           |        Entry<K,V>       |\n" +
				"                         | + containsValue(Object o) : boolean              |           +-------------------------+\n" +
				"                         | + get(Object key) : V                            +-----------+ + getKey() : K          |\n" +
				"                         | + put(K key, V value) : V                        |           | + getValue() : V        |\n" +
				"                         | + remove(Object key) : V                         |           | + getValue(V value) : V |\n" +
				"                         | + putAll(Map<? extends K, extends V m) : boolean |           +-------------------------+\n" +
				"                         | + clear() : void                                 |\n" +
				"                         | + keySet() : Set<K>                              |\n" +
				"           +-------------+ + values() : Collection<V>                       |-----------+\n" +
				"           |             | + hashCode() : int                               |           |\n" +
				"           |             +------------+-------------------------+-----------+           |\n" +
				"           |                          |                         |                       |\n" +
				"           |                          |                         |                       |\n" +
				"           |                          |                         |                       |\n" +
				" +---------+----------+   +-----------+----------+    +---------+---------+   +---------+----------+\n" +
				" |     <<Class>>      |   |       <<Class>>      |    |      <<Class>>    |   |      <<Class>>     |\n" +
				" |    HashMap<K,V>    |   |  LinkedHashMap<K,V>  |    |    TreeMap<K,V>   |   |  WeakHashMap<K,V>  |\n" +
				" +--------------------+   +----------------------+    +-------------------+   +--------------------+"
		);

		System.out.println("\n\nРассмотрим эти классы подробнее. Интерфейс Entry определяет внутренний класс в каждом\n" +
				"отображении, предназначенный для хранения пар \"ключ-значение\". При этом экземпляры самого класса\n" +
				"Entry хранятся в массиве.");
	}

	public String getTHEME() {
		return THEME;
	}
}
