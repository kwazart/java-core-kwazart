package com.kwazart.java_core.main.theme21_collections_framework.map;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 29.09.2020 22:22
 */

public class TreeMapInfo implements InfoPrintable {
	private static final String THEME = "TreeMap";

	public void printInfo() {
		System.out.println("\n\nTreeMap\n" +
				"Элементами этой коллекции также являются пары \"ключ-значение\". TreeMap использует для хранения своих\n" +
				"элементов дерево, поэтому элементы хранятся в отсортированном порядке по возрастанию. Сортировка происходит\n" +
				"в естественном порядке или на основе заданного компаратора. Время доступа к элементам TreeMap мало,\n" +
				"поэтому эта коллекция является очень хорошим выбором в том случае, когда вам часто надо находить и выбирать\n" +
				"из коллекции требуемые элементы.\n\n" +
				"Полное описание этого класса можете посмотреть на официальном сайте по адресу:\n" +
				"https://docs.oracle.com/javase/8/docs/api/java/util/TreeMap.html.\n" +
				"TreeMap имеет такие конструкторы:\n\n" +
				"\tTreeMap () создает пустую коллекцию, сортированную в естественном порядке;\n" +
				"\tTreeMap (Comparator<? super K> comparator) создает пустую коллекцию с порядком сортировки, заданным в\n" +
				"\t                                     компараторе comparator;\n" +
				"\tTreeMap (Map<? extends K,? extends V> m) создает коллекцию, содержащую те же элементы, что и коллекция m\n" +
				"\t                                     с естественным порядком сортировки;\n" +
				"\tTreeMap (SortedMap<K,? extends V> s) создает коллекцию, содержащую те же элементы, что и коллекция s с тем же\n" +
				"\t                                     порядком сортировки." +
				"\n\n" +
				"Создадим коллекцию:\n\n" +
				"\tMap tm = new TreeMap<>();\n" +
				"\ttm.put(\"Brazil\", \"Brazilia\");\n" +
				"\ttm.put(\"Canada\", \"Ottawa\");\n" +
				"\ttm.put(\"Denmark\", \"Copenhagen\");\n" +
				"\ttm.put(\"France\", \"Paris\");\n" +
				"\ttm.put(\"Russia\", \"Moscow\");\n" +
				"\ttm.put(\"Sweden\", \"Stockholm\");\n" +
				"\tfor(Map.Entry e : tm.entrySet())\n" +
				"\t{\n" +
				"\t\tSystem.out.println(e.getKey()+\" \"+ e.getValue());\n" +
				"\t}\n\n" +
				"Вывод этого кода будет таким:\n\n" +
				"\tBrazil Brazilia\n" +
				"\tCanada Ottawa\n" +
				"\tDenmark Copenhagen\n" +
				"\tFrance Paris\n" +
				"\tSweden Stockholm\n" +
				"\tRussia Moscow\n\n" +
				"" +
				"Видно, что элементы отсортированы по правилам сортировки для типа String. Такой порядок сортировки\n" +
				"называется естественным.\n" +
				"Для перебора элементов коллекции можно было использовать итератор:\n\n" +
				"\tIterator iter = tm.entrySet().iterator();\n" +
				"\twhile(iter.hasNext())\n" +
				"\t{\n" +
				"\t\tEntry entry = (Entry) iter.next();\n" +
				"\t\tString key = (String) entry.getKey();\n" +
				"\t\t//get key of the current element\n" +
				"\t\tString value = (String) entry.getValue();\n" +
				"\t\t// get value of the current element\n" +
				"\t\tSystem.out.println(key+\"-> \"+ value);\n" +
				"\t}\n\n" +
				"" +
				"Иногда бывает полезно из какой-либо коллекции Map получить отдельно коллекцию либо ключей, либо\n" +
				"значений. Это можно сделать таким образом:\n\n" +
				"" +
				"\tList keyList = new ArrayList(tm.keySet());\n" +
				"\t//list of keys\n" +
				"\tList valueList = new ArrayList(tm.valueSet());\n" +
				"\t//list of values\n\n" +
				"" +
				"В первом случае мы создаем ArrayList из множества ключей отображения tm, полученного методом keySet().\n" +
				"Во втором случае мы создаем ArrayList из множества значений отображения tm, полученного методом valueSet().\n" +
				"Теперь создадим множество с порядком сортировки, заданным в компараторе. В отличие от предыдущего\n" +
				"случая, когда мы создавали компаратор в виде отдельного класса, сейчас используем анонимный компаратор.\n" +
				"Сделаем ключами объекты нашего класса Fish и будем сортировать эти объекты по цене. С объектами f4 и f5\n" +
				"снова произойдет коллизия.\n\n" +
				"" +
				"\tMap<Fish,Double> tmc = new TreeMap<>(\n" +
				"\tnew Сomparator<Fish>(){\n" +
				"\t\t@Override\n" +
				"\t\tpublic int compare(Fish o1, Fish o2) {\n" +
				"\t\t\t(int)(o1.getPrice() * 100 -\n" +
				"\t\t\to2.getPrice() * 100);\n" +
				"\t\t}\n" +
				"\t});\n" +
				"\tFish f1=new Fish(\"eel\",1.5,120);\n" +
				"\tFish f2=new Fish(\"salmon\",2.5,180);\n" +
				"\tFish f3=new Fish(\"trout\",3.2,220);\n" +
				"\tFish f4=new Fish(\"salmon\",2.2,150);\n" +
				"\tFish f5=new Fish(\"salmon\",2.2,150);\n" +
				"\ttmc.put(f1, 120.0);\n" +
				"\ttmc.put(f2, 180.0);\n" +
				"\ttmc.put(f3, 220.0);\n" +
				"\ttmc.put(f4, 150.0);\n" +
				"\ttmc.put(f5, 150.0);\n" +
				"\tfor(Object eo : tmc.entrySet())\n" +
				"\t{\n" +
				"\t\tMap.Entry e=(Map.Entry)eo;\n" +
				"\t\tSystem.out.println(e.getKey()+\"->\"+ e.getValue());\n" +
				"\t}\n\n" +
				"" +
				"Вывод этого кода будет таким:\n\n" +
				"" +
				"\teel weight:1.5 price:120.0->120.0\n" +
				"\tsalmon weight:2.2 price:150.0->150.0\n" +
				"\tsalmon weight:2.5 price:180.0->180.0\n" +
				"\ttrout weight:3.2 price:220.0->220.0");
	}

	@Override
	public String getTHEME() {
		return THEME;
	}
}
