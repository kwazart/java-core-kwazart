package com.kwazart.java_core.main.theme21_collections_framework.map;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 29.09.2020 22:46
 */

public class WeakHashMapInfo implements InfoPrintable {
	private static final String THEME = "WeakHashMap";

	public void printInfo() {
		System.out.println("\n\nWeakHashMap\n" +
				"WeakHashMap использует для обращения к значениям своих элементов так называемые \"слабые ссылки\"\n" +
				"(weak reference). Эти ссылки особым образом обрабатываются системой сборки мусора. Как только ключ какого-либо\n" +
				"элемента коллекции перестает использоваться вашим кодом, этот элемент удаляется системой сборки мусора.\n" +
				"Другими словами, если ваш код каким-либо образом удалил ключ какого-то элемента такой коллекции, то\n" +
				"при следующем запуске сборки мусора этот элемент будет удален из коллекции. Это приведет к \"странным\"\n" +
				"последствиям, таким как уменьшение размера коллекции. Коллекция такого вида полезна, если для вас важно\n" +
				"экономить ресурсы, если вам необходимо хранить очень большие объемы данных. Другим полезным качеством\n" +
				"такой коллекции является отсутствие утечек памяти, которые обычно создаются \"потерянными\" ссылками.\n" +
				"В случае WeakHashMap потерянные ссылки удаляются сборщиком мусора, исключая утечку памяти.\n\n" +
				"Полное описание этого класса можете посмотреть на официальном сайте по адресу:\n" +
				"https://docs.oracle.com/javase/8/docs/api/java/util/WeakHashMap.html.\n" +
				"WeakHashMap имеет такие конструкторы:\n\n" +
				"\tWeakHashMap () создает пустую коллекцию с емкостью 16 и фактором загрузки 0.75;\n" +
				"\tWeakHashMap (int capacity) создает коллекцию с начальной емкостью capacity и фактором загрузки 0.75;\n" +
				"\tWeakHashMap (int capacity, float loadfactor) создает коллекцию с емкостью capacity и с фактором загрузки loadfactor;\n" +
				"\tWeakHashMap ((Map<? extends K,? extends V> m)) создает коллекцию, содержащую те же элементы, что и коллекция m.\n\b" +
				"" +
				"Посмотрим, как это работает.\n" +
				"\tMap<Fish,Double> wtm = new WeakHashMap<>();\n" +
				"\tFish f1=new Fish(\"eel\",1.5,120);\n" +
				"\tFish f2=new Fish(\"salmon\",2.5,180);\n" +
				"\tFish f3=new Fish(\"trout\",3.2,220);\n" +
				"\twtm.put(f1, 120.0);\n" +
				"\twtm.put(f2, 180.0);\n" +
				"\twtm.put(f3, 220.0);\n" +
				"\tSystem.out.println(\"Before:\");\n" +
				"\tfor(Object eo : wtm.entrySet())\n" +
				"\t{\n" +
				"\t\tMap.Entry e=(Map.Entry)eo;\n" +
				"\t\tSystem.out.println(e.getKey()+\"->\"+ e.getValue());\n" +
				"\t}\n" +
				"\tf2=null;\n" +
				"\t//do something huge to invoke garbage collector\n" +
				"\tfor(int i=0; i<10000; i++) {\n" +
				"\t\tbyte b[] = new byte[1000000];\n" +
				"\t\tb = null;\n" +
				"\t}\n" +
				"\tSystem.out.println(\"After:\");\n" +
				"\tfor(Object eo : wtm.entrySet())\n" +
				"\t{\n" +
				"\t\tMap.Entry e=(Map.Entry)eo;\n" +
				"\t\tSystem.out.println(e.getKey()+\"->\"+ e.getValue());\n" +
				"\t}\n\n" +
				"Вывод этого кода будет таким:\n\n" +
				"\tBefore:\n" +
				"\tsalmon weight:2.5 price:180.0->180.0\n" +
				"\ttrout weight:3.2 price:220.0->220.0\n" +
				"\teel weight:1.5 price:120.0->120.0\n\n" +
				"After:\n" +
				"trout weight:3.2 price:220.0->220.0\n" +
				"eel weight:1.5 price:120.0->120.0\n" +
				"" +
				"Мы создали коллекцию из трех элементов и увидели их после выполнения первого цикла. Затем удалили\n" +
				"объект, который является ключом одного из элементов коллекции. После этого выполнили бессмысленные\n" +
				"действия, чтобы загрузить память и вызвать сборку мусора. После чего снова вывели содержимое коллекции на экран\n" +
				"и увидели, что в ней осталось лишь два элемента. Элемент коллекции, ключ которого \"потерял\" свою ссылку, был\n" +
				"автоматически удален из коллекции. Замените в этом примере WeakHashMap на другую коллекцию, например,\n" +
				"HashMap, и убедитесь, что удаления элемента из коллекции не произойдет.");
	}

	@Override
	public String getTHEME() {
		return THEME;
	}
}
