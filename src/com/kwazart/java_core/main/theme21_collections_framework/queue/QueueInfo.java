package com.kwazart.java_core.main.theme21_collections_framework.queue;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 29.09.2020 17:06
 */

public class QueueInfo implements InfoPrintable {
	public static final String THEME = "Queue";

	public void printInfo() {
		System.out.println("\n\nQueue\n" +
				"Queue (читается, как \"кью\") является базовым для коллекций, хранящих свои элементы в порядке,\n" +
				"определяющем очередность их обработки. У этого интерфейса есть единственный прямой класс наследник – PriorityQueue.");
		printPriorityQueueInfo();
	}

	private void printPriorityQueueInfo() {
		System.out.println("\n\nPriorityQueue\n" +
				"Коллекция PriorityQueue называется очередью и хранит свои элементы в порядке, необходимом для их обработки.\n" +
				"Она упорядочивает элементы либо в порядке их естественной сортировки (задаваемой интерфейсом Comparable),\n" +
				"либо в порядке сортировки, определенной в интерфейсе Comparator, переданном в коллекцию через конструктор.\n" +
				"По умолчанию очередь реализует принцип FIFO, но это, при необходимости, можно изменить.\n" +
				"Полное описание этого класса можете посмотреть на официальном сайте по адресу:\n" +
				"https://docs.oracle.com/javase/8/docs/api/java/util/PriorityQueue.html.\n\n" +
				"PriorityQueue имеет такие конструкторы:\n\n" +
				"\tPriorityQueue () создает пустую коллекцию с естественным порядком размещения;\n" +
				"\tPriorityQueue (Collection <? extends E> c) создает коллекцию, в которую добавляются все элементы коллекции c\n" +
				"\t                                          и хранятся в естественном для c  порядке;\n" +
				"\tPriorityQueue (int capacity) создает коллекцию с начальной емкостью capacity;\n" +
				"\tPriorityQueue (int capacity, Comparator <? super E> comparator) создает коллекцию с емкостью\n" +
				"\t                                          capacity и с порядком хранения, заданным в компараторе comparator;\n" +
				"\tPriorityQueue (SortedSet<E> s) создает коллекцию, содержащую те же элементы, что и коллекция s, с тем же\n" +
				"\t                                          порядком хранения\n" +
				"\tPriorityQueue (PriorityQueue <E> q) создает коллекцию, содержащую те же элементы, что и коллекция q, с тем же\n" +
				"\t                                          порядком хранения.\n\n" +
				"" +
				"У очереди есть одна интересная особенность, связанная с методами для добавления и удаления элементов. Для\n" +
				"выполнения каждого из перечисленных действий создано по два метода, и не всегда понятно, чем они отличаются.\n" +
				"Поэтому отметьте для себя следующее.\n" +
				"Для добавления в очередь новых элементов можно использовать методы add(value) или offer(value).\n" +
				"Разница между ними заключается в том, что при неудачном завершении действия add(value) вызывает исключение,\n" +
				"а offer(value) возвращает false.\n" +
				"Для удаления из очереди первого элемента и его возвращения можно использовать методы remove() или\n" +
				"poll(). Разница между ними заключается в том, что при неудачном завершении действия remove() вызывает\n" +
				"исключение, а poll() возвращает null.\n" +
				"Для извлечения из очереди первого элемента без его удаления можно использовать методы element() или\n" +
				"peek(). Разница между ними заключается в том, что при неудачном завершении действия element() вызывает\n" +
				"исключение, а peek() возвращает null.\n" +
				"Поскольку значение null является служебным в классе PriorityQueue, вы не можете вставлять в очередь элементы,\n" +
				"равные null. Если у вас есть такая потребность, используйте вместо PriorityQueue коллекцию LinkedList.\n" +
				"Рассмотрим использование очереди.\n\n" +
				"" +
				"\tPriorityQueue<Integer> pq = new PriorityQueue<>();\n" +
				"\tpq.add(4);\n" +
				"\tpq.add(3);\n" +
				"\tpq.add(1);\n" +
				"\tpq.offer(9);\n" +
				"\tSystem.out.println(\"Collection:\"+pq);\n" +
				"\tSystem.out.println(\"Collection's size:\"+pq.size());\n" +
				"\tSystem.out.println(\"\\nCollection Using Iterator:\");\n" +
				"\tIterator<Integer> iter = pq.iterator();\n" +
				"\twhile(iter.hasNext())\n" +
				"\t{\n" +
				"\t\tSystem.out.println(iter.next());\n" +
				"\t}\n\n" +
				"" +
				"Вывод этого кода будет таким:\n\n" +
				"" +
				"\tCollection:[1, 4, 3, 9]\n" +
				"\tCollection's size:4\n" +
				"\tCollection Using Iterator:\n" +
				"\t1\n\t4\n\t3\n\t9\n\n" +
				"" +
				"Как написано в документации по приведенной выше ссылке:\n" +
				"\tThe Iterator provided in method iterator() is not gua\n" +
				"\tranteed to traverse the elements of the priority queue\n" +
				"\tin any particular order. If you need ordered traversal,\n" +
				"\tconsider using Arrays.sort(pq.toArray()).\n\n" +
				"" +
				"Именно это мы и видим в приведенном фрагменте кода.\n" +
				"Рассмотрим разные способы получения первого элемента очереди: с удалением этого элемента из очереди\n" +
				"и без удаления.\n\n" +
				"" +
				"\tSystem.out.println(\"Picking the head of the queue: \" + pq.peek());\n" +
				"\tSystem.out.println(\"Collection:\"+pq);\n" +
				"\tSystem.out.println(\"Collection's size:\"+pq.size());\n" +
				"\tSystem.out.println(\"Polling the head of the queue: \"+ pq.poll());\n" +
				"\tSystem.out.println(\"Collection:\"+pq);\n" +
				"\tSystem.out.println(\"Collection's size:\"+pq.size());\n\n" +
				"" +
				"Вывод этого кода будет таким:\n\n" +
				"" +
				"\tPicking the head of the queue: 1\n" +
				"\tCollection:[1, 4, 3, 9, 12]\n" +
				"\tCollection's size:5\n" +
				"\tPolling the head of the queue: 1\n" +
				"\tCollection:[3, 4, 12, 9]\n" +
				"\tCollection's size:4\n\n" +
				"" +
				"Как видите, использование метода peek() позволяет получить первый элемент очереди, не удаляя его, в то\n" +
				"время как использование метода poll() удаляет этот элемент из очереди.\n" +
				"В рассмотренных примерах элементы хранились в очереди в порядке их добавления. Давайте создадим очередь\n" +
				"с компаратором, элементы которой будут храниться в ней не в порядке добавления в очередь, а в порядке,\n" +
				"определяемом компаратором. Мы работали с очередью с элементами типа Integer, поэтому создадим компаратор\n" +
				"для этого типа, только сортировать будем в порядке уменьшения значений элементов.\n\n" +
				"" +
				"\t//create the comparator\n" +
				"\tComparator<Integer> comparator = new Comparator<Integer>()\n" +
				"\t{\n" +
				"\t\t@Override\n" +
				"\t\tpublic int compare(Integer o1, Integer o2)\n" +
				"\t\t{\n" +
				"\t\t\tif( o1 > o2 ) //if first element is greater\n" +
				"\t\t\t{ //we return negative value\n" +
				"\t\t\t\treturn -1; //to get descending sort order\n" +
				"\t\t\t}\n" +
				"\t\t\tif( o1 < o2 ) //if first element is less\n" +
				"\t\t\t{ //we return positive value\n" +
				"\t\t\t\treturn 1; //to get descending sort order\n" +
				"\t\t\t}\n" +
				"\t\t\treturn 0; //if the elements are equal\n" +
				"\t\t}\n" +
				"\t};\n\n" +
				"" +
				"\t//create queue with Integer elements in random order\n" +
				"\tQueue<Integer> pq = new PriorityQueue<>(comparator);\n" +
				"\tpq.add(4);\n" +
				"\tpq.add(3);\n" +
				"\tpq.add(5);\n" +
				"\tpq.add(9);\n" +
				"\tpq.offer(1);\n" +
				"\t//show the collection\n" +
				"\tIterator<Integer> iter = pq.iterator();\n" +
				"\twhile(iter.hasNext())\n" +
				"\t{\n" +
				"\t\tSystem.out.println(iter.next());\n" +
				"\t}\n\n" +
				"Вывод этого кода будет таким:\n\n" +
				"\t9\n\t5\n\t4\n\t3\n\t1\n\n" +
				"" +
				"Как видите, элементы очереди перебираются итератором в соответствии с нашим компаратором. А в какой\n" +
				"очередности элементы будут извлекаться из очереди? Проверим. Добавьте в код такой цикл:\n\n" +
				"" +
				"\tSystem.out.println(\"Removing elements from the queue:\");\n" +
				"\twhile( !pq.isEmpty() )\n" +
				"\t{\n" +
				"\t\tSystem.out.println( pq.remove() );\n" +
				"\t}\n\n" +
				"" +
				"Вывод этого кода будет таким:\n\n" +
				"\tRemoving elements from the queue:\n" +
				"\t9\n\t5\n\t4\n\t3\n\t1\n\n" +
				"Элементы из очереди извлекаются в порядке, заданном компаратором, а не порядке добавления их в очередь.");
	}

	public String getTHEME() {
		return THEME;
	}
}
