package com.kwazart.java_core.main.theme21_collections_framework.set;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 29.09.2020 21:49
 */

public class LinkedHashSetInfo implements InfoPrintable {
	public static final String THEME = "LinkedHashSet";

	public void printInfo() {
		System.out.println("\n\nLinkedHashSet\n" +
				"LinkedHashSet содержит все те же методы, что и класс HashSet и отличается от последнего тем, что хранит\n" +
				"элементы коллекции в порядке их добавления. Эта коллекция немного медленнее, чем HashSet.\n" +
				"Полное описание этого класса можете посмотреть на официальном сайте по адресу:\n" +
				"https://docs.oracle.com/javase/8/docs/api/java/util/LinkedHashSet.html.\n" +
				"Из особенностей этой коллекции можно отметить такую. Если производится попытка добавить в коллекцию\n" +
				"элемент, который в ней уже существует, то возможны два варианта:\n" +
				"1. Совпадающий с добавляемым существующий элемент коллекции удаляется, и в коллекцию добавляется\n" +
				"новый елемент;\n" +
				"2. Коллекция не изменяется, и добавляемый элемент отвергается. LinkedHashSet выполняет второй сценарий.\n" +
				"Во всех остальных своих чертах эта коллекция совпадает с рассмотренной коллекцией HashSet.\n\n" +
				"LinkedHashSet имеет такие конструкторы:\n\n" +
				"\tLinkedHashSet () создает пустую коллекцию;\n" +
				"\tLinkedHashSet (Collection <? extends E> c) создает коллекцию, в которую при создании добавляются все элементы коллекции c;\n" +
				"\tLinkedHashSet (int capacity) создает коллекцию с начальной емкостью capacity и с фактором загрузки равным 0.75;\n" +
				"\tLinkedHashSet (int capacity, float loadfactor) создает коллекцию с начальной емкостью capacity и с заданным фактором загрузки.\n" +
				"Как видите, здесь тоже полное совпадение с HashSet.");
	}

	@Override
	public String getTHEME() {
		return THEME;
	}
}
