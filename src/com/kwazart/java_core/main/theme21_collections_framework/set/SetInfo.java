package com.kwazart.java_core.main.theme21_collections_framework.set;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 29.09.2020 15:53
 */

public class SetInfo implements InfoPrintable {
	public static final String THEME = "Set";

	public void printInfo() {
		System.out.println("\n\nSet\n" +
				"Set является базовым для коллекций, являющихся неупорядоченными и не допускающих наличия элементов\n" +
				"с равными значениями. Элементами с равными значениями называются такие элементы коллекции el1 и el2,\n" +
				"для которых выполняется условие el1.equals(el2). Такие коллекции называются множествами.\n" +
				"На приведенной ранее схеме видно, что интерфейс Set наследуют три класса: HashSet, LinkedHashSet\n" +
				"и TreeSet. Для наглядности на этой диаграмме не указаны еще некоторые промежуточные интерфейсы и\n" +
				"абстрактные классы.");
		// для дальнейшего разделения меню все закидываю в отдельные методы
		// TODO добавить информацию о сложности добавления в листы
	}

	public String getTHEME() {
		return THEME;
	}
}
