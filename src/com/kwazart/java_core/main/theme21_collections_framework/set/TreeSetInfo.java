package com.kwazart.java_core.main.theme21_collections_framework.set;

import com.kwazart.java_core.main.model.InfoPrintable;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 29.09.2020 21:51
 */

public class TreeSetInfo implements InfoPrintable {
	public static final String THEME = "TreeSet";

	public void printInfo() {
		System.out.println("\n\nTreeSet\n" +
				"Эта коллекция для хранения своих элементов использует дерево и поэтому всегда содержит элементы в\n" +
				"отсортированном по возрастанию порядке. Правда, обработка элементов в TreeSet выполняется медленнее,\n" +
				"чем в HashSet или LinkedHashSet.\n" +
				"Полное описание этого класса можете посмотреть на официальном сайте по адресу:\n" +
				"https://docs.oracle.com/javase/8/docs/api/java/util/TreeSet.html.\n\n" +
				"TreeSet имеет такие конструкторы:\n\n" +
				"\tTreeSet () создает пустую коллекцию;\n" +
				"\tTreeSet (Collection <? extends E> c) создает коллекцию, в которую при создании добавляются все элементы\n" +
				"\t                                           коллекции c, сортированные в порядке, естественном для c;\n" +
				"\tTreeSet (Comparator<? super E> comparator) создает пустую коллекцию с порядком сортировки, заданным в компараторе\n" +
				"\t                                           comparator;\n" +
				"\tTreeSet(SortedSet<E> s) создает коллекцию, содержащую те же элементы, что и коллекция s, с теми же правилами\n" +
				"\t                                           сортировки.\n\n" +
				"Выполните такой код:\n\n" +
				"\tTreeSet<String> ts = new TreeSet<>();\n" +
				"\tts.add(\"Georgia\");\n" +
				"\tts.add(\"Argentina\");\n" +
				"\tts.add(\"Russia\");\n" +
				"\tts.add(\"Belgium\");\n" +
				"\tts.add(\"Canada\");" +
				"\tSystem.out.println(\"Collection:\"+ts);\n" +
				"\tSystem.out.println(\"Collection's size:\"+ts.size());\n\n" +
				"Вы увидите список добавленных стран в отсортированном порядке:\n\n" +
				"\tCollection:[Argentina, Belgium, Canada, Georgia, Russia]\n" +
				"\tCollection's size:5\n\n" +
				"" +
				"Попробуем создать коллекцию TreeSet из объектов нашего класса Fish. Если использовать класс Fish в том\n" +
				"виде, в каком он у нас сейчас существует, мы получим исключение ClassCastException: listexample.Fish cannot\n" +
				"be cast to java.lang.Comparable.\n" +
				"В переводе на человеческий язык это означает, что JVM не знает, как сравнивать объекты нашего класса Fish. По\n" +
				"имени? По весу или цене? Чтобы решить эту проблему, надо использовать либо интерфейс Comparable, либо интерфейс\n" +
				"Comparator. Каждый из этих интерфейсов позволяет задать правила сравнения объектов какого-либо класса.\n" +
				"Рассмотрим сначала интерфейс Comparable и укажем, что хотим сортировать рыбок по цене. Для этого\n" +
				"надо указать, что наш класс Fish реализует интерфейс Comparable. В этом интерфейсе объявлен метод c\n" +
				"сигнатурой int compareTo(Object o). Он вызывается от имени тестируемого объекта класса, а в параметре ему передается\n" +
				"объект, с которым надо выполнить сравнение. Логика такая: если этот метод возвращает целое значение больше 0, значит,\n" +
				"тестируемый объект больше переданного, если метод возвращает 0, значит, оба объекта равны, и если метод\n" +
				"возвращает значение меньше 0 – значит, тестируемый объект меньше переданного. Еще раз обратите внимание,\n" +
				"что тип возвращаемого значения – int. Параметр метода compareTo() является объектом типа Object, и его надо\n" +
				"приводить к требуемому типу (в нашем случае – к Fish).\n" +
				"Однако существует версия параметризированного интерфейса Comparable, при применении которой тип параметра\n" +
				"метода compareTo() будет типа вашего класса.\n" +
				"Этой версией Comparable мы и воспользуемся. Приведем класс Fish к такому виду:\n\n" +
				"\tpublic class Fish implements Comparable<Fish>\n" +
				"\t{\n" +
				"\t\tprivate String name;\n" +
				"\t\tprivate double weight;\n" +
				"\t\tprivate double price;\n" +
				"\t\tpublic Fish(String name, double weight, double price)\n" +
				"\t\t{\n" +
				"\t\t\tthis.name=name;\n" +
				"\t\t\tthis.weight=weight;\n" +
				"\t\t\tthis.price=price;\n" +
				"\t\t}\n" +
				"\t\tpublic boolean equals(Object o)\n" +
				"\t\t{\n" +
				"\t\t\tif (o == this) return true;\n" +
				"\t\t\tif (!(o instanceof Fish))\n" +
				"\t\t\t{\n" +
				"\t\t\t\treturn false;\n" +
				"\t\t\t}\n" +
				"\t\t\tFish tmp = (Fish) o;\n" +
				"\t\t\treturn (tmp.name.equals(this.name) &&\n" +
				"\t\t\ttmp.weight == this.weight &&\n" +
				"\t\t\ttmp.price == (this.price));\n" +
				"\t\t}\n" +
				"\t\tpublic int hashCode()\n" +
				"\t\t{\n" +
				"\t\t\tint code = 17;\n" +
				"\t\t\tcode = 31 * code + this.name.hashCode();\n" +
				"\t\t\tcode = 31 * code + (int)this.weight;\n" +
				"\t\t\tcode = 31 * code + (int)this.price;\n" +
				"\t\t\treturn code;\n" +
				"\t\t}\n" +
				"\t\t@Override\n" +
				"\t\tpublic String toString()\n" +
				"\t\t{\n" +
				"\t\t\treturn this.name+\" weight:\"+this.weight+\"\n" +
				"\t\t\tprice:\"+this.price;\n" +
				"\t\t}\n" +
				"\t\t@Override\n" +
				"\t\tpublic int compareTo(Fish o)\n" +
				"\t\t{\n" +
				"\t\t\treturn (int)(this.price * 100 - o.price * 100);\n" +
				"\t\t}\n" +
				"\t}\n\n" +
				"" +
				"Умножение на 100 используется для увеличения точности. Без него рыбы с ценами, например, 125.5 и 125.99,\n" +
				"рассматривались бы как одинаковые.\n" +
				"Теперь коллекция рыбок будет создана, и они будут отсортированы по цене. Этот код:\n\n" +
				"\tTreeSet<Fish> fishes = new TreeSet<>();\n" +
				"\tfishes.add(new Fish(\"eel\",1.5,120));\n" +
				"\tfishes.add(new Fish(\"salmon\",2.5,180));\n" +
				"\tfishes.add(new Fish(\"carp\",3.5,80));\n" +
				"\tfishes.add(new Fish(\"trout\",2.2,150));\n" +
				"\tfishes.add(new Fish(\"trout\",2.2,150));\n" +
				"\tSystem.out.println(\"Collection:\"+fishes);\n" +
				"\tSystem.out.println(\"Collection's size:\"+fishes.size());\n\n" +
				"выведет на экран такое описание коллекции:\n\n" +
				"\tCollection:[carp weight:3.5 price:80.0, eel weight:1.5 price:120.0,\n" +
				"\ttrout weight:2.2 price:150.0, salmon weight:2.5 price:180.0]\n" +
				"\tCollection’s size: 4\n\n" +
				"" +
				"Здесь все просто, однако есть одна оговорка. Мы можем использовать интерфейс Comparable, если у нас\n" +
				"есть возможность изменять код требуемого класса, чтобы добавить классу наследование и реализовать в нем метод\n" +
				"compareTo(). А такая возможность есть не всегда. Как быть в случае, если мы не можем изменять определение\n" +
				"класса? Если класс нам недоступен для редактирования?\n" +
				"В этом случае нам поможет интерфейс Comparator. Рассмотрим использование интерфейса Comparator\n" +
				"для решения той же задачи – указать правила сравнения объектов нашего класса. При этом мы не будем вносить\n" +
				"никаких изменений в класс, объекты которого будем\n" +
				"сравнивать. Создадим рядом с классом Fish такой класс:\n\n" +
				"\tpublic class FishComparator implements Comparator<Fish>\n" +
				"\t{\n" +
				"\t\t@Override\n" +
				"\t\tpublic int compare(Fish o1, Fish o2)\n" +
				"\t\t{\n" +
				"\t\t\treturn (int)(o1.getWeight() * 100 -\n" +
				"\t\t\to2.getWeight() * 100);\n" +
				"\t\t}\n" +
				"\t}\n\n" +
				"" +
				"Имя этого класса произвольно, а главное в нем то, что он наследует интерфейсу Comparator<Fish>. В этом классе\n" +
				"метод compare() переопределяется по тем же правилам, что и метод compareTo() из интерфейса Comparable.\n" +
				"Но у этого метода уже определены два параметра, поскольку он обращается к сравниваемому классу извне – ведь\n" +
				"операция сравнения бинарная. В случае интерфейса Comparable у метода compareTo() был один параметр, поскольку вторым\n" +
				"сравниваемым объектом выступал объект, от имени которого вызывался метод compareTo().\n\n" +
				"В этом случае мы хотим сортировать рыбок по весу. Умножение на 100 требуется для увеличения точности.\n" +
				"Без него рыбки с весом 2.2, 2.5 и 2.8 рассматривались бы как одинаковые.\n" +
				"При этом в классе Fish мы ничего не изменяем. Точнее говоря, нам пришлось добавить в этот класс геттер для\n" +
				"веса, чтобы в FishComparator получить доступ к private полю weight объектов класса Fish. Наличие геттера\n" +
				"является очень легким требованием к классу, ведь в Java все классы должны представлять собой Java bean.\n\n" +
				"\tpublic class Fish\n" +
				"\t{\n" +
				"\t\tprivate String name;\n" +
				"\t\tprivate double weight;\n" +
				"\t\tprivate double price;\n" +
				"\t\tpublic Fish(String name, double weight, double price)\n" +
				"\t\t{\n" +
				"\t\t\tthis.name=name;\n" +
				"\t\t\tthis.weight=weight;\n" +
				"\t\t\tthis.price=price;\n" +
				"\t\t}\n" +
				"\t\tpublic boolean equals(Object o)\n" +
				"\t\t{\n" +
				"\t\t\tif (o == this) return true;\n" +
				"\t\t\tif (!(o instanceof Fish))\n" +
				"\t\t\t{\n" +
				"\t\t\t\treturn false;\n" +
				"\t\t\t}\n" +
				"\t\tFish tmp = (Fish) o;\n" +
				"\t\treturn (tmp.name.equals(this.name) &&\n" +
				"\t\ttmp.weight == this.weight &&\n" +
				"\t\ttmp.price == (this.price));\n" +
				"\t\t}\n" +
				"\t\tpublic int hashCode()\n" +
				"\t\t{\n" +
				"\t\t\tint code = 17;\n" +
				"\t\t\tcode = 31 * code + this.name.hashCode();\n" +
				"\t\t\tcode = 31 * code + (int)this.weight;\n" +
				"\t\t\tcode = 31 * code + (int)this.price;\n" +
				"\t\t\treturn code;\n" +
				"\t\t}\n" +
				"\t\tpublic double getWeight()\n" +
				"\t\t{\n" +
				"\t\t\treturn this.weight;\n" +
				"\t\t}\n" +
				"\t\t@Override\n" +
				"\t\tpublic String toString()\n" +
				"\t\t{\n" +
				"\t\t\treturn this.name+\" weight:\"+this.weight+\"\n" +
				"\t\t\tprice:\"+this.price;\n" +
				"\t\t}\n" +
				"\t}\n\n" +
				"" +
				"Теперь при создании коллекции TreeSet надо в конструктор коллекции передать объект класса FishComparator.\n\n" +
				"\tTreeSet<Fish> fishes = new TreeSet<>(new FishComparator());\n" +
				"\tfishes.add(new Fish(\"eel\",1.5,120));\n" +
				"\tfishes.add(new Fish(\"salmon\",2.5,180));\n" +
				"\tfishes.add(new Fish(\"carp\",3.5,80));\n" +
				"\tfishes.add(new Fish(\"trout\",2.2,150));\n" +
				"\tfishes.add(new Fish(\"trout\",2.8,150));\n" +
				"\tSystem.out.println(\"Collection:\"+fishes);\n" +
				"\tSystem.out.println(\"Collection's size:\"+fishes.size());\n\n" +
				"Вот вывод этого кода:\n\n" +
				"\tCollection:[eel weight:1.5 price:120.0, trout weight:2.2 price:150.0,\n" +
				"\tsalmon weight:2.5 price:180.0, trout weight:2.8 price:150.0, carp weight:3.5 price:80.0]\n" +
				"\tCollection's size:5\n\n" +
				"Как видите, мы добились желаемого результата, не изменяя класс Fish.\n" +
				"Запомните, интерфейсы Comparable и Comparator позволяют указать правила сравнения для объектов любого\n" +
				"класса. При этом использование интерфейса Comparable требует доступа к классу для его изменения. Интерфейс\n" +
				"Comparator позволяет задать правила сравнения без изменения целевого класса. Обратите внимание на то, что\n" +
				"в обоих этих интерфейсах описано по одному методу.\n");
	}

	@Override
	public String getTHEME() {
		return THEME;
	}
}
