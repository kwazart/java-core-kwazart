package com.kwazart.java_core.main.util;

import com.kwazart.java_core.main.model.InfoPrintable;
import com.kwazart.java_core.main.model.ThemeRepository;
import com.kwazart.java_core.main.theme022_io.EmptyIOClass;
import com.kwazart.java_core.main.theme022_io.FileInfo;
import com.kwazart.java_core.main.theme022_io.IOInfo;
import com.kwazart.java_core.main.theme022_io.input_output_stream.info.*;
import com.kwazart.java_core.main.theme022_io.reader_writer.info.FileReaderWriterInfo;
import com.kwazart.java_core.main.theme022_io.reader_writer.info.IOStreamReaderWriterInfo;
import com.kwazart.java_core.main.theme022_io.system.SystemInfo;
import com.kwazart.java_core.main.theme023_serializable.SerializableInfo;
import com.kwazart.java_core.main.theme024_multithreading.MultiInfo;
import com.kwazart.java_core.main.theme025_lambda.LambdaInfo;
import com.kwazart.java_core.main.theme02_condition.LogicalInfo;
import com.kwazart.java_core.main.theme03_loops.LoopInfo;
import com.kwazart.java_core.main.theme01_variable.VariableInfo;
import com.kwazart.java_core.main.theme04_git.GitInfo;
import com.kwazart.java_core.main.theme05_methods.MethodInfo;
import com.kwazart.java_core.main.theme08_encapsulation.EncapsulationInfo;
import com.kwazart.java_core.main.theme09_inheritance.InheritanceInfo;
import com.kwazart.java_core.main.theme10_polymorphism.PolymorphismInfo;
import com.kwazart.java_core.main.theme06_class_object.NewClassInfo;
import com.kwazart.java_core.main.theme07_constructors.ConstructorClassInfo;
import com.kwazart.java_core.main.theme11_enum.EnumInfo;
import com.kwazart.java_core.main.theme12_inner_class.InnerInfo;
import com.kwazart.java_core.main.theme13_nested_class.NestedInfo;
import com.kwazart.java_core.main.theme14_local_class.LocalInfo;
import com.kwazart.java_core.main.theme15_anonymous_class.AnonymousInfo;
import com.kwazart.java_core.main.theme16_date.DateInfo;
import com.kwazart.java_core.main.theme17_object.ObjectInfo;
import com.kwazart.java_core.main.theme18_generics.GenericInfo;
import com.kwazart.java_core.main.theme19_static.StaticInfo;
import com.kwazart.java_core.main.theme20_exception.ExceptionInfo;
import com.kwazart.java_core.main.theme21_collections_framework.*;
import com.kwazart.java_core.main.theme21_collections_framework.list.ArrayListInfo;
import com.kwazart.java_core.main.theme21_collections_framework.list.LinkedListInfo;
import com.kwazart.java_core.main.theme21_collections_framework.list.ListInfo;
import com.kwazart.java_core.main.theme21_collections_framework.map.*;
import com.kwazart.java_core.main.theme21_collections_framework.queue.QueueInfo;
import com.kwazart.java_core.main.theme21_collections_framework.set.HashSetInfo;
import com.kwazart.java_core.main.theme21_collections_framework.set.LinkedHashSetInfo;
import com.kwazart.java_core.main.theme21_collections_framework.set.SetInfo;
import com.kwazart.java_core.main.theme21_collections_framework.set.TreeSetInfo;

import java.util.List;
import java.util.Scanner;

/**
 * @Author Artem Polozov
 * @Version 1.0
 * Created 22.09.2020 23:57
 */

public class MenuUtil {
	private ThemeRepository<InfoPrintable> repository;
	private ThemeRepository<InfoPrintable> repositoryJCF;
	private ThemeRepository<InfoPrintable> repositoryIO;

	public MenuUtil() {
		this.repository = new ThemeRepository<>("MAIN menu");
		this.repositoryJCF = new ThemeRepository<>("JCF menu");
		this.repositoryIO = new ThemeRepository<>("IO menu");
		addTheme();
		addJCFTheme();
		addIOTheme();
	}

	public void viewMenu() {
		printLogo();
		System.out.println("\n\tGreetings!" +
				"\nThis reference is designed to provide an introduction to the basics of the Java language." +
				"\nDuring the filling process, material will be added and updated." +
				"\nThe versions can be found in the code of each class.");
		makeChoice(this.repository);
		System.out.println("Goodbye!");
	}

	private void makeChoice(ThemeRepository<InfoPrintable> repository) {
		Scanner scanner = new Scanner(System.in);
		String choice;
		while (true) {
			System.out.println("\n\nYou have entered the " + repository.getName() + " .\nList of topics:");
			printListThemes(repository);
			System.out.print("\n\nSelect topic number (0 - exit): ");
			choice = scanner.nextLine();
			try {
				int index = Integer.parseInt(choice);
				if (index <= repository.getMaxIndex() && index >= 0) {
					if (index == 0) {
						System.out.println("Exit the " + repository.getName() );
						return;
					}
					if (index == 21) {
						makeChoice(repositoryJCF);
					}
					if (index == 22) {
						makeChoice(repositoryIO);
					}
					printStructure(repository.getTheme(index));
					continue;
				}
			} catch (NumberFormatException e) {
				System.err.println("\nInvalid input! Try again.");
				continue;
			}
			System.err.println("\nThere is no such number. Try again.");
		}
	}

	private void printListThemes(ThemeRepository<InfoPrintable> repository) {
		List<InfoPrintable> themes = repository.getAllThemes();
		for (int i = 0; i < themes.size(); i++) {
			System.out.printf("\tTheme %-2d . . . %s\n", (i+1), themes.get(i).getTHEME());
		}
	}

	// список основных тем
	private void addTheme() {
		repository.add(new VariableInfo());
		repository.add(new LogicalInfo());
		repository.add(new LoopInfo());
		repository.add(new GitInfo());
		repository.add(new MethodInfo());
		repository.add(new NewClassInfo());
		repository.add(new ConstructorClassInfo());
		repository.add(new EncapsulationInfo());
		repository.add(new InheritanceInfo());
		repository.add(new PolymorphismInfo());
		repository.add(new EnumInfo());
		repository.add(new InnerInfo());
		repository.add(new NestedInfo());
		repository.add(new LocalInfo());
		repository.add(new AnonymousInfo());
		repository.add(new DateInfo());
		repository.add(new ObjectInfo());
		repository.add(new GenericInfo());
		repository.add(new StaticInfo());
		repository.add(new ExceptionInfo());
		repository.add(new EmptyJcfClass());
		repository.add(new EmptyIOClass());
		repository.add(new SerializableInfo());
		repository.add(new MultiInfo());
		repository.add(new LambdaInfo());
	}

	// список тем коллекций
	private void addJCFTheme() {
		repositoryJCF.add(new CollectionInfo());
		repositoryJCF.add(new ListInfo());
		repositoryJCF.add(new ArrayListInfo());
		repositoryJCF.add(new LinkedListInfo());
		repositoryJCF.add(new SetInfo());
		repositoryJCF.add(new HashSetInfo());
		repositoryJCF.add(new LinkedHashSetInfo());
		repositoryJCF.add(new TreeSetInfo());
		repositoryJCF.add(new QueueInfo());
		repositoryJCF.add(new MapInfo());
		repositoryJCF.add(new HashMapInfo());
		repositoryJCF.add(new LinkedHashMap());
		repositoryJCF.add(new TreeMapInfo());
		repositoryJCF.add(new WeakHashMapInfo());
	}

	// список тем ввода вывода
	private void addIOTheme() {
		repositoryIO.add(new IOInfo());
		repositoryIO.add(new FileInfo());
		repositoryIO.add(new IOStreamInfo());
		repositoryIO.add(new FileIOStreamInfo());
		repositoryIO.add(new ByteArrayIOStreamInfo());
		repositoryIO.add(new ObjectIOStreamInfo());
		repositoryIO.add(new BufferedIOStreamInfo());
		repositoryIO.add(new FileReaderWriterInfo());
		repositoryIO.add(new IOStreamReaderWriterInfo());
		repositoryIO.add(new SystemInfo());
	}

	private void printStructure(InfoPrintable theme) {
		theme.printInfo();
	}

	private void printLogo() {
		System.out.println(
				"    ____     ___   ___      ___     ___            ____    ____    ____     _____ \n" +
						"   |__ |    /   |  \\  \\    /  /    /   |          /  __\\  /  _  \\ |  _  \\  |  ___|\n" +
						"     | |   / /| |   \\  \\  /  /    / /| |         |  |     | | | | | |_|  | | |___ \n" +
						" _   | |  | |_| |    \\  \\/  /    | |_| |         |  |     | | | | |     /  |  ___|\n" +
						"| |__/ |  |  _  |     \\    /     |  _  |         |  |__   | | | | |  |\\ \\  | |___\n" +
						" \\____/   |_| |_|      \\__/      |_| |_|          \\____/   \\____/ |__| \\_\\ |_____|\n"

		);
	}

}
